package verification

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"mime/multipart"
	"net/http/httptest"
	"os"
	"testing"
)

func genConfig() config.Config {
	cfg := config.Config{}
	cfg.Files.Verification = "./verification-test-files"
	return cfg
}

func TestTeam_PageVerification(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "1"))
	assert.NoError(t, w.WriteField("page", "some.domain/some/page"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestTeam_DNSVerification(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "2"))
	assert.NoError(t, w.WriteField("domain", "some.domain"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestTeam_RobotPhotoVerification(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create directory for files
	assert.NoError(t, os.MkdirAll(cfg.Files.Verification, os.ModePerm))

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "3"))
	f, err := w.CreateFormFile("image", "someimage.png")
	assert.NoError(t, err)
	f.Write([]byte("some data"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())

	// Delete created directory
	assert.NoError(t, os.RemoveAll(cfg.Files.Verification))
}

func TestTeam_PageVerification_NonExistentField(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "1"))
	assert.NoError(t, w.WriteField("page", ""))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'page' is required"}`, response.Body.String())
}

func TestTeam_DNSVerification_NonExistentField(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "2"))
	assert.NoError(t, w.WriteField("domain", ""))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'domain' is required"}`, response.Body.String())
}

func TestTeam_RobotPhotoVerification_NonExistentField(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create directory for files
	assert.NoError(t, os.MkdirAll(cfg.Files.Verification, os.ModePerm))

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "3"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "form file 'image' is required"}`, response.Body.String())

	// Delete created directory
	assert.NoError(t, os.RemoveAll(cfg.Files.Verification))
}

func TestTeam_AlreadyVerified(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, true))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "team already verified"}`, response.Body.String())
}

func TestTeam_InvalidMethod(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestTeam_InvalidContentType(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'multipart/form-data'"}`, response.Body.String())
}

func TestTeam_InvalidForm(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "1"))
	assert.NoError(t, w.WriteField("page", "some.domain/some/page"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "invalid form body"}`, response.Body.String())
}

func TestTeam_InvalidInteger(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "a"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'type' must be a valid integer"}`, response.Body.String())
}

func TestTeam_RobotPhotoVerification_InvalidExtension(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)

	// Create directory for files
	assert.NoError(t, os.MkdirAll(cfg.Files.Verification, os.ModePerm))

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "3"))
	f, err := w.CreateFormFile("image", "someimage.test")
	assert.NoError(t, err)
	f.Write([]byte("some data"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "uploaded image must be 'jpg', 'png', or 'jpeg'"}`, response.Body.String())

	// Delete created directory
	assert.NoError(t, os.RemoveAll(cfg.Files.Verification))
}

func TestTeam_InvalidType(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "4"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "invalid verification method"}`, response.Body.String())
}

func TestTeam_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	verificationCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(verificationCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	verificationCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))

	// Create multipart form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	assert.NoError(t, w.WriteField("type", "1"))
	assert.NoError(t, w.WriteField("page", "some.domain/some/page"))
	assert.NoError(t, w.Close())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", &b)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request.Header.Set("Content-Type", w.FormDataContentType())

	// Run request
	team(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to add verification method"}`, response.Body.String())
}
