package verification

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func resetPassword(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	// Create logger
	logger := zap.L().Named("verification.reset_password").With(zap.String("method", "POST"), zap.String("path", "/verification/reset-password"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Validate request method and headers
	if r.Method != "POST" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if body.Password == "" {
		responses.Error(w, http.StatusBadRequest, "field 'password' is required")
		return
	} else if len(body.Password) != 64 {
		responses.Error(w, http.StatusBadRequest, "field 'password' must be 64 characters long")
		return
	}

	// Hash new password
	h, err := hash.DefaultHash(body.Password)
	if err != nil {
		logger.Error("failed to generate password hash", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to hash password")
		return
	}

	user.Password = h

	// Save user
	if err := db.Collection("users").Update(bson.M{"_id": user.Id}, bson.M{"$set": user}, false, false); err != nil {
		logger.Error("failed to update user in database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update user")
		return
	}

	// Delete token
	if err := db.Collection("tokens").DeleteOne(bson.M{"_id": request_context.GetTokenId(r)}); err != nil {
		logger.Error("failed to delete password reset token", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete token")
		return
	}

	responses.Success(w)
	logger.Info("reset password for user")
}
