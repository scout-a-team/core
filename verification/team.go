package verification

import (
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	ByPage int64 = iota + 1
	ByDNS
	ByPhoto
)

func team(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	cfg := request_context.GetConfigFacility(r)
	user := request_context.GetTokenUser(r)

	// Create the logger
	logger := zap.L().Named("verification.team").With(zap.String("method", "POST"), zap.String("path", "/verification/team"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Ensure not already verified
	if request_context.GetTeamVerified(r) {
		responses.Error(w, http.StatusForbidden, "team already verified")
		return
	}

	// Validate request method
	if r.Method != "POST" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if strings.SplitN(r.Header.Get("Content-Type"), ";", 2)[0] != "multipart/form-data" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'multipart/form-data'")
		return
	}

	// Parse and validate form
	if err := r.ParseMultipartForm(10 << 20); err != nil {
		responses.Error(w, http.StatusBadRequest, "invalid form body")
		return
	}
	vType, err := strconv.ParseInt(r.FormValue("type"), 10, 64)
	if err != nil {
		responses.Error(w, http.StatusBadRequest, "field 'type' must be a valid integer")
		return
	}

	logger = logger.With(zap.Int64("verification_type", vType))

	// Match verification type
	var source string
	switch vType {
	case ByPage:
		// Get the page from the form
		page := r.FormValue("page")
		if page == "" {
			responses.Error(w, http.StatusBadRequest, "field 'page' is required")
			return
		}

		source = page

	case ByDNS:
		// Get the domain from the form
		domain := r.FormValue("domain")
		if domain == "" {
			responses.Error(w, http.StatusBadRequest, "field 'domain' is required")
			return
		}

		source = domain

	case ByPhoto:
		// Get the file from the form
		file, handler, err := r.FormFile("image")
		if err == http.ErrMissingFile {
			responses.Error(w, http.StatusBadRequest, "form file 'image' is required")
			return
		} else if err != nil {
			logger.Error("failed to retrieve file from form for social media verification")
			responses.Error(w, http.StatusInternalServerError, "failed to retrieve file")
			return
		}
		defer file.Close()

		// Ensure correct file type
		ext := filepath.Ext(handler.Filename)
		if ext != ".jpg" && ext != ".png" && ext != ".jpeg" {
			responses.Error(w, http.StatusBadRequest, "uploaded image must be 'jpg', 'png', or 'jpeg'")
			return
		}

		// Open temporary file
		tempFile, err := ioutil.TempFile(cfg.Files.Verification, user.TeamId+"-*"+ext)
		if err != nil {
			logger.Error("failed to open temporary file for social media verification", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to save file")
			return
		}
		defer tempFile.Close()

		// Copy to file
		if _, err := io.Copy(tempFile, file); err != nil {
			logger.Error("failed to write to temporary file for social media verification", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to save file")
			return
		}

		source = tempFile.Name()

	default:
		responses.Error(w, http.StatusBadRequest, "invalid verification method")
		return
	}

	// Save verification method to database
	if err := db.Collection("verifications").Update(bson.M{"team": user.TeamId}, bson.M{"$set": bson.M{"type": vType, "source": source}}, false, false); err != nil {
		logger.Error("failed to insert verification method", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to add verification method")
		return
	}

	responses.Success(w)
	logger.Info("began verification process for team")
}
