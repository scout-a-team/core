package verification

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"net/http/httptest"
	"testing"
)

func TestToken(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("database.Verification"), true, true).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))

	// Run request
	token(response, request)

	// Validate response
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Key string `json:"key"`
		} `json:"data"`
	}
	assert.NoError(t, json.NewDecoder(response.Body).Decode(&body))
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, "success", body.Status)
	assert.NotEmpty(t, body.Data.Key)
}

func TestToken_AlreadyVerified(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("database.Verification"), true, true).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, true))

	// Run request
	token(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "team already verified"}`, response.Body.String())
}

func TestToken_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("database.Verification"), true, true).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))

	// Run request
	token(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestToken_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	db.On("Collection", "verifications").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("database.Verification"), true, true).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/team", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, false))

	// Run request
	token(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to save key"}`, response.Body.String())
}
