package verification

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestResetPassword(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestResetPassword_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/reset-password", bytes.NewBufferString(`{"password":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestResetPassword_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestResetPassword_InvalidJSON(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestResetPassword_NoBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' is required"}`, response.Body.String())
}

func TestResetPassword_InvalidLength(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":"abc"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' must be 64 characters long"}`, response.Body.String())
}

func TestResetPassword_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update user"}`, response.Body.String())
}

func TestResetPassword_DeleteDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/reset-password", bytes.NewBufferString(`{"password":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	resetPassword(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete token"}`, response.Body.String())
}
