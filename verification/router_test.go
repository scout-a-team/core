package verification

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRoute(t *testing.T) {
	r := chi.NewRouter()

	// Initialize router
	Route(r)

	expected := []struct {
		route   string
		methods []string
	}{
		{
			route:   "/email",
			methods: []string{"GET"},
		},
		{
			route:   "/reset-password",
			methods: []string{"POST"},
		},
		{
			route:   "/team",
			methods: []string{"GET", "POST"},
		},
	}

	// Check routes are correct
	for i, route := range r.Routes() {
		assert.Equal(t, expected[i].route, route.Pattern)

		for _, method := range expected[i].methods {
			assert.Contains(t, route.Handlers, method)
		}
	}
}
