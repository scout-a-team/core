package verification

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          "some-role",
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestEmail(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/email", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))

	// Run request
	email(response, request)

	// Validate response
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Token string `json:"token"`
		} `json:"data"`
	}
	assert.NoError(t, json.NewDecoder(response.Body).Decode(&body))
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, "success", body.Status)
	assert.NotEmpty(t, body.Data.Token)
}

func TestEmail_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/email", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))

	// Run request
	email(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestEmail_UpdateUserError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/email", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))

	// Run request
	email(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update user"}`, response.Body.String())
}

func TestEmail_DeleteTokenError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/email", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))

	// Run request
	email(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete token"}`, response.Body.String())
}

func TestEmail_GenerateAuthenticationError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/email", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtTokenId, primitive.NewObjectID()))

	// Run request
	email(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate token"}`, response.Body.String())
}
