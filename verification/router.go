package verification

import "github.com/go-chi/chi"

// Add routes to main router
func Route(r chi.Router) {
	r.Get("/email", email)
	r.Post("/team", team)
	r.Get("/team", token)
	r.Post("/reset-password", resetPassword)
}
