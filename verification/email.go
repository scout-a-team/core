package verification

import (
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func email(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	// Create the logger
	logger := zap.L().Named("verification.email").With(zap.String("method", "GET"), zap.String("path", "/verification/email"),
		zap.String("authenticated_user", user.Id.Hex()))

	// Validate request method
	if r.Method != "GET" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	}

	// Mark email as verified
	user.VerifiedEmail = true
	user.Enabled = true

	// Update user
	if err := db.Collection("users").Update(bson.M{"_id": user.Id}, bson.M{"$set": user}, false, false); err != nil {
		logger.Error("failed to mark user email as verified", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update user")
		return
	}

	// Delete verification token
	if err := db.Collection("tokens").DeleteOne(bson.M{"user_id": user.Id.Hex()}); err != nil {
		logger.Error("failed to delete email verification token", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete token")
		return
	}

	// Generate an authentication token
	token, _, err := jwt.GenerateAuthentication(r.Header.Get("User-Agent"), user, db)
	if err != nil {
		logger.Error("failed to generate authentication token", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to generate token")
		return
	}

	responses.SuccessWithData(w, map[string]string{"token": token})
	logger.Info("verified email for user")
}
