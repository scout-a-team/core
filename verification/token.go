package verification

import (
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func token(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	// Create logger
	logger := zap.L().Named("verification.token").With(zap.String("method", "GET"), zap.String("path", "/verification/team"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Ensure not already verified
	if request_context.GetTeamVerified(r) {
		responses.Error(w, http.StatusForbidden, "team already verified")
		return
	}

	// Validate request method
	if r.Method != "GET" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	}

	// Generate random bytes
	key := make([]byte, 64)
	if _, err := rand.Read(key); err != nil {
		logger.Error("failed to generate key for user", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to generate key")
		return
	}

	// Generate hash for database
	hasher := sha512.New()
	hasher.Write(key)
	hash := hex.EncodeToString(hasher.Sum(nil))

	// Add to the database
	if err := db.Collection("verifications").Update(bson.M{"team": user.TeamId}, database.Verification{Team: user.TeamId, Key: hash}, true, true); err != nil {
		logger.Error("failed to save verification key", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to save key")
		return
	}

	responses.SuccessWithData(w, map[string]string{"key": base64.StdEncoding.EncodeToString(key)})
	logger.Info("generated key for team verification")
}
