# Scout a Team Core
This is the core backend code that allows the base services to function properly.
There is nothing here pertaining to the non-free tiers.

## Configuration
To configure the monolith or microservice, you can use either command line flags, environment variables, or a configuration file.
The order of precedence is as stated below, from lowest to highest:
```
    defaults -> remote provider -> file -> environment -> flags
```

### File
The configuration file must be name `config` and be placed in the current working directory or in the folder `/etc/scout-a-team/`.
Alternatively, a specific configuration file can be passed by the flag `--config` or `-c`.
The supported configuration languages are JSON, TOML, YAML, and HCL.
Examples of configuration file type can be found [here](/example-configs).

### Environment
To load configuration from environment variable, use the key in all caps with the prefix `SAT` and underscores for all separating characters.
For example, the key `http.host` would be `SAT_HTTP_HOST`.

### Flags
To load configuration from flags, use the key as the argument and pass the value after it.
However, boolean keys do not require a value to be passed after them.
If a boolean key is present, then it is true, otherwise it is false.
For example, the key `http.host` would be `--http.host`.

### Remote Configuration
To load from remote configuration, the flags prefixed with `provider` must be used.
Currently, Etcd and Consul are the only supported configuration stores.
Encrypted configuration stores are also supported using a GPG keyring.
Below are the flags for configuring a remote provider:

| Flag                    | Type    | Description                               | Default                  |
|-------------------------|---------|-------------------------------------------|--------------------------|
| provider                | string  | Configuration provider to use             | file                     |
| provider.format         | string  | Configuration file type to parse as       | json                     |
| provider.host           | string  | Host of the remote provider               | http://127.0.0.1:4001    |
| provider.path           | string  | Path to the location of the configuration | /config/config.json      |
| provider.secure         | boolean | Read from an encrypted keystore           | false                    |
| provider.secure.keyring | string  | Keyring to use for encrypted keystores    | /etc/secrets/keyring.gpg |

### Configuration Keys
| Key                 | Type    | Description                                                                              | Default               |
|----------------------------|---------|-----------------------------------------------------------------------------------|-----------------------|
| http.host                  | string  | Address for the server to listen on. Use 0.0.0.0 to listen on all interfaces      | 127.0.0.1             |
| http.port                  | integer | Port for the server to run on                                                     | 8080                  |
| http.domain                | string  | Domain the server will be accessible from                                         | http://127.0.0.1:8080 |
| database.connection_string | string  | IP address of the Postgres server                                                 | 127.0.0.1             |
| database.database          | string  | Database to use on the Postgres server                                            | postgres              |
| database.import_workers    | integer | Number of data import workers to spawn                                            | 5                     |
| logging.level              | string  | Minimum log level to output                                                       | info                  |
| logging.development        | boolean | Enable development mode                                                           | false                 |
| tba.api_key                | string  | API key to use for authenticating with the TBA API                                | ""                    |
| tba.user_agent             | string  | The value for the User-Agent header                                               | Go-http-client/1.1    |
| tba.caching                | boolean | Enable the retrieval of data only if it has changed since the last requested data | false                 |
| tba.refresh_interval       | integer | The time (in seconds) for when to refresh the data                                | 900                   |
