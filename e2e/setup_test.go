package e2e

import (
	"context"
	"github.com/go-chi/chi"
	chimiddleware "github.com/go-chi/chi/middleware"
	redislib "github.com/go-redis/redis/v7"
	"gitlab.com/scout-a-team/core/authentication"
	"gitlab.com/scout-a-team/core/events"
	"gitlab.com/scout-a-team/core/match_scout"
	"gitlab.com/scout-a-team/core/pit_scout"
	"gitlab.com/scout-a-team/core/pkg/cache"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/middleware"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"gitlab.com/scout-a-team/core/statistics"
	"gitlab.com/scout-a-team/core/teams"
	"gitlab.com/scout-a-team/core/users"
	"gitlab.com/scout-a-team/core/verification"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
)

// Load configuration
var cfg = config.LoadConfig(true)

// Connect to cache and database
var redis *redislib.Client
var db database.Database

// Mailer configuration
var ctx, cancelMail = context.WithCancel(context.Background())
var mailMutex sync.RWMutex
var mails = make(map[string]mail.Message)

// Router used to test against
var router = chi.NewRouter()

// Setup the database, cache, mailer, and files
func setupFramework() {
	logger := zap.L().Named("testing.e2e.setup")

	// Connect to database and cache
	redis = cache.Connect(cfg)
	db = database.Connect(cfg)

	// Configure mocked mailer
	mailChan := make(chan mail.Message)
	go mailer(mailChan, ctx)

	// Create files directory
	if err := os.MkdirAll(cfg.Files.Verification, os.ModePerm); err != nil {
		logger.Fatal("failed to create verification files directory", zap.String("directory", cfg.Files.Verification), zap.Error(err))
	}
	if err := os.MkdirAll(cfg.Files.Photos, os.ModePerm); err != nil {
		logger.Fatal("failed to create photos files directory", zap.String("directory", cfg.Files.Photos), zap.Error(err))
	}

	// Generate test data
	generateTestData()

	// Setup middlewares
	router.Use(request_context.Database(db))
	router.Use(request_context.Cache(redis))
	router.Use(request_context.Mail(mailChan))
	router.Use(request_context.Config(cfg))
	router.Use(chimiddleware.RealIP)
	router.Use(chimiddleware.Recoverer)
	router.Use(middleware.Logging)
	router.Use(middleware.Authentication(db))
	router.Use(middleware.Verification(db))

	// Setup error handlers
	router.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
	})
	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusNotFound, "not found")
	})

	// Add routes
	router.Route("/authentication", authentication.Route)
	router.Route("/verification", verification.Route)
	router.Route("/teams", teams.Route)
	router.Route("/teams/{team}/users", users.Route)
	router.Route("/scouting/match", match_scout.Route)
	router.Route("/scouting/pit", pit_scout.Route)
	router.Route("/events", events.Route)
	router.Route("/statistics", statistics.Route)
}

// Teardown the database, cache, mailer and files
func teardownFramework() {
	logger := zap.L().Named("testing.e2e.teardown")

	// Stop the mailer
	cancelMail()

	// Remove generated files
	if err := os.RemoveAll(cfg.Files.Verification); err != nil {
		logger.Fatal("failed to remove verification files directory", zap.String("directory", cfg.Files.Verification), zap.Error(err))
	}
	if err := os.RemoveAll(cfg.Files.Photos); err != nil {
		logger.Fatal("failed to remove photos files directory", zap.String("directory", cfg.Files.Photos), zap.Error(err))
	}

	// Drop testing database
	if err := db.Drop(); err != nil {
		logger.Fatal("failed to drop testing database", zap.Error(err))
	}

	// Drop cache database
	if _, err := redis.FlushAll().Result(); err != nil {
		logger.Fatal("failed to drop testing cache", zap.Error(err))
	}
}

// Mail daemon to use instead of the actual MailGun one
func mailer(messages chan mail.Message, ctx context.Context) {
	for {
		select {
		case m, ok := <-messages:
			// Ensure got message
			if !ok {
				continue
			}

			// Add to "mailbox" by template
			mailMutex.Lock()
			mails[m.Template] = m
			mailMutex.Unlock()

		case <-ctx.Done():
			return
		}
	}
}

// Generate the test data for the database
func generateTestData() {
	logger := zap.L().Named("testing.e2e.generate_test_data")

	// Load test data from file
	file, err := os.OpenFile("./test_data.bson", os.O_RDONLY, 0755)
	if err != nil {
		logger.Fatal("failed to open test data file: test_data.bson", zap.Error(err))
	}
	rawData, err := ioutil.ReadAll(file)
	if err != nil {
		logger.Fatal("failed to read test data from file", zap.Error(err))
	}

	// Parse test data into anonymous struct
	// Arrays of interfaces are used instead of concrete types due to
	// the type that InsertMany accepts. Also, BSON is used instead of
	// JSON due to what fields are ignored in HTTP responses.
	var data struct {
		Teams   []interface{} `bson:"teams"`
		Events  []interface{} `bson:"events"`
		Matches []interface{} `bson:"matches"`
	}
	if err := bson.Unmarshal(rawData, &data); err != nil {
		logger.Fatal("failed to unmarshal data", zap.Error(err))
	}

	// Insert data into database
	if _, err := db.Collection("teams").InsertMany(data.Teams); err != nil {
		logger.Fatal("failed to insert teams into database", zap.Error(err))
	}
	if _, err := db.Collection("events").InsertMany(data.Events); err != nil {
		logger.Fatal("failed to insert events into database", zap.Error(err))
	}
	if _, err := db.Collection("matches").InsertMany(data.Matches); err != nil {
		logger.Fatal("failed to insert matches into database", zap.Error(err))
	}
}
