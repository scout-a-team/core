package e2e

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListEvents(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string           `json:"status"`
		Data   []database.Event `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 2)
}

func TestListEvents_ByTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events", ownerAuthenticationToken, map[string]string{"team": "frc114"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string           `json:"status"`
		Data   []database.Event `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
}

func TestListEvents_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events", ownerAuthenticationToken, map[string]string{"team": "nonexistent"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string           `json:"status"`
		Data   []database.Event `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 0)
}

func TestDescribeEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/2020caln", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string         `json:"status"`
		Data   database.Event `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve event from database
	var event database.Event
	assert.NoError(t, db.Collection("events").FindOne(bson.M{"key": "2020caln"}).Decode(&event))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, event.Id, body.Data.Id)
}

func TestDescribeEvent_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/abcdefg", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestListTeamsInEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/2020caln/teams", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string   `json:"status"`
		Data   []string `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve event from database
	var event database.Event
	assert.NoError(t, db.Collection("events").FindOne(bson.M{"key": "2020caln"}).Decode(&event))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, event.TeamIds, body.Data)
}

func TestListTeamsInEvent_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/abcdefg/teams", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestListMatchesInEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/2020caln/matches", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string                   `json:"status"`
		Data   []map[string]interface{} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve event from database
	var matches []database.Match
	assert.NoError(t, db.Collection("matches").FindMany(bson.M{"event": "2020caln"}).All(&matches))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(matches))
}

func TestListMatchesInEvent_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/abcdefg/matches", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestDescribeMatch(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/2020caln/matches/2020caln_qm1", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string         `json:"status"`
		Data   database.Match `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, "2020caln_qm1", body.Data.Key)
	assert.Equal(t, "2020caln", body.Data.EventId)
}

func TestDescribeMatch_NonExistentMatch(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/2020caln/matches/abcdefg", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified match does not exist in specified event"}`, response.Body.String())
}

func TestDescribeMatch_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/events/abcdefg/matches/2020caln_qm1", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}
