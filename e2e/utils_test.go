package e2e

import (
	"bytes"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
)

func sha256Hash(str string) string {
	hash := sha256.New()
	hash.Write([]byte(str))
	return hex.EncodeToString(hash.Sum(nil))
}

func sha512Hash(str string) string {
	hash := sha512.New()
	hash.Write([]byte(str))
	return hex.EncodeToString(hash.Sum(nil))
}

func createRequest(method, target, token string, query map[string]string, body map[string]interface{}) *http.Request {
	// Create request
	var request *http.Request
	if body != nil {
		encoded, _ := json.Marshal(body)
		buff := bytes.NewBuffer(encoded)
		request = httptest.NewRequest(method, target, buff)
		request.Header.Set("Content-Type", "application/json")
	} else {
		request = httptest.NewRequest(method, target, nil)
	}

	// Add authentication
	if token != "" {
		request.Header.Set("Authorization", token)
	}

	// Add query parameters
	if query != nil {
		qp := request.URL.Query()
		for key, value := range query {
			qp.Set(key, value)
		}
		request.URL.RawQuery = qp.Encode()
	}

	return request
}

func createForm(values map[string]io.Reader) (bytes.Buffer, string) {
	var b bytes.Buffer
	writer := multipart.NewWriter(&b)
	for key, r := range values {
		var field io.Writer
		switch r.(type) {
		case *os.File:
			f := r.(*os.File)
			field, _ = writer.CreateFormFile(key, f.Name())
		default:
			field, _ = writer.CreateFormField(key)
		}

		io.Copy(field, r)

		if w, ok := r.(io.Closer); ok {
			w.Close()
		}
	}

	writer.Close()

	return b, writer.FormDataContentType()
}
