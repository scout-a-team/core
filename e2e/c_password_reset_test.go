package e2e

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var resetPasswordFlowResetToken = ""

func TestForgotPassword_NonExistentUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/authentication/forgot-password", "", map[string]string{"email": "user.does@not.exist"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure email was not sent
	assert.NotContains(t, mails, "password-reset")

	// Ensure token not generated
	assert.EqualError(t, db.Collection("tokens").FindOne(bson.M{"type": database.TokenPasswordReset}).Decode(&struct{}{}), mongo.ErrNoDocuments.Error())
}

func TestForgotPassword(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/authentication/forgot-password", "", map[string]string{"email": "alex@krantz.dev"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Wait a bit for the email to get transferred
	time.Sleep(500 * time.Millisecond)

	// Ensure email was sent
	mailMutex.Lock()
	assert.Contains(t, mails, "password-reset")
	resetEmail := mails["password-reset"]
	assert.Equal(t, ownerUser.Email, resetEmail.Recipient)
	assert.Equal(t, ownerUser.Name, resetEmail.Variables["name"])
	assert.Contains(t, resetEmail.Variables, "token")
	resetPasswordFlowResetToken = resetEmail.Variables["token"].(string)
	delete(mails, "password-reset")
	mailMutex.Unlock()

	// Ensure token generated
	token := jwt.Unvalidated(resetPasswordFlowResetToken)
	id, err := primitive.ObjectIDFromHex(token.Header["kid"].(string))
	assert.NoError(t, err)
	assert.NoError(t, db.Collection("tokens").FindOne(bson.M{"_id": id, "type": database.TokenPasswordReset}).Decode(&struct{}{}))
}

func TestResetPassword(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve password before
	beforePassword := ownerUser.Password

	// Create body
	body := map[string]interface{}{
		"password": sha256Hash("my-amazing-password-2"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/verification/reset-password", resetPasswordFlowResetToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Retrieve password after
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"_id": ownerUser.Id}).Decode(&ownerUser))
	assert.NotEqual(t, beforePassword, ownerUser.Password)
}

func TestPasswordReset_FailureOnTokenReuse(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"password": sha256Hash("this-isn't-to-be-set"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/verification/reset-password", resetPasswordFlowResetToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusUnauthorized, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid token: failed to find signing key for token"}`, response.Body.String())
}
