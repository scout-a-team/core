package e2e

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListPitScoutTeams(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/pit", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []struct {
			Number int    `json:"number"`
			Name   string `json:"name"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve list of all teams
	var teams []database.Team
	assert.NoError(t, db.Collection("teams").FindMany(bson.M{}).All(&teams))

	// Validate body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(teams))
}

func TestListPitScoutTeams_AlreadyScouted(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/pit", ownerAuthenticationToken, map[string]string{"filter": "with"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []struct {
			Number int    `json:"number"`
			Name   string `json:"name"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 0)
}

func TestCreatePitScoutData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"weight":                  125,
		"width":                   31,
		"length":                  30,
		"drive_train_motor_count": 4,
		"drive_train_motor_type":  "CIM",
		"drive_train_type":        "Tank",
		"wheel_type":              "Traction",
		"wheel_diameter":          6,
		"programming_language":    "Java",
		"camera":                  true,
		"notes":                   "overall decent",
		"game_specific": map[string]interface{}{
			"climbing_method": "hook",
			"move_on_bar":     false,
			"carry_robot":     false,
			"low_goal":        true,
			"outer_goal":      true,
			"inner_goal":      true,
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/scouting/pit/%s", ownerUser.TeamId), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure pit scout data set properly
	id, _ := primitive.ObjectIDFromHex(ownerUser.TeamId)
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"_id": id}).Decode(&team))
	assert.Contains(t, team.Pit, ownerUser.TeamId)
	assert.True(t, team.Pit[ownerUser.TeamId].Completed)
}

func TestCreatePitScoutData_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"weight":                  125,
		"width":                   31,
		"length":                  30,
		"drive_train_motor_count": 4,
		"drive_train_motor_type":  "CIM",
		"drive_train_type":        "Tank",
		"wheel_type":              "Traction",
		"wheel_diameter":          6,
		"programming_language":    "Java",
		"camera":                  true,
		"notes":                   "overall decent",
		"game_specific": map[string]interface{}{
			"climbing_method": "hook",
			"move_on_bar":     false,
			"carry_robot":     false,
			"low_goal":        true,
			"outer_goal":      true,
			"inner_goal":      true,
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/scouting/pit/%s", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestReadPitScoutData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/scouting/pit/%s", ownerUser.TeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string            `json:"status"`
		Data   database.PitScout `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, float64(125), body.Data.Weight)
	assert.Equal(t, float64(31), body.Data.Width)
	assert.Equal(t, float64(30), body.Data.Length)
	assert.Equal(t, int64(4), body.Data.DriveTrainMotorCount)
	assert.Equal(t, "CIM", body.Data.DriveTrainMotorType)
	assert.Equal(t, "Tank", body.Data.DriveTrainType)
	assert.Equal(t, "Traction", body.Data.WheelType)
	assert.Equal(t, int64(6), body.Data.WheelDiameter)
	assert.Equal(t, "Java", body.Data.ProgrammingLanguage)
	assert.True(t, body.Data.HasCamera)
	assert.Equal(t, "overall decent", body.Data.Notes)
	assert.Equal(t, "hook", body.Data.GameSpecific.ClimbingMethod)
	assert.False(t, body.Data.GameSpecific.MoveOnBar)
	assert.False(t, body.Data.GameSpecific.CarryRobot)
	assert.True(t, body.Data.GameSpecific.LowGoal)
	assert.True(t, body.Data.GameSpecific.OuterGoal)
	assert.True(t, body.Data.GameSpecific.InnerGoal)
}

func TestReadPitScoutData_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/scouting/pit/%s", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}
