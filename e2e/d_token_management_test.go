package e2e

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
)

var tokenManagementTokenList []database.Token

func TestListTokens(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/authentication/tokens", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []database.Token
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Get list of tokens in database
	assert.NoError(t, db.Collection("tokens").FindMany(bson.M{"user_id": ownerUser.Id.Hex(), "type": database.TokenAuthentication}).All(&tokenManagementTokenList))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(tokenManagementTokenList))
}

func TestDeleteTokenById(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Find earliest issued token
	token := tokenManagementTokenList[0]
	for _, t := range tokenManagementTokenList {
		if t.IssuedAt.Before(token.IssuedAt) {
			token = t
		}
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/authentication/tokens", ownerAuthenticationToken, map[string]string{"id": token.Id.Hex()}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure token no longer exists
	assert.EqualError(t, db.Collection("tokens").FindOne(bson.M{"_id": token.Id}).Decode(&struct{}{}), mongo.ErrNoDocuments.Error())
}

func TestDeleteTokenById_WithInvalidId(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/authentication/tokens", ownerAuthenticationToken, map[string]string{"id": "invalid-token"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid id format"}`, response.Body.String())
}

func TestDeleteAllTokens(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/authentication/tokens", ownerAuthenticationToken, map[string]string{"id": "all"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure no more tokens in database
	var tokens []database.Token
	assert.NoError(t, db.Collection("tokens").FindMany(bson.M{"type": database.TokenAuthentication}).All(&tokens))
	assert.Len(t, tokens, 0)
}

func TestReLogin(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password-2"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token    string `json:"token"`
			Redirect bool   `json:"redirect"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))

	// Validate response body
	assert.Equal(t, "success", responseBody.Status)
	assert.False(t, responseBody.Data.Redirect)
	assert.NotEmpty(t, responseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), responseBody.Data.Token)
	ownerAuthenticationToken = responseBody.Data.Token
}
