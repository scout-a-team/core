package e2e

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var secondaryUser database.User
var secondaryAuthenticationToken = ""

func TestCreateUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"first_name": "Secondary",
		"last_name":  "User",
		"email":      "secondary@user.com",
		"password":   sha256Hash("another-amazing-password"),
		"role":       rbac.RoleMember,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", fmt.Sprintf("/teams/%s/users", ownerUser.TeamId), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure user created
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"email": "secondary@user.com"}).Decode(&secondaryUser))
	assert.Equal(t, "secondary@user.com", secondaryUser.Email)
	assert.Equal(t, "Secondary User", secondaryUser.Name)
	assert.Equal(t, rbac.RoleMember, secondaryUser.Role)
	assert.Equal(t, ownerUser.TeamId, secondaryUser.TeamId)

	// Wait a bit for the email to get transferred
	time.Sleep(500 * time.Millisecond)

	// Ensure on-boarding email sent
	mailMutex.Lock()
	assert.Contains(t, mails, "user-registration")
	userRegistrationEmail := mails["user-registration"]
	assert.Equal(t, secondaryUser.Email, userRegistrationEmail.Recipient)
	assert.Equal(t, secondaryUser.Name, userRegistrationEmail.Variables["name"])
	delete(mails, "user-registration")
	mailMutex.Unlock()

	// Ensure verification email sent
	mailMutex.Lock()
	assert.Contains(t, mails, "email-verification")
	emailVerification := mails["email-verification"]
	assert.Equal(t, secondaryUser.Email, emailVerification.Recipient)
	assert.Equal(t, secondaryUser.Name, emailVerification.Variables["name"])
	assert.Contains(t, emailVerification.Variables, "token")
	delete(mails, "email-verification")
	mailMutex.Unlock()

	// Mark user as verified
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": secondaryUser.Id}, bson.M{"$set": bson.M{"email_verified": true, "enabled": true}}, false, false))
}

func TestCreateUser_WithExistingEmail(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"first_name": "Secondary",
		"last_name":  "User",
		"email":      "secondary@user.com",
		"password":   sha256Hash("another-amazing-password"),
		"role":       rbac.RoleMember,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", fmt.Sprintf("/teams/%s/users", ownerUser.TeamId), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusConflict, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified email address is already in use"}`, response.Body.String())

	// Ensure no new users created
	var users []database.User
	assert.NoError(t, db.Collection("users").FindMany(bson.M{}).All(&users))
	assert.Len(t, users, 2)
}

func TestCreateUser_UserNotInTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"first_name": "Secondary",
		"last_name":  "User",
		"email":      "secondary@user.com",
		"password":   sha256Hash("another-amazing-password"),
		"role":       rbac.RoleMember,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", fmt.Sprintf("/teams/%s/users", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestCreateUser_ImproperPermissions(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Generate authentication token for secondary user
	var err error
	secondaryAuthenticationToken, _, err = jwt.GenerateAuthentication("", secondaryUser, db)
	assert.NoError(t, err)

	// Create body
	body := map[string]interface{}{
		"first_name": "Secondary",
		"last_name":  "User",
		"email":      "secondary@user.com",
		"password":   sha256Hash("another-amazing-password"),
		"role":       rbac.RoleMember,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", fmt.Sprintf("/teams/%s/users", ownerUser.TeamId), secondaryAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestListUsers(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send requests
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/users", ownerUser.TeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []database.User
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Get list of tokens in database
	var users []database.User
	assert.NoError(t, db.Collection("users").FindMany(bson.M{"team_id": ownerUser.TeamId}).All(&users))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(users))
}

func TestListUsers_UserNotInTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send requests
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/users", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestReadUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, secondaryUser.Id.Hex()), ownerAuthenticationToken, map[string]string{}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string        `json:"status"`
		Data   database.User `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, body.Data.Id, secondaryUser.Id)
}

func TestReadUser_NonExistentUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, primitive.NewObjectID().Hex()), ownerAuthenticationToken, map[string]string{}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified user does not exist"}`, response.Body.String())
}

func TestReadUser_UserNotInTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/users/%s", primitive.NewObjectID().Hex(), secondaryUser.Id.Hex()), ownerAuthenticationToken, map[string]string{}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestUpdateUser_ByMember(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":  "John Doe",
		"email": "some-email@address.com",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s", secondaryUser.TeamId, secondaryUser.Id.Hex()), secondaryAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure properly updated
	var user database.User
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"_id": secondaryUser.Id}).Decode(&user))
	assert.Equal(t, "John Doe", user.Name)
	assert.Equal(t, "some-email@address.com", user.Email)

	// Wait a bit for the email to get transferred
	time.Sleep(500 * time.Millisecond)

	// Ensure email verification mail sent
	mailMutex.Lock()
	assert.Contains(t, mails, "email-verification")
	emailVerification := mails["email-verification"]
	assert.Equal(t, "some-email@address.com", emailVerification.Recipient)
	assert.Equal(t, "John Doe", emailVerification.Variables["name"])
	assert.Contains(t, emailVerification.Variables, "token")
	delete(mails, "email-verification")
	mailMutex.Unlock()

	// Reset user data
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": secondaryUser.Id}, bson.M{"$set": bson.M{"name": "Secondary User", "email": "secondary@user.com"}}, false, false))
}

func TestUpdateUser_MemberNotUpdatingSelf(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":  "John Doe",
		"email": "some-email@address.com",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, primitive.NewObjectID().Hex()), secondaryAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestUpdateUser_ByOwner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":  "John Doe",
		"email": "some-email@address.com",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, secondaryUser.Id.Hex()), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure properly updated
	var user database.User
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"_id": secondaryUser.Id}).Decode(&user))
	assert.Equal(t, "John Doe", user.Name)
	assert.Equal(t, "some-email@address.com", user.Email)

	// Wait a bit for the email to get transferred
	time.Sleep(500 * time.Millisecond)

	// Ensure email verification mail sent
	mailMutex.Lock()
	assert.Contains(t, mails, "email-verification")
	emailVerification := mails["email-verification"]
	assert.Equal(t, "some-email@address.com", emailVerification.Recipient)
	assert.Equal(t, "John Doe", emailVerification.Variables["name"])
	assert.Contains(t, emailVerification.Variables, "token")
	delete(mails, "email-verification")
	mailMutex.Unlock()

	// Reset user data
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": secondaryUser.Id}, bson.M{"$set": bson.M{"name": "Secondary User", "email": "secondary@user.com"}}, false, false))
}

func TestUpdateUser_ByOwnerNonExistentUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":  "John Doe",
		"email": "some-email@address.com",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified user does not exist"}`, response.Body.String())
}

func TestAssignRole(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s/assign", ownerUser.TeamId, secondaryUser.Id.Hex()), ownerAuthenticationToken, nil, map[string]interface{}{"role": rbac.RoleMentor})
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure role assigned
	var user database.User
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"_id": secondaryUser.Id}).Decode(&user))
	assert.Equal(t, user.Role, rbac.RoleMentor)

	// Reset user role
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": secondaryUser.Id}, bson.M{"$set": bson.M{"role": rbac.RoleMember}}, false, false))
}

func TestAssignRole_InvalidPermissions(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s/assign", ownerUser.TeamId, secondaryUser.Id.Hex()), secondaryAuthenticationToken, nil, map[string]interface{}{"role": rbac.RoleMentor})
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestAssignRole_AssignOwner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s/assign", ownerUser.TeamId, secondaryUser.Id.Hex()), ownerAuthenticationToken, nil, map[string]interface{}{"role": rbac.RoleOwner})
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "cannot create a new owner"}`, response.Body.String())
}

func TestAssignRole_NonExistentUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s/assign", ownerUser.TeamId, primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, map[string]interface{}{"role": rbac.RoleMentor})
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified user does not exist"}`, response.Body.String())
}

func TestDeleteUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create test user
	id := primitive.NewObjectID()
	_, err := db.Collection("users").InsertOne(database.User{
		Id:            id,
		Name:          "Delete Me",
		Email:         "delete@me.plz",
		Password:      "some-unhashed-password",
		Role:          rbac.RoleMember,
		TeamId:        ownerUser.TeamId,
		Enabled:       false,
		VerifiedEmail: false,
		TOTP:          database.TwoFactor{},
		WebAuthn:      database.TwoFactor{},
		RecoveryCodes: nil,
	})
	assert.NoError(t, err)

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure user successfully deleted
	assert.EqualError(t, db.Collection("users").FindOne(bson.M{"_id": id}).Err(), mongo.ErrNoDocuments.Error())
}

func TestDeleteUser_NonExistentUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified user does not exist"}`, response.Body.String())
}

func TestDeleteUser_DeleteOwner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, ownerUser.Id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "owner must delete team to delete self"}`, response.Body.String())
}

func TestDeleteUser_InvalidPermissions(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", fmt.Sprintf("/teams/%s/users/%s", ownerUser.TeamId, secondaryUser.Id.Hex()), secondaryAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}
