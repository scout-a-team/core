package e2e

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/match_scout"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListMatchesToScout(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/match/2020caln", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string   `json:"status"`
		Data   []string `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve event
	var event database.Event
	assert.NoError(t, db.Collection("events").FindOne(bson.M{"key": "2020caln"}).Decode(&event))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, body.Data, event.MatchIds)
}

func TestListMatchesToScout_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/match/abcdefg", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestScoutMatch_ByOwner(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"match": "2020caln_qm1",
		"team":  114,
		"events": map[string]map[string]interface{}{
			"2.4": {
				"type":                match_scout.Shot,
				"shots_made":          4,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationOuter,
				"shot_from":           0,
			},
			"5.6": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.7": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.8": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.9": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"6.0": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"10.9": {
				"type":                match_scout.Shot,
				"shots_made":          5,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationLow,
				"shot_from":           0,
			},
			"25.1": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"25.2": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"25.3": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"30.5": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyTrench,
			},
			"30.6": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyTrench,
			},
			"42.4": {
				"type":         match_scout.CPStage2,
				"cp_start":     42.4,
				"cp_end":       51.3,
				"cp_attempts":  2,
				"cp_completed": true,
			},
			"54.0": {
				"type":              match_scout.Malfunction,
				"malfunction_start": 54.0,
				"malfunction_end":   63.1,
				"malfunction_type":  match_scout.MalfunctionTypeBreak,
			},
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/scouting/match/2020caln", ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Retrieve scouted data for match
	var scoutedMatch database.TeamMatchData
	assert.NoError(t, db.Collection("team_match_data").FindOne(bson.M{"match": "2020caln_qm1", "scouted_by": ownerUser.TeamId, "team": "frc114"}).Decode(&scoutedMatch))

	// Retrieve scouted averages for event
	var scoutedEvent database.EventMatchAverage
	assert.NoError(t, db.Collection("team_match_average").FindOne(bson.M{"team": 114, "scouted_by": ownerUser.TeamId}).Decode(&scoutedEvent))
}

func TestScoutMatch_ByMember(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"match": "2020caln_qm1",
		"team":  1661,
		"events": map[string]map[string]interface{}{
			"2.4": {
				"type":                match_scout.Shot,
				"shots_made":          4,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationOuter,
				"shot_from":           0,
			},
			"5.6": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.7": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.8": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"5.9": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"6.0": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromOpponentBoundaries,
			},
			"10.9": {
				"type":                match_scout.Shot,
				"shots_made":          5,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationLow,
				"shot_from":           0,
			},
			"25.1": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"25.2": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"25.3": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyLoading,
			},
			"30.5": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyTrench,
			},
			"30.6": {
				"type":              match_scout.Collected,
				"collected_success": true,
				"collected_from":    match_scout.CollectedFromFriendlyTrench,
			},
			"42.4": {
				"type":         match_scout.CPStage2,
				"cp_start":     42.4,
				"cp_end":       51.3,
				"cp_attempts":  2,
				"cp_completed": true,
			},
			"54.0": {
				"type":              match_scout.Malfunction,
				"malfunction_start": 54.0,
				"malfunction_end":   63.1,
				"malfunction_type":  match_scout.MalfunctionTypeBreak,
			},
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/scouting/match/2020caln", secondaryAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Retrieve scouted data for match
	var scoutedMatch database.TeamMatchData
	assert.NoError(t, db.Collection("team_match_data").FindOne(bson.M{"match": "2020caln_qm1", "scouted_by": ownerUser.TeamId, "team": "frc114"}).Decode(&scoutedMatch))

	// Retrieve scouted averages for event
	var scoutedEvent database.EventMatchAverage
	assert.NoError(t, db.Collection("team_match_average").FindOne(bson.M{"team": 114, "scouted_by": ownerUser.TeamId}).Decode(&scoutedEvent))
}

func TestScoutMatch_AlreadyScoutedMatch(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	// Stripped down bc it's intended to fail
	body := map[string]interface{}{
		"match": "2020caln_qm1",
		"team":  114,
		"events": map[string]map[string]interface{}{
			"2.4": {
				"type":                match_scout.Shot,
				"shots_made":          4,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationOuter,
				"shot_from":           0,
			},
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/scouting/match/2020caln", ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team already scouted for match"}`, response.Body.String())
}

func TestScoutMatch_TeamNotInEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	// Stripped down bc it's intended to fail
	body := map[string]interface{}{
		"match": "2020caln_qm1",
		"team":  100,
		"events": map[string]map[string]interface{}{
			"2.4": {
				"type":                match_scout.Shot,
				"shots_made":          4,
				"shots_taken":         5,
				"shot_score_location": match_scout.ShotLocationOuter,
				"shot_from":           0,
			},
		},
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/scouting/match/2020caln", ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team not in event"}`, response.Body.String())
}

func TestMatchStats(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/match/2020caln/stats", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string                       `json:"status"`
		Data   []database.EventMatchAverage `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 2)
}

func TestMatchStats_NoScoutedData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/match/2020mndu/stats", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestMatchStats_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/scouting/match/abcdefg/stats", ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}
