package e2e

import (
	"encoding/json"
	"fmt"
	"github.com/pquerna/otp/totp"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"
	"time"
)

var twoFactorFlowTOTPSecret = ""
var twoFactorFlowRecoveryCode = ""

func TestEnableTOTP2FA(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"totp":     true,
		"webauthn": false,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", fmt.Sprintf("/teams/%s/users/%s/2fa", ownerUser.TeamId, ownerUser.Id.Hex()), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Totp          string   `json:"totp"`
			RecoveryCodes []string `json:"recovery_codes"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))

	// Validate response body
	assert.Equal(t, "success", responseBody.Status)
	assert.Len(t, responseBody.Data.Totp, 135)
	assert.Len(t, responseBody.Data.RecoveryCodes, 8)
	for _, code := range responseBody.Data.RecoveryCodes {
		assert.Len(t, code, 16)
		assert.Regexp(t, regexp.MustCompile("^[0-9a-f]{16}$"), code)
	}
	twoFactorFlowTOTPSecret = responseBody.Data.Totp
	twoFactorFlowRecoveryCode = responseBody.Data.RecoveryCodes[0]
}

func TestLogin_WithTOTP(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create login request body
	loginBody := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password-2"),
	}

	// Send login request
	loginResponse := httptest.NewRecorder()
	loginRequest := createRequest("POST", "/authentication/login", "", nil, loginBody)
	router.ServeHTTP(loginResponse, loginRequest)

	// Basic response validation
	assert.Equal(t, http.StatusOK, loginResponse.Code)

	// Parse login response body
	var loginResponseBody struct {
		Status string `json:"status"`
		Data   struct {
			TwoFactor bool   `json:"2fa"`
			Token     string `json:"token"`
			Methods   struct {
				Totp     bool `json:"totp"`
				Webauthn bool `json:"webauthn"`
			}
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(loginResponse.Body.Bytes(), &loginResponseBody))

	// Validate login response body
	assert.Equal(t, "success", loginResponseBody.Status)
	assert.True(t, loginResponseBody.Data.TwoFactor)
	assert.NotEmpty(t, loginResponseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), loginResponseBody.Data.Token)
	assert.True(t, loginResponseBody.Data.Methods.Totp)
	assert.False(t, loginResponseBody.Data.Methods.Webauthn)

	// Generate TOTP code
	secret := twoFactorFlowTOTPSecret[strings.Index(twoFactorFlowTOTPSecret, "secret=")+7:]
	code, err := totp.GenerateCode(secret, time.Now())
	assert.NoError(t, err)

	// Send totp request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/authentication/2fa/totp", loginResponseBody.Data.Token, map[string]string{"code": code}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse totp response body
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token string `json:"token"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))

	// Validate body
	assert.Equal(t, "success", responseBody.Status)
	assert.NotEmpty(t, responseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), responseBody.Data.Token)
}

func TestLogin_WithInvalidTOTP(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create login request body
	loginBody := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password-2"),
	}

	// Send login request
	loginResponse := httptest.NewRecorder()
	loginRequest := createRequest("POST", "/authentication/login", "", nil, loginBody)
	router.ServeHTTP(loginResponse, loginRequest)

	// Basic response validation
	assert.Equal(t, http.StatusOK, loginResponse.Code)

	// Parse login response body
	var loginResponseBody struct {
		Status string `json:"status"`
		Data   struct {
			TwoFactor bool   `json:"2fa"`
			Token     string `json:"token"`
			Methods   struct {
				Totp     bool `json:"totp"`
				Webauthn bool `json:"webauthn"`
			}
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(loginResponse.Body.Bytes(), &loginResponseBody))

	// Validate login response body
	assert.Equal(t, "success", loginResponseBody.Status)
	assert.True(t, loginResponseBody.Data.TwoFactor)
	assert.NotEmpty(t, loginResponseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), loginResponseBody.Data.Token)
	assert.True(t, loginResponseBody.Data.Methods.Totp)
	assert.False(t, loginResponseBody.Data.Methods.Webauthn)

	// Send totp request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/authentication/2fa/totp", loginResponseBody.Data.Token, map[string]string{"code": "123456"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid authentication code"}`, response.Body.String())
}

func TestDisableTOTP2FA(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", fmt.Sprintf("/teams/%s/users/%s/2fa", ownerUser.TeamId, ownerUser.Id.Hex()), ownerAuthenticationToken, map[string]string{"method": "totp"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure no methods enabled
	var user database.User
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"_id": ownerUser.Id}).Decode(&user))
	assert.False(t, user.TOTP.Enabled)
}
