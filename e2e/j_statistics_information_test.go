package e2e

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListEventMatchAverage(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events", ownerAuthenticationToken, map[string]string{"team": "114"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string                       `json:"status"`
		Data   []database.EventMatchAverage `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
	assert.Equal(t, "2020caln", body.Data[0].Event)
	assert.Equal(t, 1, body.Data[0].DataPoints)
}

func TestListEventMatchAverage_TeamWithoutData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events", ownerAuthenticationToken, map[string]string{"team": "100"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestListEventMatchAverage_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events", ownerAuthenticationToken, map[string]string{"team": "1"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestListMatchPerformance(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events/2020caln/matches", ownerAuthenticationToken, map[string]string{"team": "114"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string                   `json:"status"`
		Data   []database.TeamMatchData `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
	assert.Equal(t, "2020caln_qm1", body.Data[0].MatchId)
	assert.Equal(t, "frc114", body.Data[0].TeamId)
}

func TestListMatchPerformance_TeamWithoutData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events/2020caln/matches", ownerAuthenticationToken, map[string]string{"team": "5089"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestListMatchPerformance_TeamNotInEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events/2020caln/matches", ownerAuthenticationToken, map[string]string{"team": "100"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team not in event"}`, response.Body.String())
}

func TestListMatchPerformance_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events/2020caln/matches", ownerAuthenticationToken, map[string]string{"team": "1"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestListMatchPerformance_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/events/abcdefg/matches", ownerAuthenticationToken, map[string]string{"team": "114"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestEventRankingsByPoints(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/rankings/points", ownerAuthenticationToken, map[string]string{"event": "2020caln"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string       `json:"status"`
		Data   [][2]float64 `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve team ids from database for event
	var event database.Event
	assert.NoError(t, db.Collection("events").FindOne(bson.M{"key": "2020caln"}).Decode(&event))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(event.TeamIds))
}

func TestEventRankingsByPoints_NonExistentEvent(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/rankings/points", ownerAuthenticationToken, map[string]string{"event": "abcdefg"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestGlobalRankingsByTrueSkill_ForTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/rankings/skill", ownerAuthenticationToken, map[string]string{"global": "no"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string       `json:"status"`
		Data   [][2]float64 `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve all teams
	var teams []database.Team
	assert.NoError(t, db.Collection("teams").FindMany(bson.M{}).All(&teams))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(teams))
}

func TestGlobalRankingsByTrueSkill_ForGlobal(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/statistics/rankings/skill", ownerAuthenticationToken, map[string]string{"global": "yes"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string       `json:"status"`
		Data   [][2]float64 `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Retrieve all teams
	var teams []database.Team
	assert.NoError(t, db.Collection("teams").FindMany(bson.M{}).All(&teams))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, len(teams))
}
