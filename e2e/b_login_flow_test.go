package e2e

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
)

var ownerAuthenticationToken = ""

func TestLogin_TeamNotVerified(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token    string `json:"token"`
			Redirect bool   `json:"redirect"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))

	// Validate response body
	assert.Equal(t, "success", responseBody.Status)
	assert.True(t, responseBody.Data.Redirect)
	assert.NotEmpty(t, responseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), responseBody.Data.Token)

	// Ensure token exists
	token := jwt.Unvalidated(responseBody.Data.Token)
	id, err := primitive.ObjectIDFromHex(token.Header["kid"].(string))
	assert.NoError(t, err)
	assert.NoError(t, db.Collection("tokens").FindOne(bson.M{"_id": id}).Decode(&struct{}{}))
}

func TestLogin_TeamVerified(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Mark team as verified
	tid, err := primitive.ObjectIDFromHex(ownerUser.TeamId)
	assert.NoError(t, err)
	assert.NoError(t, db.Collection("teams").Update(bson.M{"_id": tid}, bson.M{"$set": bson.M{"verified": true}}, false, false))

	// Create request body
	body := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token    string `json:"token"`
			Redirect bool   `json:"redirect"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))

	// Validate response body
	assert.Equal(t, "success", responseBody.Status)
	assert.False(t, responseBody.Data.Redirect)
	assert.NotEmpty(t, responseBody.Data.Token)
	assert.Regexp(t, regexp.MustCompile("[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+.[a-zA-Z0-9_-]+."), responseBody.Data.Token)
	ownerAuthenticationToken = responseBody.Data.Token

	// Ensure token exists
	token := jwt.Unvalidated(responseBody.Data.Token)
	id, err := primitive.ObjectIDFromHex(token.Header["kid"].(string))
	assert.NoError(t, err)
	assert.NoError(t, db.Collection("tokens").FindOne(bson.M{"_id": id}).Decode(&struct{}{}))
}

func TestLogin_InvalidEmail(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":    "invalid@e.mail",
		"password": sha256Hash("my-amazing-password"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusUnauthorized, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid email or password"}`, response.Body.String())
}

func TestLogin_InvalidPassword(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-invalid-password"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusUnauthorized, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid email or password"}`, response.Body.String())
}

func TestLogin_DisabledUser(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Mark user as disabled
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": ownerUser.Id}, bson.M{"$set": bson.M{"enabled": false}}, false, false))

	// Create request body
	body := map[string]interface{}{
		"email":    "alex@krantz.dev",
		"password": sha256Hash("my-amazing-password"),
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/login", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user is not enabled"}`, response.Body.String())

	// Re-enable user
	assert.NoError(t, db.Collection("users").Update(bson.M{"_id": ownerUser.Id}, bson.M{"$set": bson.M{"enabled": true}}, false, false))
}
