package e2e

import (
	"bytes"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/verification"
	"go.mongodb.org/mongo-driver/bson"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
	"time"
)

var ownerUser database.User
var registrationFlowEmailVerificationToken = ""
var registrationFlowAuthenticationToken = ""

func TestRegistration(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":       "alex@krantz.dev",
		"password":    sha256Hash("my-amazing-password"),
		"first_name":  "Alex",
		"last_name":   "Krantz",
		"team_number": 100,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/register", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure team now registered
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 100}).Decode(&team))
	assert.True(t, *team.Registered)
	assert.False(t, *team.Verified)

	// Ensure user exists
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"email": "alex@krantz.dev"}).Decode(&ownerUser))
	assert.Equal(t, "alex@krantz.dev", ownerUser.Email)
	assert.Equal(t, "Alex Krantz", ownerUser.Name)
	assert.Equal(t, rbac.RoleOwner, ownerUser.Role)
	assert.False(t, ownerUser.Enabled)
	assert.Equal(t, team.Id.Hex(), ownerUser.TeamId)

	// Wait a bit for the email to get transferred
	time.Sleep(500 * time.Millisecond)

	// Ensure on-boarding email sent
	mailMutex.Lock()
	assert.Contains(t, mails, "team-registration")
	teamRegistrationEmail := mails["team-registration"]
	assert.Equal(t, ownerUser.Email, teamRegistrationEmail.Recipient)
	assert.Equal(t, ownerUser.Name, teamRegistrationEmail.Variables["name"])
	delete(mails, "team-registration")
	mailMutex.Unlock()

	// Ensure verification email sent
	mailMutex.Lock()
	assert.Contains(t, mails, "email-verification")
	emailVerification := mails["email-verification"]
	assert.Equal(t, ownerUser.Email, emailVerification.Recipient)
	assert.Equal(t, ownerUser.Name, emailVerification.Variables["name"])
	assert.Contains(t, emailVerification.Variables, "token")
	registrationFlowEmailVerificationToken = emailVerification.Variables["token"].(string)
	delete(mails, "email-verification")
	mailMutex.Unlock()
}

func TestRegistration_TeamAlreadyRegistered(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":       "alex@krantz.dev",
		"password":    sha256Hash("my-amazing-password"),
		"first_name":  "Alex",
		"last_name":   "Krantz",
		"team_number": 100,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/register", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusConflict, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team already has an account"}`, response.Body.String())
}

func TestRegistration_UserAlreadyRegistered(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":       "alex@krantz.dev",
		"password":    sha256Hash("my-amazing-password"),
		"first_name":  "Alex",
		"last_name":   "Krantz",
		"team_number": 973,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/register", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusConflict, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified email address is already in use"}`, response.Body.String())
}

func TestRegistration_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create request body
	body := map[string]interface{}{
		"email":       "email@dne.com",
		"password":    sha256Hash("my-amazing-password"),
		"first_name":  "Alex",
		"last_name":   "Krantz",
		"team_number": 0,
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("POST", "/authentication/register", "", nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestEmailVerification(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/verification/email", registrationFlowEmailVerificationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Token string `json:"token"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Ensure successful
	assert.Equal(t, "success", body.Status)
	assert.NotEmpty(t, body.Data.Token)
	registrationFlowAuthenticationToken = body.Data.Token

	// Ensure user is marked as enabled and verified email
	assert.NoError(t, db.Collection("users").FindOne(bson.M{"email": "alex@krantz.dev"}).Decode(&ownerUser))
	assert.True(t, ownerUser.Enabled)
	assert.True(t, ownerUser.VerifiedEmail)

	// Ensure only one token for user
	var tokens []database.Token
	result := db.Collection("tokens").FindMany(bson.M{"user_id": ownerUser.Id.Hex()})
	assert.NoError(t, result.All(&tokens))
	assert.Len(t, tokens, 1)
	assert.Equal(t, tokens[0].Type, database.TokenAuthentication)
}

func TestEmailVerification_FailureOnTokenReuse(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/verification/email", registrationFlowEmailVerificationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusUnauthorized, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "invalid token: failed to find signing key for token"}`, response.Body.String())
}

func TestGetTeamVerificationToken(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/verification/team", registrationFlowAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Key string `json:"key"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Ensure successful
	assert.Equal(t, "success", body.Status)
	assert.NotEmpty(t, body.Data.Key)

	// Ensure exists in database
	var v database.Verification
	assert.NoError(t, db.Collection("verifications").FindOne(bson.M{}).Decode(&v))

	// Verify hash
	token, err := base64.StdEncoding.DecodeString(body.Data.Key)
	assert.NoError(t, err)
	hash := sha512.New()
	hash.Write(token)
	assert.Equal(t, hex.EncodeToString(hash.Sum(nil)), v.Key)
}

func TestTeamVerification_ByPage(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create multipart body
	body, contentType := createForm(map[string]io.Reader{
		"type": bytes.NewBufferString(strconv.FormatInt(verification.ByPage, 10)),
		"page": bytes.NewBufferString("https://my-website.com/some-page"),
	})

	// Send request
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/verification/team", &body)
	request.Header.Set("Authorization", registrationFlowAuthenticationToken)
	request.Header.Set("Content-Type", contentType)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure exists in database
	var v database.Verification
	assert.NoError(t, db.Collection("verifications").FindOne(bson.M{}).Decode(&v))

	// Ensure expected values
	assert.Equal(t, verification.ByPage, v.Type)
	assert.Equal(t, "https://my-website.com/some-page", v.Source)
}

func TestTeamVerification_ByDNS(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create multipart body
	body, contentType := createForm(map[string]io.Reader{
		"type":   bytes.NewBufferString(strconv.FormatInt(verification.ByDNS, 10)),
		"domain": bytes.NewBufferString("my-website.com"),
	})

	// Send request
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/verification/team", &body)
	request.Header.Set("Authorization", registrationFlowAuthenticationToken)
	request.Header.Set("Content-Type", contentType)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure exists in database
	var v database.Verification
	assert.NoError(t, db.Collection("verifications").FindOne(bson.M{}).Decode(&v))

	// Ensure expected values
	assert.Equal(t, verification.ByDNS, v.Type)
	assert.Equal(t, "my-website.com", v.Source)
}

func TestTeamVerification_ByPhoto(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Open file
	file, err := os.OpenFile("./verification_image.png", os.O_RDONLY, 0755)
	assert.NoError(t, err)

	// Create multipart body
	body, contentType := createForm(map[string]io.Reader{
		"type":  bytes.NewBufferString(strconv.FormatInt(verification.ByPhoto, 10)),
		"image": file,
	})

	// Send request
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/verification/team", &body)
	request.Header.Set("Authorization", registrationFlowAuthenticationToken)
	request.Header.Set("Content-Type", contentType)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure exists in database
	var v database.Verification
	assert.NoError(t, db.Collection("verifications").FindOne(bson.M{}).Decode(&v))

	// Ensure expected values
	assert.Equal(t, verification.ByPhoto, v.Type)
	assert.Contains(t, v.Source, ownerUser.TeamId)
}
