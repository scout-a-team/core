package e2e

import (
	"flag"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	flag.Parse()

	// Create required resources if running integration tests
	if !testing.Short() {
		setupFramework()
	}

	// Run the tests
	result := m.Run()

	// Destroy created resources from integration tests
	if !testing.Short() {
		teardownFramework()
	}

	// Exit with given status code
	os.Exit(result)
}
