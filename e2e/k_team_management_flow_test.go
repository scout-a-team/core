package e2e

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"net/http/httptest"
	"testing"
)

var teamManagementFlowTeamId = ""

func TestSearchTeams_ByNumber(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/teams", ownerAuthenticationToken, map[string]string{"number": "100"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []struct {
			Number int    `json:"number"`
			Name   string `json:"name"`
			Id     string `json:"id"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
	team := body.Data[0]
	assert.Equal(t, 100, team.Number)
	assert.Equal(t, "The WildHats", team.Name)
	assert.NotEmpty(t, team.Id)
	teamManagementFlowTeamId = team.Id
}

func TestSearchTeams_ByName(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/teams", ownerAuthenticationToken, map[string]string{"name": "The WildHats"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string `json:"status"`
		Data   []struct {
			Number int    `json:"number"`
			Name   string `json:"name"`
			Id     string `json:"id"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
	team := body.Data[0]
	assert.Equal(t, 100, team.Number)
	assert.Equal(t, "The WildHats", team.Name)
	assert.NotEmpty(t, team.Id)
}

func TestSearchTeams_InvalidMethod(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/teams", ownerAuthenticationToken, map[string]string{"a": "b"}, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "query parameter 'name' or 'number' must be present"}`, response.Body.String())
}

func TestDescribeTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/teams/"+teamManagementFlowTeamId, ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string        `json:"status"`
		Data   database.Team `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, "The WildHats", body.Data.Name)
	assert.Equal(t, 100, body.Data.Number)
}

func TestDescribeTeam_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", "/teams/"+primitive.NewObjectID().Hex(), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestUpdateTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":    "Better Name",
		"country": "BC",
		"city":    "Vancouver",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", "/teams/"+teamManagementFlowTeamId, ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure properly updated
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 100}).Decode(&team))
	assert.Equal(t, "Better Name", team.Name)
	assert.Equal(t, "BC", team.Country)
	assert.Equal(t, "Vancouver", team.City)

	// Revert team info
	assert.NoError(t, db.Collection("teams").Update(bson.M{"number": 100}, bson.M{"$set": bson.M{"name": "The WildHats", "country": "US", "city": "Woodside"}}, false, false))
}

func TestUpdateTeam_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":    "Better Name",
		"country": "BC",
		"city":    "Vancouver",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", "/teams/"+primitive.NewObjectID().Hex(), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestUpdateTeam_InvalidPermissions(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Create body
	body := map[string]interface{}{
		"name":    "Better Name",
		"country": "BC",
		"city":    "Vancouver",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", "/teams/"+teamManagementFlowTeamId, secondaryAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestUpdateTeam_DifferentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve another team
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 114}).Decode(&team))

	// Create body
	body := map[string]interface{}{
		"name":    "Better Name",
		"country": "BC",
		"city":    "Vancouver",
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("PUT", "/teams/"+team.Id.Hex(), ownerAuthenticationToken, nil, body)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestTeamStats(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve another team
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 114}).Decode(&team))

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/stats", team.Id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string                       `json:"status"`
		Data   []database.EventMatchAverage `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.Len(t, body.Data, 1)
}

func TestTeamStats_NoEventData(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/stats", teamManagementFlowTeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestTeamStats_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/stats", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestTeamVerified(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/verified", teamManagementFlowTeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": {"verified": true}}`, response.Body.String())
}

func TestTeamVerified_UnverifiedTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Modify team
	id, _ := primitive.ObjectIDFromHex(teamManagementFlowTeamId)
	assert.NoError(t, db.Collection("teams").Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"verified": false}}, false, false))

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/verified", teamManagementFlowTeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": {"verified": false}}`, response.Body.String())

	// Revert team
	assert.NoError(t, db.Collection("teams").Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"verified": true}}, false, false))
}

func TestTeamVerified_NotPartOfTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve another team
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 114}).Decode(&team))

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/verified", team.Id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestTeamVerified_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/verified", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestTeamEvents(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve another team
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 114}).Decode(&team))

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/events", team.Id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": ["2020caln"]}`, response.Body.String())
}

func TestTeamEvents_NoEvents(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/events", teamManagementFlowTeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestTeamEvents_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/events", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestTeamMatches(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Retrieve another team
	var team database.Team
	assert.NoError(t, db.Collection("teams").FindOne(bson.M{"number": 114}).Decode(&team))

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/matches", team.Id.Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)

	// Parse response body
	var body struct {
		Status string   `json:"status"`
		Data   []string `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &body))

	// Validate response body
	assert.Equal(t, "success", body.Status)
	assert.NotEmpty(t, body.Data)
}

func TestTeamMatches_NoMatches(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/matches", teamManagementFlowTeamId), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestTeamMatches_NonExistentTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("GET", fmt.Sprintf("/teams/%s/matches", primitive.NewObjectID().Hex()), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusNotFound, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestDeleteTeam_NotPartOfTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/teams/"+primitive.NewObjectID().Hex(), ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestDeleteTeam_InvalidPermissions(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/teams/"+teamManagementFlowTeamId, secondaryAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusForbidden, response.Code)
	assert.JSONEq(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestDeleteTeam(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	// Send request
	response := httptest.NewRecorder()
	request := createRequest("DELETE", "/teams/"+teamManagementFlowTeamId, ownerAuthenticationToken, nil, nil)
	router.ServeHTTP(response, request)

	// Basic response validation
	assert.Equal(t, http.StatusOK, response.Code)
	assert.JSONEq(t, `{"status": "success"}`, response.Body.String())

	// Ensure no users
	var users []database.User
	assert.NoError(t, db.Collection("users").FindMany(bson.M{}).All(&users))
	assert.Len(t, users, 0)

	// Ensure no tokens
	var tokens []database.Token
	assert.NoError(t, db.Collection("tokens").FindMany(bson.M{}).All(&tokens))
	assert.Len(t, tokens, 1)
}
