package pit_scout

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func create(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	logger := zap.L().Named("scouting.pit.create").With(zap.String("method", "PUT"), zap.String("path", "/scouting/match/{event}"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourcePitScout, rbac.MethodCreate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate json
	var body database.PitScout
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	}

	// Ensure team exists
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Create team data if not exist
	pit, ok := team.Pit[user.TeamId]
	if !ok {
		pit = database.PitScout{}
	}

	// Update fields if given
	if pit.Weight != body.Weight && body.Weight != 0 {
		pit.Weight = body.Weight
	}
	if pit.Width != body.Width && body.Width != 0 {
		pit.Width = body.Width
	}
	if pit.Length != body.Length && body.Length != 0 {
		pit.Length = body.Length
	}
	if pit.DriveTrainMotorCount != body.DriveTrainMotorCount && body.DriveTrainMotorCount != 0 {
		pit.DriveTrainMotorCount = body.DriveTrainMotorCount
	}
	if pit.DriveTrainMotorType != body.DriveTrainMotorType && body.DriveTrainMotorType != "" {
		pit.DriveTrainMotorType = body.DriveTrainMotorType
	}
	if pit.DriveTrainType != body.DriveTrainType && body.DriveTrainType != "" {
		pit.DriveTrainType = body.DriveTrainType
	}
	if pit.WheelType != body.WheelType && body.WheelType != "" {
		pit.WheelType = body.WheelType
	}
	if pit.WheelDiameter != body.WheelDiameter && body.WheelDiameter != 0 {
		pit.WheelDiameter = body.WheelDiameter
	}
	if pit.ProgrammingLanguage != body.ProgrammingLanguage && body.ProgrammingLanguage != "" {
		pit.ProgrammingLanguage = body.ProgrammingLanguage
	}
	if pit.HasCamera != body.HasCamera {
		pit.HasCamera = body.HasCamera
	}
	if pit.Notes != body.Notes && body.Notes != "" {
		pit.Notes = body.Notes
	}

	// Game specific data
	if pit.GameSpecific.ClimbingMethod != body.GameSpecific.ClimbingMethod && body.GameSpecific.ClimbingMethod != "" {
		pit.GameSpecific.ClimbingMethod = body.GameSpecific.ClimbingMethod
	}
	if pit.GameSpecific.MoveOnBar != body.GameSpecific.MoveOnBar {
		pit.GameSpecific.MoveOnBar = body.GameSpecific.MoveOnBar
	}
	if pit.GameSpecific.CarryRobot != body.GameSpecific.CarryRobot {
		pit.GameSpecific.CarryRobot = body.GameSpecific.CarryRobot
	}
	if pit.GameSpecific.LowGoal != body.GameSpecific.LowGoal {
		pit.GameSpecific.LowGoal = body.GameSpecific.LowGoal
	}
	if pit.GameSpecific.OuterGoal != body.GameSpecific.OuterGoal {
		pit.GameSpecific.OuterGoal = body.GameSpecific.OuterGoal
	}
	if pit.GameSpecific.InnerGoal != body.GameSpecific.InnerGoal {
		pit.GameSpecific.InnerGoal = body.GameSpecific.InnerGoal
	}

	// Mark as completed if all data given
	pit.Completed = pit.Weight != 0 && pit.Width != 0 && pit.Length != 0 && pit.DriveTrainMotorCount != 0 && pit.DriveTrainMotorType != "" && pit.DriveTrainType != "" &&
		pit.WheelType != "" && pit.WheelDiameter != 0 && pit.ProgrammingLanguage != "" && pit.GameSpecific.ClimbingMethod != ""

	// Add back to map
	team.Pit[user.TeamId] = pit

	// Save the data
	if err := db.Collection("teams").Update(bson.M{"_id": teamId}, &team, false, true); err != nil {
		responses.Error(w, http.StatusInternalServerError, "failed to replace team data")
		return
	}

	responses.Success(w)
	logger.Info("modified team pit scout data for specified team")
}
