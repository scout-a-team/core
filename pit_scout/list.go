package pit_scout

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func list(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("scouting.pit.list").With(zap.String("method", "GET"), zap.String("path", "/scouting/pit"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourcePitScout, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Validate request method
	if r.Method != http.MethodGet {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	}

	// Find all teams
	var teams []database.Team
	cur := db.Collection("teams").FindMany(bson.M{})
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusInternalServerError, "no teams in database")
		return
	} else if err != nil {
		logger.Error("failed to query database for teams without pit data", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Unmarshal teams into array
	if err := cur.All(&teams); err != nil {
		logger.Error("failed to decode response into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	filter := r.URL.Query().Get("filter")
	if filter == "" {
		filter = "without"
	}

	// Condense team list to only have name and number
	var simpleTeam []map[string]interface{}
	for _, team := range teams {
		pit, ok := team.Pit[user.TeamId]
		if (ok && filter == "without") || (!ok && filter == "with") {
			continue
		} else if (filter == "without" && pit.Completed) || (filter == "with" && !pit.Completed) {
			continue
		}

		simpleTeam = append(simpleTeam, map[string]interface{}{
			"number": team.Number,
			"name":   team.Name,
		})
	}

	responses.SuccessWithData(w, simpleTeam)
	logger.Info("got list of team pit scouting data", zap.Bool("filtered", filter == "without" || filter == "with"))
}
