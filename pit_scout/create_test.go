package pit_scout

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          rbac.RoleOwner,
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestCreate(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("*database.Team"), false, true).Return(nil)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Pit = make(map[string]database.PitScout)
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight": 115.4,"width": 1,"length": 1,"drive_train_motor_count": 1,"drive_train_motor_type": "CIMs","drive_train_type": "Tank","wheel_type": "Traction","wheel_diameter": 6,"programming_language": "Java","camera": true,"notes": "some notes","game_specific": {"climber_type": "yeet","highest_level": 2,"carry_other_robot": true,"carry_robot_count": 1,"highest_carry": 1},"year": 2019}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestCreate_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight": 115.4,"width": 1,"length": 1,"drive_train_motor_count": 1,"drive_train_motor_type": "CIMs","drive_train_type": "Tank","wheel_type": "Traction","wheel_diameter": 6,"programming_language": "Java","camera": true,"notes": "some notes","game_specific": {"climber_type": "yeet","highest_level": 2,"carry_other_robot": true,"carry_robot_count": 1,"highest_carry": 1},"year": 2019}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestCreate_InvalidBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestCreate_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight": 115.4,"width": 1,"length": 1,"drive_train_motor_count": 1,"drive_train_motor_type": "CIMs","drive_train_type": "Tank","wheel_type": "Traction","wheel_diameter": 6,"programming_language": "Java","camera": true,"notes": "some notes","game_specific": {"climber_type": "yeet","highest_level": 2,"carry_other_robot": true,"carry_robot_count": 1,"highest_carry": 1},"year": 2019}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestCreate_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight": 115.4,"width": 1,"length": 1,"drive_train_motor_count": 1,"drive_train_motor_type": "CIMs","drive_train_type": "Tank","wheel_type": "Traction","wheel_diameter": 6,"programming_language": "Java","camera": true,"notes": "some notes","game_specific": {"climber_type": "yeet","highest_level": 2,"carry_other_robot": true,"carry_robot_count": 1,"highest_carry": 1},"year": 2019}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestCreate_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("*database.Team"), false, true).Return(errors.New("some database error"))
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Pit = make(map[string]database.PitScout)
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/{team}", bytes.NewBufferString(`{"weight": 115.4,"width": 1,"length": 1,"drive_train_motor_count": 1,"drive_train_motor_type": "CIMs","drive_train_type": "Tank","wheel_type": "Traction","wheel_diameter": 6,"programming_language": "Java","camera": true,"notes": "some notes","game_specific": {"climber_type": "yeet","highest_level": 2,"carry_other_robot": true,"carry_robot_count": 1,"highest_carry": 1},"year": 2019}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to replace team data"}`, response.Body.String())
}
