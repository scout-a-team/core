package pit_scout

import (
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/request_context"
)

// Add routes to main router
func Route(r chi.Router) {
	r.Get("/", list)

	r.Route("/{team}", func(r chi.Router) {
		r.Use(request_context.TeamIdCtx)
		r.Get("/", read)
		r.Put("/", create)
	})
}
