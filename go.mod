module gitlab.com/scout-a-team/core

go 1.13

require (
	github.com/akrantz01/go-tba v1.0.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/mailgun/mailgun-go/v3 v3.6.4
	github.com/mssola/user_agent v0.5.1
	github.com/pquerna/otp v1.2.0
	github.com/rs/cors v1.7.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/scout-a-team/trueskill v1.2.3
	go.mongodb.org/mongo-driver v1.1.2
	go.uber.org/zap v1.11.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
)
