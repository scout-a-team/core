package main

import (
	"context"
	"github.com/go-chi/chi"
	chimiddleware "github.com/go-chi/chi/middleware"
	"github.com/rs/cors"
	"gitlab.com/scout-a-team/core/authentication"
	"gitlab.com/scout-a-team/core/events"
	"gitlab.com/scout-a-team/core/importer"
	"gitlab.com/scout-a-team/core/match_scout"
	"gitlab.com/scout-a-team/core/metrics_processor"
	"gitlab.com/scout-a-team/core/pit_scout"
	"gitlab.com/scout-a-team/core/pkg/cache"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/middleware"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"gitlab.com/scout-a-team/core/statistics"
	"gitlab.com/scout-a-team/core/teams"
	"gitlab.com/scout-a-team/core/users"
	"gitlab.com/scout-a-team/core/verification"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var shutdown = make(chan os.Signal, 1)

func main() {
	// Load the configuration
	cfg := config.LoadConfig(false)

	// Create the logger
	logger := zap.L().Named("main")
	defer func() {
		if err := logger.Sync(); err != nil { /* Error never going to happen, appeasing the linter */
		}
	}()

	// Connect to database
	db := database.Connect(cfg)
	logger.Debug("connected to the database")

	// Connect to cache
	redis := cache.Connect(cfg)
	logger.Debug("connected to the cache")

	// Initialize mail API
	_, mailChannel, mailCancel := mail.Setup(cfg)

	// Create temporary files directory for verification if not exists
	if _, err := os.Stat("./files/verification"); os.IsNotExist(err) {
		if err := os.MkdirAll("./files/verification", os.ModePerm); err != nil {
			logger.Fatal("failed to create files verification directory")
		}
	}

	// Setup routes
	router := chi.NewRouter()

	// Add middlewares
	router.Use(request_context.Database(db))
	router.Use(request_context.Cache(redis))
	router.Use(request_context.Mail(mailChannel))
	router.Use(chimiddleware.RealIP)
	router.Use(chimiddleware.Recoverer)
	router.Use(middleware.Logging)
	router.Use(middleware.Authentication(db))
	router.Use(middleware.Verification(db))

	// Setup error handlers
	router.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
	})
	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusNotFound, "not found")
	})

	// Add authentication routes
	if !cfg.Modules.DisableAuthentication {
		router.Route("/authentication", authentication.Route)
	}

	// Add verification routes
	if !cfg.Modules.DisableVerification {
		router.Route("/verification", verification.Route)
	}

	// Add teams routes
	if !cfg.Modules.DisableTeams {
		router.Route("/teams", teams.Route)
	}

	// Add users routes
	if !cfg.Modules.DisableUsers {
		router.Route("/teams/{team}/users", users.Route)
	}

	// Add match scout routes
	if !cfg.Modules.DisableMatchScout {
		router.Route("/scouting/match", match_scout.Route)
	}

	// Add pit scout routes
	if !cfg.Modules.DisablePitScout {
		router.Route("/scouting/pit", pit_scout.Route)
	}

	// Add event routes
	if !cfg.Modules.DisableEvents {
		router.Route("/events", events.Route)
	}

	// Add statistics routes
	if !cfg.Modules.DisableStatistics {
		router.Route("/statistics", statistics.Route)
	}

	// Listen for OS shutdown signals
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	// Create http server
	server := &http.Server{
		Addr:         cfg.Http.Host + ":" + strconv.FormatInt(int64(cfg.Http.Port), 10),
		ReadTimeout:  time.Second * 15,
		WriteTimeout: time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      router,
	}

	// Optionally enable cors
	if cfg.Http.Cors {
		server.Handler = cors.AllowAll().Handler(router)
		logger.Debug("enabled cors")
	}

	// Start in separate goroutine
	go func() {
		logger.Info("started server listening and ready to accept connections", zap.String("host", cfg.Http.Host), zap.Int("port", cfg.Http.Port))
		if err := server.ListenAndServe(); err != nil {
			if strings.Contains(err.Error(), "Server closed") {
				return
			}
			logger.Fatal("unrecoverable error while server was booting", zap.Error(err))
		}
	}()

	// Setup data importer update workers
	updates := make(chan importer.Update, 25)
	importContext, importCancel, wg := importer.Setup(updates, cfg, db)

	// Setup metrics processing
	var metricsCancel context.CancelFunc
	if !cfg.Modules.DisableMetricsProcessing {
		metricsCancel = metrics_processor.Setup(cfg, db)
	}

	// Start data importing for events and teams
	if !cfg.Modules.DisableImporter {
		go importer.ImportTeams(cfg, wg, updates, importContext)
		go importer.ImportEvents(cfg, wg, updates, importContext)
	}

	<-shutdown
	logger.Info("shutdown signal received, attempting to gracefully stop server")

	// Create shutdown context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	// Shutdown the serve gracefully
	if err := server.Shutdown(ctx); err != nil {
		logger.Fatal("failed to gracefully shutdown the server", zap.Error(err))
	}

	// Shutdown data importing and wait until complete
	importCancel()
	logger.Info("sent cancellation signal to importer goroutines, waiting for jobs to finish")
	wg.Wait()

	// Shutdown mail daemon
	logger.Info("sent cancellation signal to mail daemon goroutine, waiting for jobs to finish")
	mailCancel()

	// Shutdown metrics processing daemons
	logger.Info("sent cancellation signal to metrics processing daemon goroutine, waiting for jobs to finish")
	if metricsCancel != nil {
		metricsCancel()
	}

	logger.Info("server is shutdown, goodbye")
}
