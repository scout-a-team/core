package importer

import (
	"context"
	"github.com/akrantz01/go-tba"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"sync"
	"time"
)

const tbaUrl = "https://www.thebluealliance.com/team/"

func ImportTeams(cfg config.Config, wg *sync.WaitGroup, updates chan Update, ctx context.Context) {
	// Decrement wait group on finish
	wg.Add(1)
	defer wg.Done()

	logger := zap.L().Named("importer.teams").With(zap.Bool("caching", cfg.Tba.Caching),
		zap.Int("refresh_interval", cfg.Tba.RefreshInterval))
	logger.Info("started team importer")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Refresh timer
	timer := time.After(time.Duration(cfg.Tba.RefreshInterval) * time.Second)

	for {
		select {
		// Refresh after specified interval
		case <-timer:
			logger.Info("started retrieval of team data...")

			// Set time to now if using caching
			if cfg.Tba.Caching {
				now := time.Now()
				reqOptions.LastModified = &now
			}

			// Iterate over team pages
			// Works until there are teams with numbers above 10000
			// As of Oct 2019, the highest number is 8284
			for page := 0; page < 20; page++ {
				// Request teams
				teams, status, err := tba.ListTeams(int64(page), cfg.Tba.ApiKey, &reqOptions)
				if err != nil {
					logger.Error("failed to retrieve team list", zap.Int("status_code", status), zap.Error(err))
					break
				}

				// Stop if no data changed
				if cfg.Tba.Caching && status == http.StatusNotModified {
					break
				}

				// Ensure there are teams to be added
				if len(teams) == 0 {
					break
				}

				// Take relevant data and upsert into database
				for _, t := range teams {
					team := database.Team{
						Name:    t.Nickname,
						Number:  t.TeamNumber,
						City:    t.City,
						Country: t.Country,
						TbaUrl:  tbaUrl + strconv.FormatInt(int64(t.TeamNumber), 10),
					}

					// Update or insert into database
					updates <- Update{
						Data:       team,
						Query:      bson.M{"number": t.TeamNumber},
						Upsert:     true,
						Collection: "teams",
					}
				}

				logger.Debug("retrieved data for page", zap.Int("page", page))
			}

			logger.Info("finished retrieval of team data")

		// Exit the loop
		case <-ctx.Done():
			logger.Info("stopped team importer")
			return
		}
	}
}
