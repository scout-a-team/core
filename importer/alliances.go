package importer

import (
	"github.com/akrantz01/go-tba"
	"github.com/akrantz01/go-tba/responses"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.uber.org/zap"
	"net/http"
	"time"
)

// Generate a list of the alliances for an event
func generateAlliances(event string, cfg config.Config) []database.Alliance {
	logger := zap.L().Named("importer.alliances").With(zap.String("event", event))
	logger.Debug("started retrieval of alliance data...")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Set time to now if using caching
	if cfg.Tba.Caching {
		now := time.Now()
		reqOptions.LastModified = &now
	}

	// Request rankings
	eventAlliances, status, err := tba.EventAlliances(event, cfg.Tba.ApiKey, &reqOptions)
	if err != nil {
		logger.Error("failed to retrieve alliances for event", zap.Int("status_code", status), zap.Error(err))
		return nil
	}

	// Stop if no data changed
	if cfg.Tba.Caching && status == http.StatusNotModified {
		return nil
	}

	// Take relevant data add to insertable array
	var alliances []database.Alliance
	for _, a := range eventAlliances {
		// Get relevant information
		alliance := database.Alliance{
			Name:    a.Name,
			TeamIds: a.Picks,
			Status:  "unknown",
			Wins:    -1,
			Ties:    -1,
			Losses:  -1,
		}

		// Add status information if found
		if status, ok := a.Status.(responses.EventAllianceStatus); ok {
			alliance.Status = status.Status
			alliance.Wins = status.Record.Wins
			alliance.Ties = status.Record.Ties
			alliance.Losses = status.Record.Losses
		}

		alliances = append(alliances, alliance)
	}

	logger.Debug("finished retrieval of alliance data")
	return alliances
}
