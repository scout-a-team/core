package importer

import (
	"github.com/akrantz01/go-tba"
	"github.com/akrantz01/go-tba/responses"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
	"sync"
	"time"
)

func ImportMatches(event string, cfg config.Config, wg *sync.WaitGroup, updates chan Update) {
	// Decrement wait group on finish
	wg.Add(1)
	defer wg.Done()

	logger := zap.L().Named("importer.match").With(zap.String("event", event))
	logger.Debug("started retrieval of match data...")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Set time to now if using caching
	if cfg.Tba.Caching {
		now := time.Now()
		reqOptions.LastModified = &now
	}

	// Request matches
	matches, status, err := tba.EventMatches(event, cfg.Tba.ApiKey, &reqOptions)
	if err != nil {
		logger.Error("failed to retrieve event list", zap.Int("status_code", status), zap.Error(err))
		return
	}

	// Stop if no data changed
	if cfg.Tba.Caching && status == http.StatusNotModified {
		return
	}

	for _, m := range matches {
		// Parse time strings
		scheduled := time.Unix(m.PredictedTime, 0)
		happened := time.Unix(m.ActualTime, 0)

		// Convert match type string to enum value
		var matchType int
		switch m.CompetitionLevel {
		case "qm":
			matchType = database.MatchQualification
		case "qf":
			matchType = database.MatchQuarterFinal
		case "sf":
			matchType = database.MatchSemiFinal
		case "f":
			matchType = database.MatchFinal
		}

		// Convert team string to enum value
		var winner int
		switch m.WinningAlliance {
		case "blue":
			winner = database.WinAllianceBlue

		case "red":
			winner = database.WinAllianceRed

		default:
			winner = database.WinAllianceUnknown
		}

		// Retrieve relevant info
		match := database.Match{
			EventId:        event,
			Key:            m.Key,
			ScheduledAt:    scheduled,
			HappenedAt:     happened,
			MatchType:      matchType,
			MatchSetNumber: m.SetNumber,
			MatchNumber:    m.MatchNumber,
			Winner:         winner,
			RedAlliance: database.TeamAlliance{
				Team1: m.Alliances.Red.TeamKeys[0],
				Team2: m.Alliances.Red.TeamKeys[1],
				Team3: m.Alliances.Red.TeamKeys[2],
			},
			BlueAlliance: database.TeamAlliance{
				Team1: m.Alliances.Blue.TeamKeys[0],
				Team2: m.Alliances.Blue.TeamKeys[1],
				Team3: m.Alliances.Blue.TeamKeys[2],
			},
			RedScore:  m.Alliances.Red.Score,
			BlueScore: m.Alliances.Blue.Score,
		}

		// Get scoring breakdown data
		if score, ok := m.ScoreBreakdown.(responses.ScoringBreakdown2020); ok {
			match.RedAlliance.ScoreBreakdown = generateScoringDocument(score.Red)
			match.BlueAlliance.ScoreBreakdown = generateScoringDocument(score.Blue)
		}

		// Update or insert into database
		updates <- Update{
			Data:       match,
			Query:      bson.M{"key": m.Key},
			Upsert:     true,
			Collection: "matches",
		}
	}

	logger.Debug("finished retrieval of match data")
}

// Convert the scoring breakdown data into insertable format
func generateScoringDocument(score responses.ScoringBreakdown2020Alliance) database.ScoringBreakdown {
	return database.ScoringBreakdown{
		InitLineRobot: []int{
			convertInitLineString(score.InitLineRobot1),
			convertInitLineString(score.InitLineRobot2),
			convertInitLineString(score.InitLineRobot3),
		},
		EndgameRobot: []int{
			convertEndgameString(score.EndgameRobot1),
			convertEndgameString(score.EndgameRobot2),
			convertEndgameString(score.EndgameRobot3),
		},
		AutoCellsBottom:               score.AutoCellsBottom,
		AutoCellsOuter:                score.AutoCellsOuter,
		AutoCellsInner:                score.AutoCellsInner,
		TeleopCellsBottom:             score.TeleopCellsBottom,
		TeleopCellsOuter:              score.TeleopCellsOuter,
		TeleopCellsInner:              score.TeleopCellsInner,
		Stage1Activated:               score.Stage1Activated,
		Stage2Activated:               score.Stage2Activated,
		Stage3Activated:               score.Stage3Activated,
		EndgameRungLevel:              convertEndgameLevel(score.EndgameRungIsLevel),
		AutoInitLinePoints:            score.AutoInitLinePoints,
		AutoCellPoints:                score.AutoCellPoints,
		TeleopCellPoints:              score.TeleopCellPoints,
		ControlPanelPoints:            score.ControlPanelPoints,
		AutoPoints:                    score.AutoPoints,
		TeleopPoints:                  score.TeleopPoints,
		EndgamePoints:                 score.EndgamePoints,
		AdjustPoints:                  score.AdjustPoints,
		FoulPoints:                    score.FoulPoints,
		TotalPoints:                   score.TotalPoints,
		ShieldOperationalRankingPoint: score.ShieldOperationalRankingPoint,
		ShieldEnergizedRankingPoint:   score.ShieldEnergizedRankingPoint,
		RankingPoints:                 score.RankingPoints,
		FoulCount:                     score.FoulPoints,
		TechFoulCount:                 score.TechFoulCount,
	}
}

func convertInitLineString(status string) int {
	switch status {
	case "None":
		return database.InitiationLineNone
	case "Exited":
		return database.InitiationLineExited
	default:
		return database.InitiationLineUnknown
	}
}

func convertEndgameString(status string) int {
	switch status {
	case "None":
		return database.EndgameNone
	case "Park":
		return database.EndgamePark
	case "Hang":
		return database.EndgameHang
	default:
		return database.EndgameUnknown
	}
}

func convertEndgameLevel(status string) bool {
	return status == "IsLevel"
}
