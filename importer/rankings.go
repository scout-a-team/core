package importer

import (
	"github.com/akrantz01/go-tba"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.uber.org/zap"
	"net/http"
	"time"
)

// Generate a list of the rankings for an event
func generateRankings(event string, cfg config.Config) []database.EventRanking {
	logger := zap.L().Named("importer.rankings").With(zap.String("event", event))
	logger.Debug("started retrieval of ranking data...")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Set time to now if using caching
	if cfg.Tba.Caching {
		now := time.Now()
		reqOptions.LastModified = &now
	}

	// Request rankings
	eventRankings, status, err := tba.EventRankings(event, cfg.Tba.ApiKey, &reqOptions)
	if err != nil {
		logger.Error("failed to retrieve event list", zap.Int("status_code", status), zap.Error(err))
		return nil
	}

	// Stop if no data changed
	if cfg.Tba.Caching && status == http.StatusNotModified {
		return nil
	}

	// Take relevant data add to insertable array
	var ranks []database.EventRanking
	for _, r := range eventRankings.Rankings {
		ranks = append(ranks, database.EventRanking{
			EventId:            event,
			TeamId:             r.TeamKey,
			Rank:               r.Rank,
			MatchesPlayed:      r.MatchesPlayed,
			Disqualifications:  r.Disqualifications,
			TotalRankingPoints: r.ExtraStats[0],
			RankingScore:       r.SortOrders[0],
		})
	}

	logger.Debug("finished retrieval of ranking data")
	return ranks
}
