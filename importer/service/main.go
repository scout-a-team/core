package main

import (
	"gitlab.com/scout-a-team/core/importer"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

var shutdown = make(chan os.Signal, 1)

func main() {
	// Load the configuration
	cfg := config.LoadConfig(false)

	// Create the logger
	logger := zap.L().Named("main")
	defer func() {
		if err := logger.Sync(); err != nil { /* Error never going to happen, appeasing the linter */
		}
	}()

	// Connect to database server
	db := database.Connect(cfg)
	logger.Debug("connected to the database")

	// Create the updater workers
	updates := make(chan importer.Update, 25)
	ctx, cancel, wg := importer.Setup(updates, cfg, db)
	logger.Info("started workers for data import", zap.Int("count", cfg.Database.ImportWorkers))

	// Start events importer
	go importer.ImportEvents(cfg, wg, updates, ctx)

	// Start teams importer
	go importer.ImportTeams(cfg, wg, updates, ctx)

	// Listen for OS shutdown signals
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	<-shutdown
	logger.Info("shutdown signal received, attempting to gracefully stop server...")

	cancel()
	logger.Info("sent cancellation signal to goroutines, waiting for jobs to finish...")

	wg.Wait()
	logger.Info("server is shutdown, goodbye")
}
