package importer

import (
	"context"
	"github.com/akrantz01/go-tba"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
	"sync"
	"time"
)

func ImportEvents(cfg config.Config, wg *sync.WaitGroup, updates chan Update, ctx context.Context) {
	// Decrement wait group on finish
	wg.Add(1)
	defer wg.Done()

	logger := zap.L().Named("importer.events").With(zap.Bool("caching", cfg.Tba.Caching),
		zap.Int("refresh_interval", cfg.Tba.RefreshInterval))
	logger.Info("started event importer")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Refresh timer
	timer := time.After(time.Duration(cfg.Tba.RefreshInterval) * time.Second)

	for {
		select {
		// Refresh after specified interval
		case <-timer:
			logger.Info("started retrieval of event data...")

			// Set time to now if using caching
			if cfg.Tba.Caching {
				now := time.Now()
				reqOptions.LastModified = &now
			}

			// Request events
			events, status, err := tba.ListEvents(2020, cfg.Tba.ApiKey, &reqOptions)
			if err != nil {
				logger.Error("failed to retrieve event list", zap.Int("status_code", status), zap.Error(err))
				break
			}

			// Stop if no data changed
			if cfg.Tba.Caching && status == http.StatusNotModified {
				break
			}

			// Take relevant data and upsert into database
			for _, e := range events {
				// Parse date strings
				startDate, _ := time.Parse("2006-01-02", e.StartDate)
				endDate, _ := time.Parse("2006-01-02", e.EndDate)

				// Retrieve relevant info
				event := database.Event{
					Name:          e.Name,
					Key:           e.Key,
					Start:         startDate,
					End:           endDate,
					Year:          2020,
					City:          e.City,
					Country:       e.Country,
					StateProvince: e.StateProvince,
					Rankings:      generateRankings(e.Key, cfg),
					Alliances:     generateAlliances(e.Key, cfg),
					TeamIds:       generateTeams(e.Key, cfg),
					MatchIds:      generateMatches(e.Key, cfg),
				}

				// Update or insert into database
				updates <- Update{
					Data:       event,
					Query:      bson.M{"key": e.Key},
					Upsert:     true,
					Collection: "events",
				}

				// Begin importing of match data for event
				go ImportMatches(e.Key, cfg, wg, updates)
			}

			logger.Info("finished retrieval of event data")

		// Exit the loop
		case <-ctx.Done():
			logger.Info("stopped event importer")
			return
		}
	}
}

// Generate a list of team keys for a specified event
func generateTeams(event string, cfg config.Config) []string {
	logger := zap.L().Named("importer.events").With(zap.String("event", event))
	logger.Debug("started retrieval of team list...")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Set time to now if using caching
	if cfg.Tba.Caching {
		now := time.Now()
		reqOptions.LastModified = &now
	}

	// Request teams in event
	teams, status, err := tba.EventTeamKeys(event, cfg.Tba.ApiKey, &reqOptions)
	if err != nil {
		logger.Error("failed to retrieve teams list for event", zap.Int("status_code", status), zap.Error(err))
		return nil
	}

	// Stop if no data changed
	if cfg.Tba.Caching && status == http.StatusNotModified {
		return nil
	}

	return teams
}

// Generate a list of match keys for a specified event
func generateMatches(event string, cfg config.Config) []string {
	logger := zap.L().Named("importer.events").With(zap.String("event", event))
	logger.Debug("started retrieval of event list...")

	// Create request options
	reqOptions := tba.RequestOptions{
		UserAgent: &cfg.Tba.UserAgent,
	}

	// Set time to now if using caching
	if cfg.Tba.Caching {
		now := time.Now()
		reqOptions.LastModified = &now
	}

	// Request teams in event
	matches, status, err := tba.EventMatchKeys(event, cfg.Tba.ApiKey, &reqOptions)
	if err != nil {
		logger.Error("failed to retrieve teams list for event", zap.Int("status_code", status), zap.Error(err))
		return nil
	}

	// Stop if no data changed
	if cfg.Tba.Caching && status == http.StatusNotModified {
		return nil
	}

	return matches
}
