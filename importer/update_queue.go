package importer

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"sync"
)

// Data to be updated in the database
type Update struct {
	Data       interface{}
	Query      bson.M
	Upsert     bool
	Collection string
}

// Worker to buffer updates to database to prevent request_context deadlines exceeding and io failures
func UpdateWorker(updates chan Update, db database.Database, wg *sync.WaitGroup, ctx context.Context) {
	// Add self to waitgroup
	wg.Add(1)
	defer wg.Done()

	for {
		select {
		// Run the update
		case update := <-updates:
			if err := db.Collection(update.Collection).Update(update.Query, bson.M{"$set": update.Data}, update.Upsert, false); err != nil {
				zap.L().Named("importer.worker").With(zap.Error(err), zap.Any("filter", update.Query)).Error("failed to replace entry in database")
			}

		// Halt after cancellation
		case <-ctx.Done():
			return
		}
	}
}
