package importer

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"sync"
)

// Setup the updater workers for data importing
func Setup(updates chan Update, cfg config.Config, db database.Database) (ctx context.Context, cancel context.CancelFunc, wg *sync.WaitGroup) {
	// Create wait group for graceful shutdown
	wg = &sync.WaitGroup{}

	// Create shutdown request_context
	ctx, cancel = context.WithCancel(context.Background())

	// Start the workers
	if !cfg.Modules.DisableImporter {
		for w := 0; w < cfg.Database.ImportWorkers; w++ {
			go UpdateWorker(updates, db, wg, ctx)
		}
	}

	return
}
