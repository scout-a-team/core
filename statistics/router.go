package statistics

import (
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/request_context"
)

// Add routes to main router
func Route(r chi.Router) {
	r.Get("/events", events)
	r.Route("/events/{event}", func(r chi.Router) {
		r.Use(request_context.EventKeyCtx)
		r.Get("/matches", matches)
	})
	r.Get("/rankings/skill", rankingByTrueSkill)
	r.Get("/rankings/points", rankingByPoints)
}
