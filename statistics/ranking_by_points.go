package statistics

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"strings"
)

func rankingByPoints(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("statistics.rankings.points").With(zap.String("method", "GET"), zap.String("path", "/stats/rankings/points"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceStatistics, rbac.MethodDescribe) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// TODO: add rankings by ranking points across events

	// Retrieve event from query params
	eventKey := r.URL.Query().Get("event")
	if eventKey == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'event' is required")
		return
	}

	// Get requested event
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event", zap.Error(err), zap.String("event", eventKey))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	ranks := make(float642DArray, len(event.TeamIds))
	for i, rank := range event.Rankings {
		teamNum, _ := strconv.ParseInt(strings.TrimPrefix(rank.TeamId, "frc"), 10, 64)
		ranks[i] = [2]float64{float64(teamNum), rank.RankingScore}
	}

	responses.SuccessWithData(w, ranks)
	logger.Info("sent ranking points rankings", zap.String("event", eventKey))
}
