package statistics

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

func genConfig() config.Config {
	cfg := config.Config{}
	cfg.TrueSkill.Sigma = 25
	cfg.TrueSkill.Mu = 8
	return cfg
}

func TestRankingsByTrueSkill(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(nil)
	result.On("All", mock.AnythingOfType("*[]database.Team")).Return(nil).Run(func(args mock.Arguments) {
		teams := reflect.ValueOf(args.Get(0))
		sliceVal := teams.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem(), reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.Team{
			Number: 1,
			TeamTrueSkill: map[string]database.TrueSkill{
				user.TeamId: {
					Mu:    23.329,
					Sigma: 7.3333,
				},
			},
		}))
		sliceVal.Index(1).Set(reflect.ValueOf(database.Team{
			Number: 2,
			TeamTrueSkill: map[string]database.TrueSkill{
				user.TeamId: {
					Mu:    27.329,
					Sigma: 7.3333,
				},
			},
		}))
		teams.Elem().Set(sliceVal.Slice(0, 2))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	rankingByTrueSkill(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [[1,20.982344],[2,24.982344]]}`, response.Body.String())
}

func TestRankingsByTrueSkill_WithGlobal(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(nil)
	result.On("All", mock.AnythingOfType("*[]database.Team")).Return(nil).Run(func(args mock.Arguments) {
		teams := reflect.ValueOf(args.Get(0))
		sliceVal := teams.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem(), reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.Team{
			Number: 1,
			GlobalTrueSkill: database.TrueSkill{
				Mu:    23.329,
				Sigma: 7.3333,
			},
		}))
		sliceVal.Index(1).Set(reflect.ValueOf(database.Team{
			Number: 2,
			GlobalTrueSkill: database.TrueSkill{
				Mu:    27.329,
				Sigma: 7.3333,
			},
		}))
		teams.Elem().Set(sliceVal.Slice(0, 2))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	query := request.URL.Query()
	query.Set("global", "yes")
	request.URL.RawQuery = query.Encode()

	// Run request
	rankingByTrueSkill(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [[1,20.982344],[2,24.982344]]}`, response.Body.String())
}

func TestRankingsByTrueSkill_NoTeamsPresent(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	rankingByTrueSkill(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "no teams present in database"}`, response.Body.String())
}

func TestRankingsByTrueSkill_FindTeamsError(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	rankingByTrueSkill(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestRankingsByTrueSkill_DecodeError(t *testing.T) {
	// Setup facilities
	cfg := genConfig()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(nil)
	result.On("All", mock.AnythingOfType("*[]database.Team")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityConfig, cfg))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	rankingByTrueSkill(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
