package statistics

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"gitlab.com/scout-a-team/trueskill"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"sort"
)

type float642DArray [][2]float64

func (a float642DArray) Len() int {
	return len(a)
}

func (a float642DArray) Less(i, j int) bool {
	return a[i][1] < a[j][1]
}

func (a float642DArray) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func rankingByTrueSkill(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	cfg := request_context.GetConfigFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("statistics.rankings.skill").With(zap.String("method", "GET"), zap.String("path", "/stats/rankings/skill"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceStatistics, rbac.MethodDescribe) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Get all teams
	cur := db.Collection("teams").FindMany(bson.M{})
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusInternalServerError, "no teams present in database")
		return
	} else if err != nil {
		logger.Error("failed to query database for all teams", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode database response
	var teams []database.Team
	if err := cur.All(&teams); err != nil {
		logger.Error("failed to decode database response into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Choose to use global or team trueskills in rankings
	trueskills := make(float642DArray, len(teams))
	if r.URL.Query().Get("global") == "yes" {
		for i, team := range teams {
			trueskills[i] = [2]float64{float64(team.Number), trueskill.Expose(team.GlobalTrueSkill.Mu, team.GlobalTrueSkill.Sigma, cfg.TrueSkill.Mu, cfg.TrueSkill.Sigma)}
		}
	} else {
		for i, team := range teams {
			trueskills[i] = [2]float64{float64(team.Number), trueskill.Expose(team.TeamTrueSkill[user.TeamId].Mu, team.TeamTrueSkill[user.TeamId].Sigma, cfg.TrueSkill.Mu, cfg.TrueSkill.Sigma)}
		}
	}

	// Sort teams
	sort.Sort(trueskills)

	responses.SuccessWithData(w, trueskills)
	logger.Info("sent trueskill rankings", zap.Bool("global", r.URL.Query().Get("global") == "yes"))
}
