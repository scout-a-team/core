package statistics

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestMatches(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchScoutResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchScoutCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchScoutResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).TeamIds = []string{"frc1"}
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})
	matchScoutResult.On("Err").Return(nil)
	matchScoutResult.On("All", mock.AnythingOfType("*[]database.TeamMatchData")).Return(nil).Run(func(args mock.Arguments) {
		matchData := reflect.ValueOf(args.Get(0))
		sliceVal := matchData.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.TeamMatchData{
			MatchId: "some-id",
			TeamId:  "frc1",
		}))
		matchData.Elem().Set(sliceVal.Slice(0, 1))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [{"match":"some-id","team":"frc1","total_shots":0,"low_shots":0,"outer_shots":0,"inner_shots":0,"shot_locations":null,"low_shot_locations":null,"outer_shot_locations":null,"inner_shot_locations":null,"overall_accuracy":0,"auto_accuracy":0,"teleop_accuracy":0,"low_accuracy_auto":0,"low_accuracy_teleop":0,"low_accuracy_cumulative":0,"outer_accuracy_auto":0,"outer_accuracy_teleop":0,"outer_accuracy_cumulative":0,"inner_accuracy_auto":0,"inner_accuracy_teleop":0,"inner_accuracy_cumulative":0,"low_cycle_times":null,"low_cycle_times_average":0,"outer_cycle_times":null,"outer_cycle_times_average":0,"inner_cycle_times":null,"inner_cycle_times_average":0,"control_panel_stage_2_accuracy":0,"control_panel_stage_2_duration":0,"control_panel_stage_2_successful":false,"control_panel_stage_3_accuracy":0,"control_panel_stage_3_duration":0,"control_panel_stage_3_successful":false,"total_blocks":0,"block_accuracy":0,"total_malfunctions":0,"malfunction_time":0,"malfunctions":null,"climb_time":0,"climb_accuracy":0,"climb_successful":false,"total_collected":0,"collection_accuracy":0,"collection_locations":null,"total_push_matches":0,"total_push_wins":0,"total_push_time":0}]}`, response.Body.String())
}

func TestMatches_NoQueryParameter(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'team' is required"}`, response.Body.String())
}

func TestMatches_InvalidInteger(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "a")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'team' must be an integer"}`, response.Body.String())
}

func TestMatches_NegativeInteger(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "-1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'team' must be a positive integer"}`, response.Body.String())
}

func TestMatches_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestMatches_FindEventDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatches_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestMatches_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatches_TeamNotInEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).TeamIds = []string{"frc2"}
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team not in event"}`, response.Body.String())
}

func TestMatches_NoScoutedData(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchScoutResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchScoutCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchScoutResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).TeamIds = []string{"frc1"}
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})
	matchScoutResult.On("Err").Return(nil)
	matchScoutResult.On("All", mock.AnythingOfType("*[]database.TeamMatchData")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestMatches_FindMatchDataDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchScoutResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchScoutCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchScoutResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).TeamIds = []string{"frc1"}
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})
	matchScoutResult.On("Err").Return(nil)
	matchScoutResult.On("All", mock.AnythingOfType("*[]database.TeamMatchData")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatches_DecodeError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchScoutResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchScoutCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchScoutResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).TeamIds = []string{"frc1"}
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})
	matchScoutResult.On("Err").Return(nil)
	matchScoutResult.On("All", mock.AnythingOfType("*[]database.TeamMatchData")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	query := request.URL.Query()
	query.Set("team", "1")
	request.URL.RawQuery = query.Encode()

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
