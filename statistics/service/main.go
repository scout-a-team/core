package main

import (
	"context"
	"github.com/go-chi/chi"
	chimiddleare "github.com/go-chi/chi/middleware"
	"github.com/rs/cors"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/middleware"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"gitlab.com/scout-a-team/core/statistics"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var shutdown = make(chan os.Signal, 1)

func main() {
	// Load the configuration
	cfg := config.LoadConfig(false)

	// Create the logger
	logger := zap.L().Named("main")
	defer func() {
		if err := logger.Sync(); err != nil { /* Error never going to happen, appeasing the linter */
		}
	}()

	// Connect to database
	db := database.Connect(cfg)
	logger.Debug("connected to the database")

	// Setup routes
	router := chi.NewRouter()

	// Add middlewares
	router.Use(request_context.Database(db))
	router.Use(chimiddleare.RealIP)
	router.Use(chimiddleare.Recoverer)
	router.Use(middleware.Logging)
	router.Use(middleware.Authentication(db))
	router.Use(middleware.Verification(db))

	// Setup error handlers
	router.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
	})
	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		responses.Error(w, http.StatusNotFound, "not found")
	})

	// Add statistics routes
	statistics.Route(router)

	// Listen for OS shutdown signal
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	// Create http server
	server := &http.Server{
		Addr:         cfg.Http.Host + ":" + strconv.FormatInt(int64(cfg.Http.Port), 10),
		ReadTimeout:  time.Second * 15,
		WriteTimeout: time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      router,
	}

	// Optionally enable cors
	if cfg.Http.Cors {
		server.Handler = cors.AllowAll().Handler(router)
		logger.Debug("enabled cors")
	}

	// Start in separate goroutine
	go func() {
		logger.Info("started server listening and ready to accept connections", zap.String("host", cfg.Http.Host), zap.Int("port", cfg.Http.Port))
		if err := server.ListenAndServe(); err != nil {
			if strings.Contains(err.Error(), "Server closed") {
				return
			}
			logger.Fatal("unrecoverable error while server was booting", zap.Error(err))
		}
	}()

	<-shutdown
	logger.Info("shutdown signal received, attempting to gracefully stop server")

	// Create shutdown context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	// Shutdown the server gracefully
	if err := server.Shutdown(ctx); err != nil {
		logger.Fatal("failed to gracefully shutdown the server", zap.Error(err))
	}
	logger.Info("server is shutdown, goodbye")
}
