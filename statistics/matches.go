package statistics

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

func matches(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("statistics.matches").With(zap.String("method", "GET"), zap.String("path", "/stats/events/{event}/matches"),
		zap.String("event", eventKey), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceStatistics, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Validate request on query parameters
	if len(r.URL.RawQuery) == 0 || r.URL.Query().Get("team") == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'team' is required")
		return
	}

	// Convert team string to integer
	teamNum, err := strconv.ParseInt(r.URL.Query().Get("team"), 10, 64)
	if err != nil {
		responses.Error(w, http.StatusBadRequest, "query parameter 'team' must be an integer")
		return
	} else if teamNum <= 0 {
		responses.Error(w, http.StatusBadRequest, "query parameter 'team' must be a positive integer")
		return
	}
	logger = logger.With(zap.Int64("team", teamNum))

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event existence", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure team exists
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"number": teamNum}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure team in event
	valid := false
	specifiedTeam := "frc" + strconv.Itoa(team.Number)
	for _, tid := range event.TeamIds {
		if specifiedTeam == tid {
			valid = true
			break
		}
	}
	if !valid {
		responses.Error(w, http.StatusBadRequest, "specified team not in event")
		return
	}

	// Get all raw scouted data for a match
	cur := db.Collection("team_match_data").FindMany(bson.M{"team": specifiedTeam})
	if err := cur.Err(); err != nil {
		logger.Error("failed to query database for raw match scout data", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode raw data into array
	var data []database.TeamMatchData
	if err := cur.All(&data); err != nil {
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		logger.Error("failed to decode response into array", zap.Error(err))
		return
	}

	// Return empty of no data
	if data == nil {
		responses.SuccessWithData(w, []string{})
		return
	}

	responses.SuccessWithData(w, data)
	logger.Info("got scouted data for all matches at event")
}
