package authentication

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestForgotPassword(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Email = "some@user.com"
		user.Name = "Some User"
		user.Id = primitive.NewObjectID()
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/forgot-password?email=some@user.com", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestForgotPassword_InvalidMethod(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/forgot-password?email=some@user.com", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestForgotPassword_NoQueryParameter(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/forgot-password", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'email' is required"}`, response.Body.String())
}

func TestForgotPassword_NonExistentUser(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/forgot-password?email=some@user.com", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestForgotPassword_FindUserDatabaseError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/forgot-password?email=some@user.com", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestForgotPassword_TokenGenerationError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("some database error"))
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Email = "some@user.com"
		user.Name = "Some User"
		user.Id = primitive.NewObjectID()
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/forgot-password?email=some@user.com", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	forgotPassword(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate token"}`, response.Body.String())
}
