package authentication

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
	"time"
)

func TestListTokens(t *testing.T) {
	id := primitive.NewObjectID()
	now := time.Now().UTC()

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "tokens").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(nil)
	result.On("Next").Return(true).Once()
	result.On("Next").Return(false)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		token := args.Get(0).(*database.Token)
		token.Id = id
		token.OS = "os"
		token.Browser = "browser"
		token.IssuedAt = now
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/tokens", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	listTokens(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [{"id":"`+id.Hex()+`","os":"os","browser":"browser","issued_at":"`+now.Format("2006-01-02T15:04:05.999999999Z")+`"}]}`, response.Body.String())
}

func TestListTokens_NoTokens(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "tokens").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/tokens", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	listTokens(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestListTokens_DatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "tokens").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/tokens", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	listTokens(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestListTokens_DecodeError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "tokens").Return(collection)
	collection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Err").Return(nil)
	result.On("Next").Return(true)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/tokens", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	listTokens(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to decode tokens"}`, response.Body.String())
}
