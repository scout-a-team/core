package authentication

import (
	"github.com/go-chi/chi"
)

// Add routes to main router
func Route(r chi.Router) {
	// User authentication
	r.Post("/register", register)
	r.Post("/login", login)
	r.Get("/logout", logout)
	r.Get("/forgot-password", forgotPassword)

	// Two factor authentication
	r.Get("/2fa/totp", totp)

	// Token management
	r.Get("/tokens", listTokens)
	r.Delete("/tokens", deleteToken)
}
