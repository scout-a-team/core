package authentication

import (
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func logout(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)

	logger := zap.L().Named("authentication.logout").With(zap.String("method", "GET"), zap.String("path", "/authentication/logout"),
		zap.String("authenticated_user", request_context.GetTokenUser(r).Id.Hex()))

	// Validate request method
	if r.Method != "GET" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	}

	// Delete authentication token signing key
	if err := db.Collection("tokens").DeleteOne(bson.M{"_id": request_context.GetTokenId(r)}); err != nil {
		logger.Error("failed to delete associated tokens signing keys from database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete item")
		return
	}

	responses.Success(w)
	logger.Info("logged out user")
}
