package authentication

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func listTokens(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("authentication.tokens.list").With(zap.String("method", "GET"), zap.String("path", "/authentication/tokens"),
		zap.String("authenticated_user", user.Id.Hex()))

	// Find all user's authentication tokens
	cur := db.Collection("tokens").FindMany(bson.M{"user_id": user.Id.Hex(), "type": database.TokenAuthentication})
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.SuccessWithData(w, []string{})
		return
	} else if err != nil {
		logger.Error("failed to query database for user's tokens", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode into array
	var tokens []database.Token
	for cur.Next() {
		var token database.Token
		if err := cur.Decode(&token); err != nil {
			logger.Error("failed to decode token", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to decode tokens")
			return
		}
		tokens = append(tokens, token)
	}

	responses.SuccessWithData(w, tokens)
	logger.Info("got list of user's tokens")
}
