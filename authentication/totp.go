package authentication

import (
	otp "github.com/pquerna/otp/totp"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.uber.org/zap"
	"net/http"
)

func totp(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	// Create logger
	logger := zap.L().Named("authentication.2fa.totp").With(zap.String("path", "/authentication/2fa/totp"), zap.String("method", "POST"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Validate request method and query parameters
	if r.Method != "GET" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if r.URL.RawQuery == "" || r.URL.Query().Get("code") == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'code' is required")
		return
	}

	// Ensure valid
	if !otp.Validate(r.URL.Query().Get("code"), user.TOTP.Secret) {
		responses.Error(w, http.StatusForbidden, "invalid authentication code")
		return
	}

	// Generate authentication token
	token, _, err := jwt.GenerateAuthentication(r.Header.Get("User-Agent"), user, db)
	if err != nil {
		logger.Error("failed to generate authentication token", zap.Error(err), zap.String("token", "authentication"))
		responses.Error(w, http.StatusInternalServerError, "failed to generate authentication token")
		return
	}

	responses.SuccessWithData(w, map[string]string{"token": token})
	logger.Info("new login for user with totp authentication")
}
