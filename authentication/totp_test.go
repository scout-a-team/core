package authentication

import (
	"context"
	"encoding/json"
	"errors"
	otp "github.com/pquerna/otp/totp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
	"time"
)

func TestTotp(t *testing.T) {
	// Generate totp key
	key, err := otp.Generate(otp.GenerateOpts{
		Issuer:      "Some Issuer",
		AccountName: "some-account",
	})
	assert.NoError(t, err)
	user.TOTP.Secret = key.Secret()

	// Setup facilities
	db := &mocks.Database{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "tokens").Return(tokenCollection)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)

	// Generate valid code
	code, err := otp.GenerateCode(key.Secret(), time.Now())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/authentication/2fa/totp?code="+code, nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	totp(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))

	// Validate response body
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Token string `json:"token"`
		} `json:"data"`
		Reason string `json:"reason"`
	}
	assert.NoError(t, json.NewDecoder(response.Body).Decode(&body))
	assert.Equal(t, body.Status, "success")
	assert.NotEmpty(t, body.Data.Token)
}

func TestTotp_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/authentication/2fa/totp?code=123456", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	totp(response, request)

	// Validate response
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestTotp_InvalidQueryParameters(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/authentication/2fa/totp", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	totp(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'code' is required"}`, response.Body.String())
}

func TestTotp_InvalidCode(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.TOTP.Secret = "SOMESECRETKEYFORTOTP"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/authentication/2fa/totp?code=123456", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	totp(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "invalid authentication code"}`, response.Body.String())
}

func TestTotp_GenerateAuthenticationDatabaseError(t *testing.T) {
	// Generate totp key
	key, err := otp.Generate(otp.GenerateOpts{
		Issuer:      "Some Issuer",
		AccountName: "some-account",
	})
	assert.NoError(t, err)
	user.TOTP.Secret = key.Secret()

	// Setup facilities
	db := &mocks.Database{}
	tokenCollection := &mocks.Collection{}
	db.On("Collection", "tokens").Return(tokenCollection)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("some database error"))

	// Generate valid code
	code, err := otp.GenerateCode(key.Secret(), time.Now())

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/authentication/2fa/totp?code="+code, nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	totp(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate authentication token"}`, response.Body.String())
}
