package authentication

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func login(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)

	// Create the logger
	logger := zap.L().Named("authentication.login").With(zap.String("method", "POST"), zap.String("path", "/authentication/login"))

	// Validate request method, headers, and ensure body exists
	if r.Method != "POST" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if body.Email == "" || body.Password == "" {
		responses.Error(w, http.StatusBadRequest, "fields 'email' and 'password' are required")
		return
	} else if len(body.Password) != 64 {
		responses.Error(w, http.StatusBadRequest, "field 'password' must be 64 characters long")
		return
	} else if len(body.Email) < 5 || len(body.Email) > 254 {
		responses.Error(w, http.StatusBadRequest, "field 'email' must be between 5 and 254 characters long")
		return
	}

	// Ensure user exists
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"email": body.Email}).Decode(&user); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusUnauthorized, "invalid email or password")
		return
	} else if err != nil {
		logger.Error("failed to query database to check if user with email exists", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Add user id to logger
	logger = logger.With(zap.String("user", user.Id.Hex()))

	// Ensure user is enabled
	if !user.Enabled {
		responses.Error(w, http.StatusForbidden, "user is not enabled")
		return
	}

	// Validate password
	if valid, err := hash.Verify(body.Password, user.Password); err != nil || !valid {
		responses.Error(w, http.StatusUnauthorized, "invalid email or password")
		return
	}
	logger.Debug("validated user password")

	// Generate intermediate token for two factor auth
	if user.TOTP.Enabled || user.WebAuthn.Enabled {
		token, _, err := jwt.Generate2faLogin(r.Header.Get("User-Agent"), user, db)
		if err != nil {
			logger.Error("failed to generate two factor intermediary token", zap.Error(err), zap.String("token", "two factor"))
			responses.Error(w, http.StatusInternalServerError, "failed to generate intermediary token")
			return
		}

		responses.SuccessWithData(w, map[string]interface{}{"2fa": true, "token": token, "methods": map[string]bool{"totp": user.TOTP.Enabled, "webauthn": user.WebAuthn.Enabled}})
		logger.Info("initiated login flow with 2fa")
		return
	}

	// Generate authentication token
	token, _, err := jwt.GenerateAuthentication(r.Header.Get("User-Agent"), user, db)
	if err != nil {
		logger.Error("failed to generate authentication token", zap.Error(err), zap.String("token", "authentication"))
		responses.Error(w, http.StatusInternalServerError, "failed to generate authentication token")
		return
	}

	// Get team to check if verified
	var team database.Team
	response := map[string]interface{}{"token": token, "redirect": false}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	if err := db.Collection("teams").FindOne(bson.M{"_id": tid}).Decode(&team); err != nil {
		logger.Error("failed to query database for team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Force redirect if not verified
	if !*team.Verified {
		response["redirect"] = true
	}

	responses.SuccessWithData(w, response)
	logger.Info("new login for user")
}
