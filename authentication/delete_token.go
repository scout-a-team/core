package authentication

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"net/http"
)

func deleteToken(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("scouting.match.list").With(zap.String("method", "GET"), zap.String("path", "/scouting/match/{event}"),
		zap.String("authenticated_user", user.Id.Hex()))

	// Ensure query parameters are present
	if r.URL.Query().Get("id") == "" || r.URL.RawQuery == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'id' is required")
		return
	}

	// Revoke all tokens if requested
	if r.URL.Query().Get("id") == "all" {
		if err := db.Collection("tokens").DeleteMany(bson.M{"user_id": user.Id.Hex(), "type": database.TokenAuthentication}); err != nil {
			logger.Error("failed to delete user's tokens from database", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to delete tokens")
			return
		}

		responses.Success(w)
		logger.Info("deleted user's authentication tokens")
		return
	}

	// Ensure token id is valid
	tokenId, err := primitive.ObjectIDFromHex(r.URL.Query().Get("id"))
	if err != nil {
		responses.Error(w, http.StatusBadRequest, "invalid id format")
		return
	}

	// Delete token from database
	if err := db.Collection("tokens").DeleteOne(bson.M{"_id": tokenId, "user_id": user.Id.Hex()}); err != nil {
		logger.Error("failed to delete token from database", zap.String("id", tokenId.Hex()), zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete token")
		return
	}

	responses.Success(w)
	logger.Info("deleted user's authentication token")
}
