package authentication

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRoute(t *testing.T) {
	r := chi.NewRouter()

	// Initialize router
	Route(r)

	expected := []struct {
		route   string
		methods []string
	}{
		{
			route:   "/2fa/totp",
			methods: []string{"GET"},
		},
		{
			route:   "/forgot-password",
			methods: []string{"GET"},
		},
		{
			route:   "/login",
			methods: []string{"POST"},
		},
		{
			route:   "/logout",
			methods: []string{"GET"},
		},
		{
			route:   "/register",
			methods: []string{"POST"},
		},
		{
			route:   "/tokens",
			methods: []string{"GET", "DELETE"},
		},
	}

	// Check routes are correct
	for i, route := range r.Routes() {
		for _, method := range expected[i].methods {
			assert.Contains(t, route.Handlers, method)
		}

		assert.Equal(t, expected[i].route, route.Pattern)
	}
}
