package authentication

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

var unregistered = false

func TestRegister(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]interface{}{"email": "some@email.com", "password": password, "first_name": "Some", "last_name": "Name", "team_number": 1})

	// Create required services
	mailChan := make(chan mail.Message)
	go func() {
		<-mailChan
		<-mailChan
	}()

	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	roleCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}

	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "roles").Return(roleCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("InsertOne", mock.AnythingOfType("database.User")).Return(primitive.NewObjectID(), nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	roleCollection.On("InsertMany", mock.AnythingOfType("[]interface {}")).Return(nil, nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run the request
	register(response, request)

	// Ensure response is correct
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestRegister_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/register", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestRegister_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestRegister_NoBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: EOF"}`, response.Body.String())
}

func TestRegister_InvalidJSON(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": }`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestRegister_NoBodyFields(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "fields 'first_name', 'last_name', 'email', 'password', and 'team_number' are required"}`, response.Body.String())
}

func TestRegister_InvalidPasswordLength(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abc", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' must be 64 characters long"}`, response.Body.String())
}

func TestRegister_InvalidEmailLength(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "abc", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'email' must be between 5 and 254 characters long"}`, response.Body.String())
}

func TestRegister_AlreadyRegisteredTeam(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &registered
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 409, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team already has an account"}`, response.Body.String())
}

func TestRegister_NonExistentTeam(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestRegister_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestRegister_AlreadyRegisteredUser(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}

	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 409, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified email address is already in use"}`, response.Body.String())
}

func TestRegister_FindUserDatabaseError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}

	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestRegister_UpdateTeamDatabaseError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}

	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to insert team"}`, response.Body.String())
}

func TestRegister_InsertUserDatabaseError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}

	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("InsertOne", mock.AnythingOfType("database.User")).Return(nil, errors.New("some database error"))
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to insert data"}`, response.Body.String())
}

func TestRegister_EmailTokenError(t *testing.T) {
	// Setup facilities
	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	roleCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}

	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "roles").Return(roleCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("InsertOne", mock.AnythingOfType("database.User")).Return(primitive.NewObjectID(), nil)
	roleCollection.On("InsertMany", mock.AnythingOfType("[]interface {}")).Return([]interface{}{}, nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("some database error"))
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Registered = &unregistered
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/register", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "first_name": "some", "last_name": "user", "team": 1}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	register(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate email verification token"}`, response.Body.String())
}
