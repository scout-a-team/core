package authentication

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

var registered = true
var verified = false

func register(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	messages := request_context.GetMailFacility(r)

	// Create the logger
	logger := zap.L().Named("authentication.register").With(zap.String("method", "POST"), zap.String("path", "/authentication/register"))

	// Validate request method, headers, and ensure body exists
	if r.Method != "POST" {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		FirstName  string `json:"first_name"`
		LastName   string `json:"last_name"`
		Email      string `json:"email"`
		Password   string `json:"password"`
		TeamNumber int    `json:"team_number"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if body.FirstName == "" || body.LastName == "" || body.Email == "" || body.Password == "" {
		responses.Error(w, http.StatusBadRequest, "fields 'first_name', 'last_name', 'email', 'password', and 'team_number' are required")
		return
	} else if len(body.Password) != 64 {
		responses.Error(w, http.StatusBadRequest, "field 'password' must be 64 characters long")
		return
	} else if len(body.Email) < 5 || len(body.Email) > 254 {
		responses.Error(w, http.StatusBadRequest, "field 'email' must be between 5 and 254 characters long")
		return
	}

	// Check if team already has account
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"number": body.TeamNumber}).Decode(&team); err == nil && team.Registered != nil && *team.Registered {
		responses.Error(w, http.StatusConflict, "specified team already has an account")
		return
	} else if err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database to check if team is registered", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Check if email is in use
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"email": body.Email}).Decode(&user); err == nil {
		responses.Error(w, http.StatusConflict, "specified email address is already in use")
		return
	} else if err != mongo.ErrNoDocuments {
		logger.Error("failed to query database to check if email is in use", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}
	logger.Debug("email and team number not in use")

	// Hash user password
	h, err := hash.DefaultHash(body.Password)
	if err != nil {
		logger.Error("failed to hash password", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to hash password")
		return
	}

	// Update team entry
	team.Registered = &registered
	team.Verified = &verified
	err = db.Collection("teams").Update(bson.M{"_id": team.Id}, bson.M{"$set": team}, false, false)
	if err != nil {
		logger.Error("failed to insert team into database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to insert team")
		return
	}

	// Create user entry
	user = database.User{
		Name:     body.FirstName + " " + body.LastName,
		Email:    body.Email,
		Password: h,
		Role:     rbac.RoleOwner,
		TeamId:   team.Id.Hex(),
		Enabled:  false,
	}
	insertedUser, err := db.Collection("users").InsertOne(user)
	if err != nil {
		logger.Error("failed to insert user into database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to insert data")
		return
	}
	user.Id = insertedUser.(primitive.ObjectID)

	// Generate token for email validation
	token, _, err := jwt.GenerateEmailVerification(r.Header.Get("User-Agent"), user, db)
	if err != nil {
		logger.Error("failed to generate email validation token", zap.Error(err), zap.String("token", "email-verification"))
		responses.Error(w, http.StatusInternalServerError, "failed to generate email verification token")
		return
	}

	// Send verification email
	messages <- mail.Message{
		Subject:   "Please verify your email",
		Recipient: user.Email,
		Template:  "email-verification",
		Variables: map[string]interface{}{
			"token": token,
			"name":  user.Name,
		},
	}

	// Send on-boarding email
	messages <- mail.Message{
		Subject:   "Welcome to Scout a Team",
		Recipient: user.Email,
		Template:  "team-registration",
		Variables: map[string]interface{}{
			"name": user.Name,
		},
	}

	responses.Success(w)
	logger.Info("registered new user and team", zap.String("team", team.Id.Hex()), zap.String("user", user.Id.Hex()))
}
