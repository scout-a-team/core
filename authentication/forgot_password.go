package authentication

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func forgotPassword(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	messages := request_context.GetMailFacility(r)

	// Create logger
	logger := zap.L().Named("authentication.reset_password").With(zap.String("method", "GET"), zap.String("path", "/authentication/forgot-password"))

	// Validate method and query parameters
	if r.Method != http.MethodGet {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if r.URL.Query().Get("email") == "" || r.URL.RawQuery == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'email' is required")
		return
	}

	// Get user by email
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"email": r.URL.Query().Get("email")}).Decode(&user); err == mongo.ErrNoDocuments {
		// Don't confirm/deny an account exists
		responses.Success(w)
		return
	} else if err != nil {
		logger.Error("failed to query database for user existent", zap.Error(err), zap.String("email", r.URL.Query().Get("email")))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Issue password reset token
	token, _, err := jwt.GeneratePasswordReset(r.Header.Get("User-Agent"), user, db)
	if err != nil {
		logger.Error("failed to generate password reset token", zap.Error(err), zap.String("user", user.Id.Hex()))
		responses.Error(w, http.StatusInternalServerError, "failed to generate token")
		return
	}

	// Send verification email
	messages <- mail.Message{
		Subject:   "Password reset requested",
		Recipient: user.Email,
		Template:  "password-reset",
		Variables: map[string]interface{}{
			"token": token,
			"name":  user.Name,
		},
	}

	responses.Success(w)
	logger.Info("issued password reset", zap.String("user", user.Id.Hex()))
}
