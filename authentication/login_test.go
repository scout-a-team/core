package authentication

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestLogin(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Create required services }
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}

	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		tBool := true
		team.Verified = &tBool
	})
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.VerifiedTeam, true))
	request.Header.Set("Content-Type", "application/json")

	// Run the request
	login(response, request)

	// Ensure response is correct
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))

	// Check body is correct
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token    string `json:"token"`
			Redirect bool   `json:"redirect"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))
	assert.Equal(t, responseBody.Status, "success")
	assert.NotEqual(t, responseBody.Data.Token, "")
	assert.False(t, responseBody.Data.Redirect)
}

func TestLogin_InvalidMethod(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/login", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 405, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "method not allowed"}`, response.Body.String())
}

func TestLogin_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestLogin_NoBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: EOF"}`, response.Body.String())
}

func TestLogin_InvalidJSON(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": }`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestLogin_NoBodyFields(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "fields 'email' and 'password' are required"}`, response.Body.String())
}

func TestLogin_InvalidPasswordLength(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "some@email.com", "password": "abc"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' must be 64 characters long"}`, response.Body.String())
}

func TestLogin_InvalidEmailLength(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "abc", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+="}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'email' must be between 5 and 254 characters long"}`, response.Body.String())
}

func TestLogin_NonExistentUser(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+="}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 401, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "invalid email or password"}`, response.Body.String())
}

func TestLogin_FindUserDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+="}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestLogin_NotEnabledUser(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = false
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+="}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "user is not enabled"}`, response.Body.String())
}

func TestLogin_InvalidPassword(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBufferString(`{"email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+="}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 401, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "invalid email or password"}`, response.Body.String())
}

func TestLogin_TOTP(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
		user.TOTP.Enabled = true
		user.WebAuthn.Enabled = true
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))

	// Check response body
	var respBody struct {
		Status string `json:"status"`
		Data   struct {
			TwoFactor bool   `json:"2fa"`
			Token     string `json:"token"`
			Methods   struct {
				Totp     bool `json:"totp"`
				Webauthn bool `json:"webauthn"`
			} `json:"methods"`
		} `json:"data"`
		Reason string `json:"reason"`
	}
	assert.NoError(t, json.NewDecoder(response.Body).Decode(&respBody))
	assert.Empty(t, respBody.Reason)
	assert.Equal(t, "success", respBody.Status)
	assert.True(t, respBody.Data.TwoFactor)
	assert.True(t, respBody.Data.Methods.Totp)
	assert.True(t, respBody.Data.Methods.Webauthn)
	assert.NotEmpty(t, respBody.Data.Token)
}

func TestLogin_IntermediaryGenerationError(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
		user.TOTP.Enabled = true
		user.WebAuthn.Enabled = true
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate intermediary token"}`, response.Body.String())
}

func TestLogin_AuthenticationTokenDatabaseError(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate authentication token"}`, response.Body.String())
}

func TestLogin_FindTeamDatabaseError(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil).Once()
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	login(response, request)

	// Check response data
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestLogin_WithRedirect(t *testing.T) {
	// Setup request values
	hasher := sha256.New()
	hasher.Write([]byte("some password"))
	password := hex.EncodeToString(hasher.Sum(nil))
	body, _ := json.Marshal(map[string]string{"email": "some@email.com", "password": password})

	// Create required services
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	userResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}

	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Enabled = true
		user.Id = primitive.NewObjectID()
		pw, _ := hash.DefaultHash(password)
		user.Password = pw
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		fBool := false
		team.Verified = &fBool
	})
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/login", bytes.NewBuffer(body))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request.Header.Set("Content-Type", "application/json")

	// Run the request
	login(response, request)

	// Ensure response is correct
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))

	// Check body is correct
	var responseBody struct {
		Status string `json:"status"`
		Data   struct {
			Token    string `json:"token"`
			Redirect bool   `json:"redirect"`
		} `json:"data"`
	}
	assert.NoError(t, json.Unmarshal(response.Body.Bytes(), &responseBody))
	assert.Equal(t, responseBody.Status, "success")
	assert.NotEqual(t, responseBody.Data.Token, "")
	assert.True(t, responseBody.Data.Redirect)
}
