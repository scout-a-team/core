package metrics_processor

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/trueskill"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"time"
)

// Calculate trueskill for a match given the scouted data
func trueSkillDaemon(cfg config.Config, db database.Database, ctx context.Context) {
	logger := zap.L().Named("metrics_processor.trueskill").With(zap.String("address", cfg.TrueSkill.Address))

	client, err := trueskill.NewClient(cfg.TrueSkill.Address)
	if err != nil {
		logger.Fatal("failed to connect to trueskill calculation service", zap.Error(err))
	}
	logger.Info("connected to calculation service", zap.Float64("default_mu", cfg.TrueSkill.Mu), zap.Float64("default_sigma", cfg.TrueSkill.Sigma))

	// Run every 5-minutes
	ticker := time.NewTicker(5 * time.Minute)

	for {
		select {
		case <-ticker.C:
			var matches []database.ScoutedMatch
			if err := db.Collection("scouted_match").FindMany(bson.M{"computed": bson.M{"$not": true}}).Decode(&matches); err != nil {
				logger.Error("failed to query database for uncomputed matches", zap.Error(err))
				continue
			}

			// Compute trueskill for each match
			for _, match := range matches {
				computeForMatch(match, logger, db, client)
			}

		case <-ctx.Done():
			if err := client.Close(); err != nil {
				logger.Error("failed to close grpc connection", zap.Error(err))
			}
			return
		}
	}
}

func computeForMatch(matchData database.ScoutedMatch, logger *zap.Logger, db database.Database, client *trueskill.Client) {
	// Ensure there's data for at least one team from each alliance
	if (matchData.Blue1 != "" || matchData.Blue2 != "" || matchData.Blue3 != "") && (matchData.Red1 != "" || matchData.Red2 != "" || matchData.Red3 != "") {
		return
	}

	// Get actual match id
	matchId, _ := primitive.ObjectIDFromHex(matchData.MatchId)

	// Retrieve match from database
	var match database.Match
	if err := db.Collection("matches").FindOne(bson.M{"_id": matchId}).Decode(&match); err != nil {
		logger.Error("failed to query database for match", zap.Error(err), zap.String("match", matchData.MatchId))
		return
	}

	// Convert match to request
	request, err := convertToGRPCMatch(match, matchData.ScoutedBy, db)
	if err != nil {
		logger.Error("failed to convert to grpc request", zap.Error(err), zap.String("match", matchData.MatchId))
		return
	}
	request.Winner = trueskill.Winner(matchData.Winner)

	// Send request
	red, blue, err := client.Rate(request)
	if err != nil {
		logger.Error("failed to rate match", zap.Error(err), zap.String("match", matchData.MatchId))
		return
	}

	// Add back into database
	if err := convertFromGRPCMatch(red, blue, match, matchData.ScoutedBy, db); err != nil {
		logger.Error("failed to update team trueskill for match", zap.Error(err), zap.String("match", matchData.MatchId))
		return
	}
}
