package metrics_processor

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/trueskill"
	"go.mongodb.org/mongo-driver/bson"
	"strconv"
	"strings"
)

// Convert the specified match ID to a gRPC match
func convertToGRPCMatch(match database.Match, scoutedBy string, db database.Database) (trueskill.Match, error) {
	// Setup match to be returned
	grpcMatch := trueskill.Match{
		Red:    []*trueskill.Team{},
		Blue:   []*trueskill.Team{},
		Winner: 0,
	}

	// Get teams on red alliance
	for _, teamKey := range []string{match.RedAlliance.Team1, match.RedAlliance.Team2, match.RedAlliance.Team3} {
		// Remove prefix from key
		key := strings.TrimPrefix(teamKey, "frc")

		// Convert to integer
		number, _ := strconv.Atoi(key)

		// Query database for team
		var team database.Team
		if err := db.Collection("teams").FindOne(bson.M{"number": number}).Decode(&team); err != nil {
			return trueskill.Match{}, err
		}

		// Retrieve trueskill data and add to request
		if scoutedBy == "" {
			grpcMatch.Red = append(grpcMatch.Red, team.GlobalTrueSkill.ToGrpc())
		} else {
			grpcMatch.Red = append(grpcMatch.Red, team.TeamTrueSkill[scoutedBy].ToGrpc())
		}
	}

	// Get teams on blue alliance
	for _, teamKey := range []string{match.BlueAlliance.Team1, match.BlueAlliance.Team2, match.BlueAlliance.Team3} {
		// Remove prefix from key
		key := strings.TrimPrefix(teamKey, "frc")

		// Convert to integer
		number, _ := strconv.Atoi(key)

		// Query database for team
		var team database.Team
		if err := db.Collection("teams").FindOne(bson.M{"number": number}).Decode(&team); err != nil {
			return trueskill.Match{}, err
		}

		// Retrieve trueskill data and add to request
		if scoutedBy == "" {
			grpcMatch.Blue = append(grpcMatch.Blue, team.GlobalTrueSkill.ToGrpc())
		} else {
			grpcMatch.Blue = append(grpcMatch.Blue, team.TeamTrueSkill[scoutedBy].ToGrpc())
		}
	}

	return grpcMatch, nil
}

// Update rated teams in database
func convertFromGRPCMatch(newRed, newBlue []*trueskill.Team, match database.Match, scoutedBy string, db database.Database) error {
	// Update red teams
	for i, teamKey := range []string{match.RedAlliance.Team1, match.RedAlliance.Team2, match.RedAlliance.Team3} {
		// Remove prefix from key
		key := strings.TrimPrefix(teamKey, "frc")

		// Convert to integer
		number, _ := strconv.Atoi(key)

		// Set either global or team trueskill
		var update bson.M
		if scoutedBy == "" {
			update = bson.M{"trueskill": database.TrueSkill{
				Mu:    newRed[i].Mu,
				Sigma: newRed[i].Sigma,
			}}
		} else {
			update = bson.M{"team_trueskill": map[string]database.TrueSkill{
				scoutedBy: {
					Mu:    newRed[i].Mu,
					Sigma: newRed[i].Sigma,
				},
			}}
		}

		// Update team's field
		if err := db.Collection("teams").Update(bson.M{"number": number}, bson.M{"$set": update}, false, false); err != nil {
			return err
		}
	}

	// Update blue teams
	for i, teamKey := range []string{match.BlueAlliance.Team1, match.BlueAlliance.Team2, match.BlueAlliance.Team3} {
		// Remove prefix from key
		key := strings.TrimPrefix(teamKey, "frc")

		// Convert to integer
		number, _ := strconv.Atoi(key)

		// Set either global or team trueskill
		var update bson.M
		if scoutedBy == "" {
			update = bson.M{"trueskill": database.TrueSkill{
				Mu:    newBlue[i].Mu,
				Sigma: newBlue[i].Sigma,
			}}
		} else {
			update = bson.M{"team_trueskill": map[string]database.TrueSkill{
				scoutedBy: {
					Mu:    newBlue[i].Mu,
					Sigma: newBlue[i].Sigma,
				},
			}}
		}

		// Update team's field
		if err := db.Collection("teams").Update(bson.M{"number": number}, bson.M{"$set": update}, false, false); err != nil {
			return err
		}
	}

	return nil
}
