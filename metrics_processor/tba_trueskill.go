package metrics_processor

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/trueskill"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"time"
)

func tbaTrueSkillDaemon(cfg config.Config, db database.Database, ctx context.Context) {
	logger := zap.L().Named("metrics_processor.tba_trueskill").With(zap.String("address", cfg.TrueSkill.Address))

	client, err := trueskill.NewClient(cfg.TrueSkill.Address)
	if err != nil {
		logger.Fatal("failed to connect to trueskill calculation service", zap.Error(err))
	}
	logger.Info("connected to calculation service", zap.Float64("default_mu", cfg.TrueSkill.Mu), zap.Float64("default_sigma", cfg.TrueSkill.Sigma))

	// Run every 5 minutes
	ticker := time.NewTicker(10 * time.Minute)

	for {
		select {
		case <-ticker.C:
			var matches []database.Match
			if err := db.Collection("matches").FindMany(bson.M{}).Decode(&matches); err != nil {
				logger.Error("failed to query database for uncomputed matches", zap.Error(err))
				continue
			}

			// Compute trueskill for each match
			for _, match := range matches {
				tbaComputeForMatch(match, logger, db, client)
			}

		case <-ctx.Done():
			if err := client.Close(); err != nil {
				logger.Error("failed to close grpc connection", zap.Error(err))
			}
			return
		}
	}
}

func tbaComputeForMatch(matchData database.Match, logger *zap.Logger, db database.Database, client *trueskill.Client) {
	// Ensure there's data for the score
	if matchData.Winner == database.WinAllianceUnknown {
		return
	}

	// Convert match to request
	request, err := convertToGRPCMatch(matchData, "", db)
	if err != nil {
		logger.Error("failed to convert to grpc request", zap.Error(err), zap.String("match", matchData.Id.Hex()))
		return
	}
	request.Winner = trueskill.Winner(matchData.Winner)

	// Send request
	red, blue, err := client.Rate(request)
	if err != nil {
		logger.Error("failed to rate match", zap.Error(err), zap.String("match", matchData.Id.Hex()))
		return
	}

	// Add back to database
	if err := convertFromGRPCMatch(red, blue, matchData, "", db); err != nil {
		logger.Error("failed to update team trueskill for match", zap.Error(err), zap.String("match", matchData.Id.Hex()))
		return
	}
}
