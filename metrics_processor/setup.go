package metrics_processor

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
)

func Setup(cfg config.Config, db database.Database) context.CancelFunc {
	// Create cancellation context
	ctx, cancel := context.WithCancel(context.Background())

	// Start daemons
	go trueSkillDaemon(cfg, db, ctx)
	go tbaTrueSkillDaemon(cfg, db, ctx)

	return cancel
}
