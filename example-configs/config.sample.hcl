# Main server configuration
http {
  # What address to listen on
  # Use 0.0.0.0 for all interfaces
  # Default: 127.0.0.1
  host = "127.0.0.1"
  # What port to listen on
  # Default: 8080
  port = 8080
  # Domain/IP where the service can be accessed
  # Default: https://scouta.team
  domain = "https://scouta.team"
  # Default: true
  # Enable cross origin resource sharing
  cors = true
}

# Database connection configuration
database {
  # Connection URI for the database server
  # Default: mongodb://127.0.0.1:27017
  connection_string = "monogodb://127.0.0.1:27017"
  # Password corresponding with username
  # Default: postgres
  database = "scout-a-team"
  # Number of importer workers to spawn
  # Default: 5
  import_workers = 5
}

# Cache (redis) connection configuration
cache {
  # Connection string for the server
  # Default: 127.0.0.1:6379
  address = "127.0.0.1:6379"
  # Optional password to use
  # Default: ""
  password = ""
  # Database to select
  # Default: 0
  database = 0
}

# Logging configuration
logging {
  # Minimum level to log at
  # Options: debug, info, warn, error, dev-panic, panic, fatal
  # Default: info
  level = "info"
  # Enable development mode or not
  # Default: false
  development = false
}

 # The Blue Alliance API configuration
tba {
  # API key to use for authenticating with the TBA API
  # Default: ""
  api_key = ""
  # The value for the User-Agent header
  # Default: Go-http-client/1.1
  user_agent = "Go-http-client/1.1"
  # Enable the retrieval of data only if it has changed since the last requested
  # Default: false
  caching = false
  # The time (in seconds) for when to refresh the data
  # Default: 900
  refresh_interval = 900
}

# MailGun email API configuration
mail {
  # Domain to use for MailGun
  # Default: scouta.team
  domain = "scouta.team"
  # API key associated with MailGun domain
  # Default: ""
  api_key = ""
  # Address to send emails from
  # Default: from@scouta.team
  from = "from@scouta.team"
  # Name to send emails from
  # Default: Scout a Team
  from_name = "Scout a Team"
}

# TrueSkill calculation configuration
trueskill {
  # Address of the calculation server to connect to
  # Address format documentation: https://github.com/grpc/grpc/blob/master/doc/naming.md
  # Default: ipv4:127.0.0.1:9090
  address = "ipv4:127.0.0.1:9090"
  # Initial mean of ratings
  # This should be the same as the calculation server
  # Default: 25.0
  mu = 25.0
  # Initial standard deviation of ratings
  # Recommended value is a third of mu
  # This should be the same as the calculation server
  # Default: 8.333333333333334
  sigma = 8.333333333333334
}

# File storage configuration
files {
  # Where to store the files for team verification
  # Default: ./files/verification
  verification = "./files/verification"
  # Where to store robot photos
  # Default: ./files/photos
  photos = "./files/photos"
}

# Disable specified service modules
modules {
  # Disable the authentication module
  # Default: false
  disable_authentication = false
  # Disable the verification module
  # Default: false
  disable_verification = false
  # Disable the teams module
  # Default: false
  disable_teams = false
  # Disable the users module
  # Default: false
  disable_users = false
  # Disable the importer module
  # Default: false
  disable_importer = false
  # Disable the match scout module
  # Default: false
  disable_match_scout = false
  # Disable the pit scout module
  # Default: false
  disable_pit_scout = false
  # Disable the events module
  # Default: false
  disable_events = false
  # Disable the metrics processing module
  # Default: false
  disable_metrics_processing = false
}
