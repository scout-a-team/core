package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"time"
)

// Load the configuration into Viper
func LoadConfig(testing bool) Config {
	// Allow for configuration from environment and flags
	bindEnv()
	provider, _, _, _, _, _, config := bindFlags()

	// Set configuration name
	if testing {
		viper.SetConfigName("config.test")
	} else {
		viper.SetConfigName("config")
	}

	// Set possible configuration locations
	viper.AddConfigPath(".")
	viper.AddConfigPath("..")
	viper.AddConfigPath("/etc/scout-a-team")

	// Set default configuration methods
	setDefaults()

	// Setup remote configuration loading
	if *provider != "file" {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:34","msg":"remote configuration is currently disabled"}\n`, time.Now().UnixNano())
		os.Exit(1)

		// This is disabled until spf13/viper remote config has been fixed
		// or a better solution for configuration has been found
		// remoteConfig(*provider, *providerFormat, *providerHost, *providerPath, *providerSecure, *providerKeyRing)
	}

	// Check if specific config file is given
	if *config != "" {
		// Ensure exists and is not directory
		if fileInfo, err := os.Stat(*config); os.IsNotExist(err) {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:46","msg":"specified configuration file does not exist","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
			os.Exit(1)
		} else if fileInfo.IsDir() {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:49","msg":"specified configuration file is a directory"}\n`, time.Now().UnixNano())
			os.Exit(1)
		}

		// Read file
		file, err := os.Open(*config)
		if err != nil {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:56","msg":"failed to open configuration file","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
			os.Exit(1)
		}

		if err := viper.ReadConfig(file); err != nil {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:61","msg":"failed to parse configuration","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
			os.Exit(1)
		}

		// Use default if none
	} else if err := viper.ReadInConfig(); err != nil {
		switch err.(type) {
		case viper.ConfigFileNotFoundError:
			break

		default:
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:72","msg":"failed to parse configuration","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
			os.Exit(1)
		}
	}

	// Validate config
	validate(testing)

	// Generate the logger config
	generateLoggerConfig()

	// Unmarshal configuration into struct
	var c Config
	if err := viper.Unmarshal(&c); err != nil {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/config.go:90","msg":"failed to unmarshal configuration to struct","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
		os.Exit(1)
	}

	return c
}
