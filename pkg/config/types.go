package config

// Global configuration store
type Config struct {
	Http      http         `mapstructure:"http"`
	Database  database     `mapstructure:"database"`
	Cache     cache        `mapstructure:"cache"`
	Logging   logging      `mapstructure:"logging"`
	Tba       blueAlliance `mapstructure:"tba"`
	Mail      mail         `mapstructure:"mail"`
	Modules   modules      `mapstructure:"modules"`
	TrueSkill trueSkill    `mapstructure:"trueskill"`
	Files     files        `mapstructure:"files"`
}

// HTTP listener configuration
type http struct {
	Host   string `mapstructure:"host"`
	Port   int    `mapstructure:"port"`
	Domain string `mapstructure:"domain"`
	Cors   bool   `mapstructure:"cors"`
}

// Database connection configuration
type database struct {
	ImportWorkers    int    `mapstructure:"import_workers"`
	ConnectionString string `mapstructure:"connection_string"`
	Database         string `mapstructure:"database"`
}

// Cache connection configuration
type cache struct {
	Address  string `mapstructure:"address"`
	Password string `mapstructure:"password"`
	Database int    `mapstructure:"database"`
}

// Logging configuration
type logging struct {
	Level       string `mapstructure:"level"`
	Development bool   `mapstructure:"development"`
}

// TBA API configuration
type blueAlliance struct {
	ApiKey          string `mapstructure:"api_key"`
	UserAgent       string `mapstructure:"user_agent"`
	Caching         bool   `mapstructure:"caching"`
	RefreshInterval int    `mapstructure:"refresh_interval"`
}

// MailGun API configuration
type mail struct {
	Domain   string `mapstructure:"domain"`
	ApiKey   string `mapstructure:"api_key"`
	From     string `mapstructure:"from"`
	FromName string `mapstructure:"from_name"`
}

// Modules configuration
type modules struct {
	DisableAuthentication    bool `mapstructure:"disable_authentication"`
	DisableVerification      bool `mapstructure:"disable_verification"`
	DisableTeams             bool `mapstructure:"disable_teams"`
	DisableUsers             bool `mapstructure:"disable_users"`
	DisableRoles             bool `mapstructure:"disable_roles"`
	DisableImporter          bool `mapstructure:"disable_importer"`
	DisableMatchScout        bool `mapstructure:"disable_match_scout"`
	DisablePitScout          bool `mapstructure:"disable_pit_scout"`
	DisableEvents            bool `mapstructure:"disable_events"`
	DisableStatistics        bool `mapstructure:"disable_statistics"`
	DisableMetricsProcessing bool `mapstructure:"disable_metrics_processing"`
}

// TrueSkill configuration
type trueSkill struct {
	Address string  `mapstructure:"address"`
	Mu      float64 `mapstructure:"mu"`
	Sigma   float64 `mapstructure:"sigma"`
}

// Files configuration
type files struct {
	Verification string `mapstructure:"verification"`
	Photos       string `mapstructure:"photos"`
}
