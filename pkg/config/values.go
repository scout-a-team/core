package config

import (
	"fmt"
	"github.com/akrantz01/go-tba"
	"github.com/spf13/viper"
	"os"
	"time"
)

// Set the default configuration values
func setDefaults() {
	// Set http configuration
	viper.SetDefault("http.host", "127.0.0.1")
	viper.SetDefault("http.port", 8080)
	viper.SetDefault("http.domain", "http://"+viper.GetString("http.host")+":"+viper.GetString("http.port"))
	viper.SetDefault("http.cors", true)

	// Set database configuration
	viper.SetDefault("database.connection_string", "mongodb://127.0.0.1:27017")
	viper.SetDefault("database.database", "scout-a-team")
	viper.SetDefault("database.import_workers", 5)

	// Set cache configuration
	viper.SetDefault("cache.address", "127.0.0.1:6379")
	viper.SetDefault("cache.password", "")
	viper.SetDefault("cache.database", 0)

	// Set logging configuration
	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.development", false)

	// Set TBA configuration
	viper.SetDefault("tba.api_key", "")
	viper.SetDefault("tba.user_agent", "Go-http-client/1.1")
	viper.SetDefault("tba.caching", false)
	viper.SetDefault("tba.refresh_interval", 900)

	// Set MailGun configuration
	viper.SetDefault("mail.domain", "scouta.team")
	viper.SetDefault("mail.api_key", "")
	viper.SetDefault("mail.from", "no-reply@scouta.team")
	viper.SetDefault("mail.from_name", "Scout a Team")

	// Set server module configuration
	viper.SetDefault("modules.disable_authentication", false)
	viper.SetDefault("modules.disable_verification", false)
	viper.SetDefault("modules.disable_teams", false)
	viper.SetDefault("modules.disable_users", false)
	viper.SetDefault("modules.disable_roles", false)
	viper.SetDefault("modules.disable_importer", false)
	viper.SetDefault("modules.disable_match_scout", false)
	viper.SetDefault("modules.disable_pit_scout", false)
	viper.SetDefault("modules.disable_events", false)
	viper.SetDefault("modules.disable_statistics", false)
	viper.SetDefault("modules.disable_metrics_processing", false)

	// Set TrueSkill configuration
	viper.SetDefault("trueskill.address", "ipv4:127.0.0.1:9090")
	viper.SetDefault("trueskill.mu", 25.0)
	viper.SetDefault("trueskill.sigma", 8.333333333333334)

	// Set files configuration
	viper.SetDefault("files.verification", "./files/verification")
	viper.SetDefault("files.photos", "./files/photos")
}

// Validate the provided configuration
func validate(testing bool) {
	// Validate log level
	if level := viper.GetString("logging.level"); level != "debug" && level != "info" && level != "warn" && level != "error" && level != "dev-panic" && level != "panic" && level != "fatal" {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/values.go:72","msg":"invalid log level","valid-options":["debug","info","warn","error","dev-panic","panic","fatal"]}\n`, time.Now().UnixNano())
		os.Exit(1)
	}

	if !testing {
		// Validate API key
		if _, _, err := tba.Status(viper.GetString("tba.api_key"), nil); err != nil {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/values.go:79","msg":"invalid TBA api key"}`, time.Now().UnixNano())
			os.Exit(1)
		}

		// Ensure at least one module is enabled
		if viper.GetBool("modules.disable_authentication") && viper.GetBool("modules.disable_users") && viper.GetBool("modules.disable_teams") &&
			viper.GetBool("modules.disable_importer") && viper.GetBool("modules.disable_match_scout") && viper.GetBool("modules.disable_pit_scout") &&
			viper.GetBool("modules.disable_events") && viper.GetBool("modules.disable_metrics_processing") {
			fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/values.go:87","msg":"at least one service module must be enabled"}`, time.Now().UnixNano())
			os.Exit(1)
		}
	}
}
