package config

import (
	"flag"
	"fmt"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"os"
	"strings"
	"time"
)

// Add command line flags
func bindFlags() (provider, format, host, path *string, secure *bool, keyring, config *string) {
	// Provider configuration
	provider = flag.String("provider", "file", "Configuration provider to use")
	format = flag.String("provider.format", "json", "Configuration file type to parse as")
	host = flag.String("provider.host", "http://127.0.0.1:4001", "Host of the remote provider")
	path = flag.String("provider.path", "/config/config.json", "Path to the location of the configuration")
	secure = flag.Bool("provide.secure", false, "Read from encrypted key store")
	keyring = flag.String("provider.secure.keyring", "/etc/secrets/keyring.gpg", "Keyring to use for encrypted keystores")
	config = flag.String("config", "", "Configuration file to use")

	// Http configuration flags
	flag.String("http.host", "127.0.0.1", "Address for the server to listen on. Use 0.0.0.0 to listen on all interfaces")
	flag.Int("http.port", 8080, "Port for the server to run on")
	flag.String("http.domain", "", "Domain the server will be accessible from")
	flag.Bool("http.cors", false, "Enable cross origin resource sharing")

	// Database configuration flags
	flag.String("database.connection_string", "127.0.0.1", "Connection URI for the server")
	flag.String("database.database", "postgres", "Database to use on the Postgres server")
	flag.Int("database.import_workers", 5, "Number of importer workers to spawn")

	// Cache configuration flags
	flag.String("cache.address", "127.0.0.1:6379", "Connection string for the server")
	flag.String("cache.password", "", "Optional password to use for authentication")
	flag.Int("cache.database", 0, "Database to select")

	// Logging configuration flags
	flag.String("logging.level", "info", "Minimum log level to output")
	flag.Bool("logging.development", false, "Enable development mode")

	// Blue Alliance API configuration flags
	flag.String("tba.api_key", "", "API key to use for authenticating with the TBA API")
	flag.String("tba.user_agent", "Go-http-client/1.1", "The value for the User-Agent header")
	flag.Bool("tba.caching", false, "Enable the retrieval of data only if it has changed since the last request")
	flag.Int("tba.refresh_interval", 900, "The time (in seconds) for when to refresh the data")

	// Mail server configuration
	flag.String("mail.domain", "", "Domain to use for MailGun")
	flag.String("mail.api_key", "", "API key associated with MailGun domain")
	flag.String("mail.from", "no-reply@scouta.team", "Address to send emails from")
	flag.String("mail.from_name", "Scout a Team", "Name to send emails from")

	// Server modules configuration flags
	flag.Bool("modules.disable_authentication", false, "Disable the authentication service module")
	flag.Bool("modules.disable_verification", false, "Disable the verification service module")
	flag.Bool("modules.disable_teams", false, "Disable the teams service module")
	flag.Bool("modules.disable_users", false, "Disable the users service module")
	flag.Bool("modules.disable_roles", false, "Disable the roles service module")
	flag.Bool("modules.disable_importer", false, "Disable the importer service module")
	flag.Bool("modules.disable_match_scout", false, "Disable the match scout service module")
	flag.Bool("modules.disable_pit_scout", false, "Disable the pit scout service module")
	flag.Bool("modules.disable_events", false, "Disable the events service module")
	flag.Bool("modules.disable_statistics", false, "Disable the statistics service module")
	flag.Bool("modules.disable_metrics_processing", false, "Disable the metrics processing service module")

	// TrueSkill configuration flags
	flag.String("trueskill.address", "ipv4:127.0.0.1:9090", "Address and port of the TrueSkill service to connect to")
	flag.Float64("trueskill.mu", 25.0, "Initial mean of ratings")
	flag.Float64("trueskill.sigma", 8.333333333333334, "Initial standard deviation of ratings")

	// Files configuration flags
	flag.String("files.verification", "./files/verification", "Directory to store verification photos in")
	flag.String("files.photos", "./files/photos", "Directory to store robot photos in")

	// Parse flags
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	// Add to viper
	if err := viper.BindPFlags(pflag.CommandLine); err != nil {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"msg":"failed to add command line flags","caller":"config/sources.go:78","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
		os.Exit(1)
	}

	return
}

// Setup retrieval from environment
func bindEnv() {
	// Automatically replace '.' with '_' in variable names
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	// Set environment variable prefix
	viper.SetEnvPrefix("sat")

	// Automatically bind environment variables
	viper.AutomaticEnv()

	// Disable empty environment variables
	viper.AllowEmptyEnv(false)
}

// Setup remote configuration
func remoteConfig(provider, format, host, path string, secure bool, keyring string) {
	// Ensure format is valid
	if format != "json" && format != "yaml" && format != "toml" && format != "hcl" {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/sources.go:102","msg":"invalid configuration file format","options":["json","yaml","toml","hcl"]}\n`, time.Now().UnixNano())
		os.Exit(1)
	}

	// Ensure provider is valid
	if provider != "etcd" && provider != "consul" {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/sources.go:108","msg":"invalid configuration provider","options":["consul","etcd"]}\n`, time.Now().UnixNano())
		os.Exit(1)
	}

	// Add provider
	if secure {
		_ = viper.AddSecureRemoteProvider(provider, host, path, keyring)
	} else {
		_ = viper.AddRemoteProvider(provider, host, path)
	}

	// Set configuration file type
	viper.SetConfigType(format)

	// Load configuration
	if err := viper.ReadRemoteConfig(); err != nil {
		fmt.Printf(`{"level":"fatal","timestamp":%d,"caller":"config/sources.go:124","msg":"failed to load config from remote provider","error":"%s"}\n`, time.Now().UnixNano(), err.Error())
		os.Exit(1)
	}
}
