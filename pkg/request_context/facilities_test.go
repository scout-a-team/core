package request_context

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestDatabase(t *testing.T) {
	// Create mocked database and enforcer
	db := &mocks.Database{}

	// Create test handler
	testHandler := http.HandlerFunc(func(_ http.ResponseWriter, r *http.Request) {
		d := r.Context().Value(FacilityDatabase).(database.Database)

		// Ensure same as initialized
		assert.Equal(t, db, d)
	})

	// Run through middleware
	Database(db)(testHandler).
		ServeHTTP(httptest.NewRecorder(),
			httptest.NewRequest("GET", "/", nil))
}

func TestMail(t *testing.T) {
	// Create fake mail channel
	mailChan := make(chan mail.Message)

	// Create test handler
	testHandler := http.HandlerFunc(func(_ http.ResponseWriter, r *http.Request) {
		m := r.Context().Value(FacilityMail).(chan mail.Message)

		// Ensure same as initialized
		assert.Equal(t, mailChan, m)
	})

	// Run through middleware
	Mail(mailChan)(testHandler).
		ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("GET", "/", nil))
}

func TestConfig(t *testing.T) {
	// Create fake config
	cfg := config.Config{}

	// Create test handler
	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c := r.Context().Value(FacilityConfig).(config.Config)

		// Ensure same as initialized
		assert.Equal(t, cfg, c)
	})

	// Run through middleware
	Config(cfg)(testHandler).
		ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("GET", "/", nil))
}
