package request_context

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestEventKeyCtx_WithParameter(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Route("/{event}", func(r chi.Router) {
		r.Use(EventKeyCtx)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte(r.Context().Value(EventKey).(string)))
			assert.NoError(t, err)
		})
	})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/event-key", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, "event-key", recorder.Body.String())
}

func TestEventKeyCtx_WithoutParameter(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Use(EventKeyCtx)
	router.Get("/", func(_ http.ResponseWriter, _ *http.Request) {})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, 400, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "path parameter 'event' is required"}`, recorder.Body.String())
}
