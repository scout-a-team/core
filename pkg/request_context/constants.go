package request_context

const (
	JwtUser    = "jwt-user"
	JwtTokenId = "jwt-token-id"

	VerifiedTeam = "verified-team"

	TeamId   = "team-id"
	UserId   = "user-id"
	EventKey = "event-key"
	MatchKey = "match-key"

	FacilityDatabase = "facility-database"
	FacilityCache    = "facility-cache"
	FacilityMail     = "facility-mail"
	FacilityConfig   = "facility-config"
)
