package request_context

import (
	"context"
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// Add team id object id to request context
func TeamIdCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ensure path parameter is present
		rawId := chi.URLParam(r, "team")
		if rawId == "" {
			responses.Error(w, http.StatusBadRequest, "path parameter 'team' is required")
			return
		}

		// Parse path parameter
		teamId, err := primitive.ObjectIDFromHex(rawId)
		if err != nil {
			responses.Error(w, http.StatusBadRequest, "invalid team id format for path parameter 'team'")
			return
		}

		ctx := context.WithValue(r.Context(), TeamId, teamId)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
