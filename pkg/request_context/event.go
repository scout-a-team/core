package request_context

import (
	"context"
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"net/http"
)

// Add event key to request context
func EventKeyCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ensure path parameter is present
		eventKey := chi.URLParam(r, "event")
		if eventKey == "" {
			responses.Error(w, http.StatusBadRequest, "path parameter 'event' is required")
			return
		}

		ctx := context.WithValue(r.Context(), EventKey, eventKey)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
