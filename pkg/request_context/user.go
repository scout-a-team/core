package request_context

import (
	"context"
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// Add user id to request context
func UserIdCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ensure path parameter is present
		rawId := chi.URLParam(r, "user")
		if rawId == "" {
			responses.Error(w, http.StatusBadRequest, "path parameter 'user' is required")
			return
		}

		// Parse path parameter
		userId, err := primitive.ObjectIDFromHex(rawId)
		if err != nil {
			responses.Error(w, http.StatusBadRequest, "invalid user id format for path parameter 'user'")
			return
		}

		ctx := context.WithValue(r.Context(), UserId, userId)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
