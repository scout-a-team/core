package request_context

import (
	"github.com/go-redis/redis/v7"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

// Get database facility
func GetDatabaseFacility(r *http.Request) database.Database {
	return r.Context().Value(FacilityDatabase).(database.Database)
}

// Get cache facility
func GetCacheFacility(r *http.Request) *redis.Client {
	return r.Context().Value(FacilityCache).(*redis.Client)
}

// Get mail facility
func GetMailFacility(r *http.Request) chan mail.Message {
	return r.Context().Value(FacilityMail).(chan mail.Message)
}

// Get configuration facility
func GetConfigFacility(r *http.Request) config.Config {
	return r.Context().Value(FacilityConfig).(config.Config)
}

// Get the user information of the parsed token
func GetTokenUser(r *http.Request) database.User {
	return r.Context().Value(JwtUser).(database.User)
}

// Get the token id of the parsed token
func GetTokenId(r *http.Request) primitive.ObjectID {
	return r.Context().Value(JwtTokenId).(primitive.ObjectID)
}

// Get the team id from the path parameters
func GetTeamId(r *http.Request) primitive.ObjectID {
	return r.Context().Value(TeamId).(primitive.ObjectID)
}

// Get the user id from the path parameters
func GetUserId(r *http.Request) primitive.ObjectID {
	return r.Context().Value(UserId).(primitive.ObjectID)
}

// Get the event key from the path parameters
func GetEventKey(r *http.Request) string {
	return r.Context().Value(EventKey).(string)
}

// Get the match key from the path parameters
func GetMatchKey(r *http.Request) string {
	return r.Context().Value(MatchKey).(string)
}

// Get whether the team is verified or not
func GetTeamVerified(r *http.Request) bool {
	return r.Context().Value(VerifiedTeam).(bool)
}
