package request_context

import (
	"context"
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"net/http"
)

// Add match key to request context
func MatchKeyCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ensure path parameter is present
		matchKey := chi.URLParam(r, "match")
		if matchKey == "" {
			responses.Error(w, http.StatusBadRequest, "path parameter 'match' is required")
			return
		}

		ctx := context.WithValue(r.Context(), MatchKey, matchKey)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
