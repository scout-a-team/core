package request_context

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/scout-a-team/core/pkg/config"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestGetDatabaseFacility(t *testing.T) {
	db := &mocks.Database{}

	// Create new context
	ctx := context.WithValue(context.Background(), FacilityDatabase, db)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get values from context and ensure equal
	d := GetDatabaseFacility(r)
	assert.Equal(t, db, d)
}

func TestGetConfigFacility(t *testing.T) {
	cfg := config.Config{}

	// Create new context
	ctx := context.WithValue(context.Background(), FacilityConfig, cfg)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get values from context and ensure equal
	c := GetConfigFacility(r)
	assert.Equal(t, cfg, c)
}

func TestGetTokenUser(t *testing.T) {
	user := database.User{
		Id:            primitive.NewObjectID(),
		Name:          "Some User",
		Email:         "some@us.ser",
		Password:      "some-password-thats-hashed",
		Role:          "some-role",
		TeamId:        "some-team-id",
		Enabled:       true,
		VerifiedEmail: true,
		TOTP:          database.TwoFactor{},
		WebAuthn:      database.TwoFactor{},
		RecoveryCodes: nil,
	}

	// Create new context
	ctx := context.WithValue(context.Background(), JwtUser, user)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, user, GetTokenUser(r))
}

func TestGetTokenId(t *testing.T) {
	id := primitive.NewObjectID()
	// Create new context
	ctx := context.WithValue(context.Background(), JwtTokenId, id)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, id, GetTokenId(r))
}

func TestGetTeamId(t *testing.T) {
	// Create new context
	id := primitive.NewObjectID()
	ctx := context.WithValue(context.Background(), TeamId, id)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, id, GetTeamId(r))
}

func TestGetUserId(t *testing.T) {
	// Create new context
	id := primitive.NewObjectID()
	ctx := context.WithValue(context.Background(), UserId, id)

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, id, GetUserId(r))
}

func TestGetMatchKey(t *testing.T) {
	// Create new context
	ctx := context.WithValue(context.Background(), MatchKey, "some-match-key")

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, "some-match-key", GetMatchKey(r))
}

func TestGetEventKey(t *testing.T) {
	// Create new context
	ctx := context.WithValue(context.Background(), EventKey, "some-event-key")

	// Create request
	r := httptest.NewRequest("GET", "/", nil)
	r = r.WithContext(ctx)

	// Get value from context and ensure equal
	assert.Equal(t, "some-event-key", GetEventKey(r))
}
