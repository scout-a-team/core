package request_context

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMatchKeyCtx_WithParameter(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Route("/{match}", func(r chi.Router) {
		r.Use(MatchKeyCtx)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte(r.Context().Value(MatchKey).(string)))
			assert.NoError(t, err)
		})
	})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/match-key", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure the values are correct
	assert.Equal(t, "match-key", recorder.Body.String())
}

func TestMatchKeyCtx_WithoutParameter(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Use(MatchKeyCtx)
	router.Get("/", func(_ http.ResponseWriter, _ *http.Request) {})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, 400, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "path parameter 'match' is required"}`, recorder.Body.String())
}
