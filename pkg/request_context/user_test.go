package request_context

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestUserIdCtx_WithValidId(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Route("/{user}", func(r chi.Router) {
		r.Use(UserIdCtx)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte(r.Context().Value(UserId).(primitive.ObjectID).Hex()))
			assert.NoError(t, err)
		})
	})

	id := primitive.NewObjectID()

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/"+id.Hex(), nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, id.Hex(), recorder.Body.String())
}

func TestUserIdCtx_WithInvalidId(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Route("/{user}", func(r chi.Router) {
		r.Use(UserIdCtx)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte(r.Context().Value(UserId).(primitive.ObjectID).Hex()))
			assert.NoError(t, err)
		})
	})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/invalid-id", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, `{"status": "error", "reason": "invalid user id format for path parameter 'user'"}`, recorder.Body.String())
}

func TestUserIdCtx_WithoutParameter(t *testing.T) {
	// Setup router w/ middleware
	router := chi.NewRouter()
	router.Use(UserIdCtx)
	router.Get("/", func(_ http.ResponseWriter, _ *http.Request) {})

	// Setup the recorder
	recorder := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)

	// Run the request
	router.ServeHTTP(recorder, request)

	// Ensure values are correct
	assert.Equal(t, 400, recorder.Code)
	assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "path parameter 'user' is required"}`, recorder.Body.String())
}
