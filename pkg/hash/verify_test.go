package hash

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestVerify_MatchingHash(t *testing.T) {
	match, err := Verify("testpass", "$argon2id$v=19$m=32768,t=4,p=4$UUf4fn8$FS5Zjt8")
	assert.True(t, match)
	assert.NoError(t, err)
}

func TestVerify_NotMatchingHash(t *testing.T) {
	match, err := Verify("wrong-pass", "$argon2id$v=19$m=32768,t=4,p=4$UUf4fn8$FS5Zjt8")
	assert.False(t, match)
	assert.NoError(t, err)
}

func TestVerify_InvalidHash(t *testing.T) {
	match, err := Verify("testpass", "=19$m=32768,t=4,p=4$UUf4fn8$FS5Zjt8")
	assert.False(t, match)
	assert.Equal(t, ErrInvalidHash, err)
}

func TestVerify_InvalidVersion(t *testing.T) {
	match, err := Verify("testpass", "$argon2id$v=20$m=32768,t=4,p=4$UUf4fn8$FS5Zjt8")
	assert.False(t, match)
	assert.Equal(t, ErrIncompatibleVersion, err)
}
