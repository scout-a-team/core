package hash

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDefaultHash(t *testing.T) {
	_, err := DefaultHash("testpass")
	assert.NoError(t, err)
}

func TestHash(t *testing.T) {
	_, err := Hash("testpass", defaultParams)
	assert.NoError(t, err)
}
