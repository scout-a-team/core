package hash

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var defaultParams = &Params{
	memory:      32 * 1024,
	iterations:  4,
	parallelism: 4,
	saltLength:  5,
	keyLength:   5,
}

func TestRandomBytes(t *testing.T) {
	_, err := randomBytes(32)
	assert.NoError(t, err)
}

func TestEncodeHash(t *testing.T) {
	encoded := "$argon2id$v=19$m=32768,t=4,p=4$d29ybGQ$aGVsbG8"
	assert.Equal(t, encoded, encodeHash([]byte("hello"), []byte("world"), defaultParams))
}

func TestDecodeHash_DecodingParts(t *testing.T) {
	params, salt, hash, err := decodeHash("$argon2id$v=19$m=32768,t=4,p=4$d29ybGQ$aGVsbG8")
	assert.Equal(t, defaultParams, params)
	assert.Equal(t, []byte("world"), salt)
	assert.Equal(t, []byte("hello"), hash)
	assert.NoError(t, err)
}

func TestDecodeHash_InvalidHash(t *testing.T) {
	_, _, _, err := decodeHash("=19$m=32768,t=4,p=4$d29ybGQ$aGVsbG8")
	assert.Equal(t, err, ErrInvalidHash)
}

func TestDecodeHash_InvalidVersion(t *testing.T) {
	_, _, _, err := decodeHash("$argon2id$v=20$m=32768,t=4,p=4$d29ybGQ$aGVsbG8")
	assert.Equal(t, err, ErrIncompatibleVersion)
}
