package middleware

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strings"
)

func Verification(db database.Database) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Allow if verifying email, registering, logging in or logging out
			if (r.URL.Path == "/verification/email" && r.Method == "GET") || (r.URL.Path == "/authentication/login" && r.Method == "POST") ||
				(r.URL.Path == "/authentication/register" && r.Method == "POST") || (r.URL.Path == "/authentication/logout" && r.Method == "GET") ||
				(r.URL.Path == "/authentication/forgot-password" && r.Method == "GET") || (r.URL.Path == "/verification/reset-password" && r.Method == "POST") {
				next.ServeHTTP(w, r)
				return
			}

			// Bypass check if authenticating, verifying team, or checking status
			bypass := false
			if (r.URL.Path == "/authentication/refresh" && r.Method == "GET") || (r.URL.Path == "/verification/team" && (r.Method == "GET" || r.Method == "POST")) || (strings.HasPrefix(r.URL.Path, "/teams/") && strings.HasSuffix(r.URL.Path, "/verified") && r.Method == "GET") {
				bypass = true
			}

			// Check if verified
			tid, _ := primitive.ObjectIDFromHex(request_context.GetTokenUser(r).TeamId)
			var team database.Team
			if err := db.Collection("teams").FindOne(bson.M{"_id": tid}).Decode(&team); err == mongo.ErrNoDocuments {
				responses.Error(w, http.StatusNotFound, "team in token no longer exists")
				return
			} else if err != nil {
				zap.L().Named("middleware.verification").Error("failed to query database for team", zap.String("team", tid.Hex()))
				responses.Error(w, http.StatusInternalServerError, "failed to query database")
				return
			}

			// Ensure team is verified
			if !bypass && !*team.Verified {
				responses.Error(w, http.StatusForbidden, "team not verified")
				return
			}

			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), request_context.VerifiedTeam, *team.Verified)))
		})
	}
}
