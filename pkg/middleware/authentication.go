package middleware

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"net/http"
)

// Require authentication tokens for all endpoints except for login and register
func Authentication(db database.Database) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Allow if authenticating or registering
			if (r.URL.Path == "/authentication/login" && r.Method == "POST") || (r.URL.Path == "/authentication/register" && r.Method == "POST") ||
				(r.URL.Path == "/authentication/forgot-password" && r.Method == "GET") {
				next.ServeHTTP(w, r)
				return
			}

			// Check if authorization header is present
			if r.Header.Get("Authorization") == "" {
				responses.Error(w, http.StatusUnauthorized, "header 'Authorization' is required")
				return
			}

			// Select which type of token to use
			tokenType := database.TokenAuthentication
			if r.URL.Path == "/verification/email" && r.Method == "GET" {
				tokenType = database.TokenEmailVerification
			} else if r.URL.Path == "/verification/reset-password" && r.Method == "POST" {
				tokenType = database.TokenPasswordReset
			} else if r.URL.Path == "/authentication/2fa/totp" && r.Method == "GET" {
				tokenType = database.Token2faLogin
			}

			// Validate JWT
			token, err := jwt.Validate(r.Header.Get("Authorization"), tokenType, db)
			if err != nil {
				responses.Error(w, http.StatusUnauthorized, "invalid token: "+err.Error())
				return
			}

			// Get token claims
			claims := jwt.Claims(token)

			// Parse token ids
			subjectId, _ := primitive.ObjectIDFromHex(claims.Subject)
			tokenId, _ := primitive.ObjectIDFromHex(token.Header["kid"].(string))

			// Get user from database
			var user database.User
			if err := db.Collection("users").FindOne(bson.M{"_id": subjectId}).Decode(&user); err != nil {
				zap.L().Named("middleware.authentication").Error("failed to query database for user in token", zap.String("user", subjectId.Hex()), zap.String("token", tokenId.Hex()))
				responses.Error(w, http.StatusInternalServerError, "failed to query database")
				return
			}

			// Set user authentication values in request context
			ctx := context.WithValue(r.Context(), request_context.JwtUser, user)
			ctx = context.WithValue(ctx, request_context.JwtTokenId, tokenId)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
