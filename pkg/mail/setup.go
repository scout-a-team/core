package mail

import (
	"context"
	"github.com/mailgun/mailgun-go/v3"
	"gitlab.com/scout-a-team/core/pkg/config"
	"go.uber.org/zap"
	"time"
)

// Create a connection to MailGun
func Setup(cfg config.Config) (mailgun.Mailgun, chan Message, context.CancelFunc) {
	// Create logger
	logger := zap.L().Named("mail")

	// Create client
	mg := mailgun.NewMailgun(cfg.Mail.Domain, cfg.Mail.ApiKey)
	logger.Debug("created mailgun client")

	// Ensure working
	ctx, timeoutCancel := context.WithTimeout(context.Background(), time.Second*15)
	defer timeoutCancel()
	domain, err := mg.GetDomain(ctx, cfg.Mail.Domain)
	if err != nil {
		logger.Fatal("failed to ensure valid credentials", zap.Error(err))
	}
	logger.Debug("connected to mailgun client", zap.String("domain", domain.Domain.State))

	// Start the sender daemon
	messages := make(chan Message)
	ctx, cancel := context.WithCancel(context.Background())
	go mailDaemon(mg, cfg.Mail.From, cfg.Mail.FromName, messages, ctx)

	return mg, messages, cancel
}
