package mail

import (
	"context"
	"github.com/mailgun/mailgun-go/v3"
	"go.uber.org/zap"
	"time"
)

type Message struct {
	Subject   string
	Recipient string
	Template  string
	Variables map[string]interface{}
}

func mailDaemon(mg mailgun.Mailgun, from, fromName string, messages chan Message, ctx context.Context) {
	logger := zap.L().Named("mail.sender")

	for {
		select {
		// Wait for messages to send
		case m, ok := <-messages:
			if !ok {
				logger.Error("invalid message received from channel")
				return
			}

			// Create message
			message := mg.NewMessage(fromName+" <"+from+">", m.Subject, "", m.Recipient)
			message.SetTemplate(m.Template)
			message.SetReplyTo(from)

			// Add template variables
			for key, value := range m.Variables {
				message.AddTemplateVariable(key, value)
			}

			// Send the message
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
			if _, _, err := mg.Send(ctx, message); err != nil {
				logger.Error("failed to send message", zap.Error(err))
				cancel()
			}
			cancel()

		case <-ctx.Done():
			return
		}
	}
}
