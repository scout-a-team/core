package cache

import (
	"github.com/go-redis/redis/v7"
	"gitlab.com/scout-a-team/core/pkg/config"
	"go.uber.org/zap"
)

// Create a connection to a Redis cache
func Connect(cfg config.Config) *redis.Client {
	// Create the logger
	logger := zap.L().Named("cache")

	// Setup connection
	client := redis.NewClient(&redis.Options{
		Addr:     cfg.Cache.Address,
		Password: cfg.Cache.Password,
		DB:       cfg.Cache.Database,
	})
	logger.Debug("created cache client with specified configuration")

	// Ensure connection is working
	if _, err := client.Ping().Result(); err != nil {
		logger.Fatal("failed to connect to redis", zap.Error(err))
	}
	logger.Debug("initialized connection to cache")

	return client
}
