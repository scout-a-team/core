package jwt

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"strings"
	"testing"
)

var user = database.User{
	Id:       primitive.NewObjectID(),
	Name:     "Test User",
	Email:    "test@test.com",
	Password: "some password",
	Role:     "owner",
	TeamId:   primitive.NewObjectID().Hex(),
	Enabled:  true,
}

func TestGenerateToken(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	id, err := primitive.ObjectIDFromHex("5e0d32d4eaeb7fad81cb9a5a")
	assert.NoError(t, err)

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil)

	// Create claims for testing
	claims := jwt.StandardClaims{
		ExpiresAt: 1577836900,
		IssuedAt:  1577836800,
		Subject:   "some subject",
	}

	// Generate test token
	signed, insertedId, err := generateToken(1, claims, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", "some subject", db)

	// Validate data returned data
	assert.NoError(t, err)
	assert.Equal(t, id.Hex(), insertedId)

	// Validate token data
	segments := strings.Split(signed, ".")
	assert.Equal(t, "eyJhbGciOiJIUzUxMiIsImtpZCI6IjVlMGQzMmQ0ZWFlYjdmYWQ4MWNiOWE1YSIsInR5cCI6IkpXVCJ9", segments[0])
	assert.Equal(t, "eyJleHAiOjE1Nzc4MzY5MDAsImlhdCI6MTU3NzgzNjgwMCwic3ViIjoic29tZSBzdWJqZWN0In0", segments[1])
}

func TestGenerateToken_WithDatabaseError(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(nil, errors.New("failed to insert into database"))

	// Create claims for testing
	claims := jwt.StandardClaims{
		ExpiresAt: 1577836900,
		IssuedAt:  1577836800,
		Subject:   "some subject",
	}

	// Generate test token
	signed, insertedId, err := generateToken(1, claims, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", "some subject", db)

	// Validate data returned data
	assert.Error(t, err)
	assert.Equal(t, "", signed)
	assert.Equal(t, "", insertedId)
}

func TestGenerateAuthentication(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	id, err := primitive.ObjectIDFromHex("5e0d32d4eaeb7fad81cb9a5a")
	assert.NoError(t, err)

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil)

	// Generate token
	token, insertedId, err := GenerateAuthentication("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", user, db)

	// Ensure equal
	assert.NoError(t, err)
	assert.Equal(t, insertedId, id.Hex())
	assert.NotEmpty(t, token)
}

func TestGenerateEmailVerification(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	id, err := primitive.ObjectIDFromHex("5e0d32d4eaeb7fad81cb9a5a")
	assert.NoError(t, err)

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil)

	// Generate token
	token, insertedId, err := GenerateEmailVerification("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", user, db)

	// Ensure equal
	assert.NoError(t, err)
	assert.Equal(t, insertedId, id.Hex())
	assert.NotEmpty(t, token)
}

func TestGeneratePasswordReset(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	id, err := primitive.ObjectIDFromHex("5e0d32d4eaeb7fad81cb9a5a")
	assert.NoError(t, err)

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil)

	// Generate token
	token, insertedId, err := GeneratePasswordReset("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", user, db)

	// Ensure equal
	assert.NoError(t, err)
	assert.Equal(t, insertedId, id.Hex())
	assert.NotEmpty(t, token)
}

func TestGenerateTwoFactorLogin(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	id, err := primitive.ObjectIDFromHex("5e0d32d4eaeb7fad81cb9a5a")
	assert.NoError(t, err)

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil)

	// Generate token
	token, insertedId, err := Generate2faLogin("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0", user, db)

	// Ensure equal
	assert.NoError(t, err)
	assert.Equal(t, insertedId, id.Hex())
	assert.NotEmpty(t, token)
}
