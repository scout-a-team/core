package jwt

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Validate a token of a specified type given the signed string
func Validate(tokenString string, tokenType int, db database.Database) (token *jwt.Token, err error) {
	// Retrieve parsed token
	token, err = jwt.ParseWithClaims(tokenString, &jwt.StandardClaims{}, func(token *jwt.Token) (i interface{}, e error) {
		// Ensure proper signing method and id in header
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		} else if _, ok := token.Header["kid"]; !ok {
			return nil, fmt.Errorf("token header 'kid' is required")
		}

		// Decode token id
		tokenId, _ := primitive.ObjectIDFromHex(token.Header["kid"].(string))

		// Get signing key from database
		var t database.Token
		if err := db.Collection("tokens").FindOne(bson.M{"_id": tokenId}).Decode(&t); err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("failed to find signing key for token")
		} else if err != nil {
			return nil, err
		}

		// Ensure key is of proper type
		if tokenType != t.Type {
			var tokenTypeString string
			switch tokenType {
			case database.TokenAuthentication:
				tokenTypeString = "an authentication"
			case database.TokenEmailVerification:
				tokenTypeString = "an email verification"
			case database.TokenPasswordReset:
				tokenTypeString = "a password reset"
			case database.Token2faLogin:
				tokenTypeString = "a two factor login"
			}
			return nil, fmt.Errorf("must be " + tokenTypeString + " token")
		}

		return t.SigningKey, nil
	})
	if err != nil {
		return nil, err
	}

	return token, nil
}
