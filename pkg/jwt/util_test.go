package jwt

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUnvalidated(t *testing.T) {
	token := Unvalidated("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
	assert.NotNil(t, token)
	assert.False(t, token.Valid)
}

func TestClaims_Valid(t *testing.T) {
	token := &jwt.Token{
		Claims: &jwt.StandardClaims{
			ExpiresAt: 100,
			IssuedAt:  100,
			Issuer:    "scout-a-team",
			Subject:   "some-user",
		},
	}

	claims := Claims(token)
	assert.NotNil(t, claims)
}

func TestClaims_Invalid(t *testing.T) {
	token := &jwt.Token{
		Claims: jwt.StandardClaims{
			ExpiresAt: 100,
			IssuedAt:  100,
			Issuer:    "scout-a-team",
			Subject:   "some-user",
		},
	}

	claims := Claims(token)
	assert.Nil(t, claims)
}
