package jwt

import (
	"github.com/dgrijalva/jwt-go"
)

// Get the parts of a token without re-validating
func Unvalidated(tokenString string) *jwt.Token {
	parser := jwt.Parser{}
	token, _, _ := parser.ParseUnverified(tokenString, &jwt.StandardClaims{})
	return token
}

// Get the map claims of a token
func Claims(token *jwt.Token) *jwt.StandardClaims {
	if claims, ok := token.Claims.(*jwt.StandardClaims); !ok {
		return nil
	} else {
		return claims
	}
}
