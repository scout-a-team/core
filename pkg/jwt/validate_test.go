package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"testing"
	"time"
)

func TestValidate_WithAuthenticationToken(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	id := primitive.NewObjectID()

	var dbEntry database.Token
	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		dbEntry = args.Get(0).(database.Token)
	})
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = dbEntry.Type
		t.SigningKey = dbEntry.SigningKey
	})

	// Generate token
	signed, _, err := GenerateAuthentication("", user, db)
	assert.NoError(t, err)

	// Validate token
	token, err := Validate(signed, database.TokenAuthentication, db)
	assert.NoError(t, err)

	// Validate token data
	assert.Equal(t, id.Hex(), token.Header["kid"].(string))
	assert.Equal(t, jwt.SigningMethodHS512, token.Method)
	assert.True(t, token.Valid)
}

func TestValidate_WithEmailVerificationToken(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	id := primitive.NewObjectID()

	var dbEntry database.Token
	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		dbEntry = args.Get(0).(database.Token)
	})
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = dbEntry.Type
		t.SigningKey = dbEntry.SigningKey
	})

	// Generate token
	signed, _, err := GenerateEmailVerification("", database.User{}, db)
	assert.NoError(t, err)

	// Validate token
	token, err := Validate(signed, database.TokenEmailVerification, db)
	assert.NoError(t, err)

	// Validate token data
	assert.Equal(t, id.Hex(), token.Header["kid"].(string))
	assert.Equal(t, jwt.SigningMethodHS512, token.Method)
	assert.True(t, token.Valid)
}

func TestValidate_WithPasswordResetToken(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	id := primitive.NewObjectID()

	var dbEntry database.Token
	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		dbEntry = args.Get(0).(database.Token)
	})
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = dbEntry.Type
		t.SigningKey = dbEntry.SigningKey
	})

	// Generate token
	signed, _, err := GeneratePasswordReset("", database.User{}, db)
	assert.NoError(t, err)

	// Validate token
	token, err := Validate(signed, database.TokenPasswordReset, db)
	assert.NoError(t, err)

	// Validate token data
	assert.Equal(t, id.Hex(), token.Header["kid"].(string))
	assert.Equal(t, jwt.SigningMethodHS512, token.Method)
	assert.True(t, token.Valid)
}

func TestValidate_InvalidSigningMethod(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}

	// Generate token
	pk, err := rsa.GenerateKey(rand.Reader, 2048)
	assert.NoError(t, err)
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.StandardClaims{})
	signed, err := token.SignedString(pk)
	assert.NoError(t, err)

	// Validate token
	_, err = Validate(signed, database.TokenAuthentication, db)
	assert.EqualError(t, err, "unexpected signing method: RS256")
}

func TestValidate_InvalidKeyIdHeader(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}

	// Generate token
	signingKey := make([]byte, 128)
	_, err := rand.Read(signingKey)
	assert.NoError(t, err)
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.StandardClaims{})
	signed, err := token.SignedString(signingKey)
	assert.NoError(t, err)

	// Validate token
	_, err = Validate(signed, database.TokenAuthentication, db)
	assert.EqualError(t, err, "token header 'kid' is required")
}

func TestValidate_WithDatabaseError(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(errors.New("some database error"))

	// Generate test token
	signed, _, err := generateToken(2, jwt.StandardClaims{}, "", "some-id", db)
	assert.NoError(t, err)

	// Validate token
	_, err = Validate(signed, 2, db)
	assert.EqualError(t, err, "some database error")
}

func TestValidate_WithNotFoundSigningKey(t *testing.T) {
	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(mongo.ErrNoDocuments)

	// Generate test token
	signed, _, err := generateToken(2, jwt.StandardClaims{}, "", "some-id", db)
	assert.NoError(t, err)

	// Validate token
	_, err = Validate(signed, 2, db)
	assert.EqualError(t, err, "failed to find signing key for token")
}

func TestValidate_InvalidTokenType(t *testing.T) {
	id := primitive.NewObjectID()

	// Setup mocked authentication database
	var authToken database.Token
	dbAuth := &mocks.Database{}
	collectionAuth := &mocks.Collection{}
	resultAuth := &mocks.FindResult{}
	dbAuth.On("Collection", "tokens").Return(collectionAuth)
	collectionAuth.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		authToken = args.Get(0).(database.Token)
	})
	collectionAuth.On("FindOne", mock.AnythingOfType("primitive.M")).Return(resultAuth)
	resultAuth.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = 100
		t.SigningKey = authToken.SigningKey
	})

	// Setup mocked email verification database
	var emailVerificationToken database.Token
	dbEmailVerification := &mocks.Database{}
	collectionEmailVerification := &mocks.Collection{}
	resultEmailVerification := &mocks.FindResult{}
	dbEmailVerification.On("Collection", "tokens").Return(collectionEmailVerification)
	collectionEmailVerification.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		emailVerificationToken = args.Get(0).(database.Token)
	})
	collectionEmailVerification.On("FindOne", mock.AnythingOfType("primitive.M")).Return(resultEmailVerification)
	resultEmailVerification.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = 100
		t.SigningKey = emailVerificationToken.SigningKey
	})

	// Setup mocked password reset database
	var passwordResetToken database.Token
	dbPasswordReset := &mocks.Database{}
	collectionPasswordReset := &mocks.Collection{}
	resultPasswordReset := &mocks.FindResult{}
	dbPasswordReset.On("Collection", "tokens").Return(collectionPasswordReset)
	collectionPasswordReset.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		passwordResetToken = args.Get(0).(database.Token)
	})
	collectionPasswordReset.On("FindOne", mock.AnythingOfType("primitive.M")).Return(resultPasswordReset)
	resultPasswordReset.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = 100
		t.SigningKey = passwordResetToken.SigningKey
	})

	// Setup mocked password reset database
	var twoFactorLoginToken database.Token
	dbTwoFactorLogin := &mocks.Database{}
	collectionTwoFactorLogin := &mocks.Collection{}
	resultTwoFactorLogin := &mocks.FindResult{}
	dbTwoFactorLogin.On("Collection", "tokens").Return(collectionTwoFactorLogin)
	collectionTwoFactorLogin.On("InsertOne", mock.AnythingOfType("database.Token")).Return(id, nil).Run(func(args mock.Arguments) {
		twoFactorLoginToken = args.Get(0).(database.Token)
	})
	collectionTwoFactorLogin.On("FindOne", mock.AnythingOfType("primitive.M")).Return(resultTwoFactorLogin)
	resultTwoFactorLogin.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = 100
		t.SigningKey = twoFactorLoginToken.SigningKey
	})

	// Generate test tokens
	authSigned, _, err := GenerateAuthentication("", database.User{}, dbAuth)
	assert.NoError(t, err)
	emailVerificationSigned, _, err := GenerateEmailVerification("", database.User{}, dbEmailVerification)
	assert.NoError(t, err)
	passwordResetSigned, _, err := GeneratePasswordReset("", database.User{}, dbPasswordReset)
	assert.NoError(t, err)
	twoFactorLoginSigned, _, err := Generate2faLogin("", database.User{}, dbTwoFactorLogin)
	assert.NoError(t, err)

	// Validate token
	_, err = Validate(authSigned, database.TokenAuthentication, dbAuth)
	assert.EqualError(t, err, "must be an authentication token")
	_, err = Validate(emailVerificationSigned, database.TokenEmailVerification, dbEmailVerification)
	assert.EqualError(t, err, "must be an email verification token")
	_, err = Validate(passwordResetSigned, database.TokenPasswordReset, dbPasswordReset)
	assert.EqualError(t, err, "must be a password reset token")
	_, err = Validate(twoFactorLoginSigned, database.Token2faLogin, dbTwoFactorLogin)
	assert.EqualError(t, err, "must be a two factor login token")
}

func TestValidate_ExpiredToken(t *testing.T) {
	// Generate signing key for token
	signingKey := make([]byte, 128)
	_, err := rand.Read(signingKey)
	assert.NoError(t, err)

	// Setup mocked database
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}

	db.On("Collection", "tokens").Return(collection)
	collection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Token")).Return(nil).Run(func(args mock.Arguments) {
		t := args.Get(0).(*database.Token)
		t.Type = database.TokenAuthentication
		t.SigningKey = signingKey
	})

	// Generate token
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() - authExpiration,
		IssuedAt:  time.Now().Unix(),
		Subject:   "some-subject",
	})
	token.Header["kid"] = primitive.NewObjectID().Hex()
	signed, err := token.SignedString(signingKey)

	// Validate token
	_, err = Validate(signed, database.TokenAuthentication, db)
	assert.EqualError(t, err, "token is expired by 8640h0m0s")
}
