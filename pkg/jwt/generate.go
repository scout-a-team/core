package jwt

import (
	"crypto/rand"
	"github.com/dgrijalva/jwt-go"
	"github.com/mssola/user_agent"
	"gitlab.com/scout-a-team/core/pkg/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

const (
	authExpiration              = 60 * 60 * 24 * 30 * 12
	emailVerificationExpiration = 60 * 60 * 24 * 7
	passwordResetExpiration     = 60 * 60 * 24 * 2
	twoFactorLoginExpiration    = 60 * 60 * 1
)

// Generate an authentication JWT
// Authenticates with resources to identify and display user authorization
func GenerateAuthentication(ua string, user database.User, db database.Database) (string, string, error) {
	return generateToken(database.TokenAuthentication, jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() + authExpiration,
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   user.Id.Hex(),
	}, ua, user.Id.Hex(), db)
}

// Generate a email validation JWT
// Allows for the verification of a user's email
func GenerateEmailVerification(ua string, user database.User, db database.Database) (string, string, error) {
	return generateToken(database.TokenEmailVerification, jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() + emailVerificationExpiration,
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   user.Id.Hex(),
	}, ua, user.Id.Hex(), db)
}

// Generate a password reset JWT
// Allows for the resetting of a user's password
func GeneratePasswordReset(ua string, user database.User, db database.Database) (string, string, error) {
	return generateToken(database.TokenPasswordReset, jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() + passwordResetExpiration,
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   user.Id.Hex(),
	}, ua, user.Id.Hex(), db)
}

// Generate a two factor authentication intermediary token.
// Forces users to use their second factor if its enabled
func Generate2faLogin(ua string, user database.User, db database.Database) (string, string, error) {
	return generateToken(database.Token2faLogin, jwt.StandardClaims{
		ExpiresAt: time.Now().Unix() + twoFactorLoginExpiration,
		IssuedAt:  time.Now().Unix(),
		NotBefore: time.Now().Unix(),
		Subject:   user.Id.Hex(),
	}, ua, user.Id.Hex(), db)
}

// Generate a generic token of type 't'
func generateToken(t int, claims jwt.Claims, ua, uid string, db database.Database) (string, string, error) {
	// Parse user agent
	agent := user_agent.New(ua)

	// Create os string
	os := agent.OS()
	if agent.Mobile() {
		os += " (Mobile)"
	}

	// Create browser string
	browser, version := agent.Browser()

	// Generate signing key for JWT
	signingKey := make([]byte, 128)
	if _, err := rand.Read(signingKey); err != nil {
		return "", "", err
	}

	// Save key to database
	tokenData := database.Token{
		SigningKey: signingKey,
		UserId:     uid,
		Type:       t,
		OS:         os,
		Browser:    browser + " " + version,
		IssuedAt:   time.Now(),
	}
	insertedToken, err := db.Collection("tokens").InsertOne(tokenData)
	if err != nil {
		return "", "", err
	}

	// Generate token with specified claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	token.Header["kid"] = insertedToken.(primitive.ObjectID).Hex()

	// Sign the token
	signed, err := token.SignedString(signingKey)
	if err != nil {
		return "", "", err
	}

	return signed, insertedToken.(primitive.ObjectID).Hex(), nil
}
