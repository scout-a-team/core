package responses

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestError(t *testing.T) {
	// Create mocked response writer
	recorder := httptest.NewRecorder()

	// Generate the error response
	Error(recorder, http.StatusBadRequest, "invalid body")

	// Check headers and status code
	assert.Equal(t, recorder.Code, http.StatusBadRequest)
	assert.Equal(t, recorder.Header()["Content-Type"][0], "application/json")

	// Read body for checking
	assert.Equal(t, recorder.Body.String(), `{"status": "error", "reason": "invalid body"}`)
}
