package responses

import (
	"go.uber.org/zap"
	"net/http"
)

// Send a generic success response
func Success(w http.ResponseWriter) {
	// Create the logger
	logger := zap.L().Named("responses").With(zap.Int("status", 200), zap.String("type", "success"))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write([]byte(`{"status": "success"}`)); err != nil {
		logger.Error("failed to write response", zap.Error(err))
	}
	logger.Debug("new response")
}

// Send a generic success response with a specific status code
func SuccessWithStatus(w http.ResponseWriter, status int) {
	// Create the logger
	logger := zap.L().Named("responses").With(zap.Int("status", status), zap.String("type", "success-status"))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if _, err := w.Write([]byte(`{"status": "success"}`)); err != nil {
		logger.Error("failed to write response", zap.Error(err))
	}
	logger.Debug("new response")
}
