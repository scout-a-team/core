package responses

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSuccess(t *testing.T) {
	// Create mocked response writer
	recorder := httptest.NewRecorder()

	// Generate success response
	Success(recorder)

	// Check headers and status code
	assert.Equal(t, recorder.Code, http.StatusOK)
	assert.Equal(t, recorder.Header()["Content-Type"][0], "application/json")

	// Read body for checking
	assert.Equal(t, recorder.Body.String(), `{"status": "success"}`)
}

func TestSuccessWithStatus(t *testing.T) {
	// Create mocked response writer
	recorder := httptest.NewRecorder()

	// Generate success with status response
	SuccessWithStatus(recorder, http.StatusNoContent)

	// Check headers and status code
	assert.Equal(t, recorder.Code, http.StatusNoContent)
	assert.Equal(t, recorder.Header()["Content-Type"][0], "application/json")

	// Read body for checking
	assert.Equal(t, recorder.Body.String(), `{"status": "success"}`)
}
