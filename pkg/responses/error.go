package responses

import (
	"fmt"
	"go.uber.org/zap"
	"net/http"
)

// Send an error response with a reason
func Error(w http.ResponseWriter, status int, reason string) {
	// Create the logger
	logger := zap.L().Named("responses").With(zap.Int("status", status), zap.String("type", "error"))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if _, err := w.Write([]byte(fmt.Sprintf(`{"status": "error", "reason": "%s"}`, reason))); err != nil {
		logger.Error("failed to write response", zap.Error(err))
	}
	logger.Debug("new response")
}
