package responses

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
)

// Send a success response with some data.
// The response data must be JSON serializable.
func SuccessWithData(w http.ResponseWriter, data interface{}) {
	// Create the logger
	logger := zap.L().Named("responses").With(zap.Int("status", 200), zap.String("type", "success-data"))

	// Encode data to JSON
	encoded, _ := json.Marshal(data)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write([]byte(`{"status": "success", "data": ` + string(encoded) + `}`)); err != nil {
		logger.Error("failed to write response", zap.Error(err))
	}
	logger.Debug("new response")
}

// Send a success response with some data and a specific status code.
// The response data must be JSON serializable.
func SuccessWithDataAndStatus(w http.ResponseWriter, data interface{}, status int) {
	// Create the logger
	logger := zap.L().Named("responses").With(zap.Int("status", status), zap.String("type", "success-data-status"))

	// Encode data to JSON
	encoded, _ := json.Marshal(data)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if _, err := w.Write([]byte(`{"status": "success", "data": ` + string(encoded) + `}`)); err != nil {
		logger.Error("failed to write response", zap.Error(err))
	}
	logger.Debug("new response")
}
