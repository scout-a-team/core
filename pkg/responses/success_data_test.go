package responses

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSuccessWithData(t *testing.T) {
	// Create mocked response writer
	recorder := httptest.NewRecorder()

	// Generate success with data response
	SuccessWithData(recorder, []interface{}{"string", 1, 1.2, true})

	// Check headers and status code
	assert.Equal(t, recorder.Code, http.StatusOK)
	assert.Equal(t, recorder.Header()["Content-Type"][0], "application/json")

	// Read body for checking
	assert.Equal(t, recorder.Body.String(), `{"status": "success", "data": ["string",1,1.2,true]}`)
}

func TestSuccessWithDataAndStatus(t *testing.T) {
	// Create mocked response writer
	recorder := httptest.NewRecorder()

	// Generate success with data and status response
	SuccessWithDataAndStatus(recorder, []interface{}{"string", 1, 1.2, true}, http.StatusNoContent)

	// Check headers and status code
	assert.Equal(t, recorder.Code, http.StatusNoContent)
	assert.Equal(t, recorder.Header()["Content-Type"][0], "application/json")

	// Read body for checking
	assert.Equal(t, recorder.Body.String(), `{"status": "success", "data": ["string",1,1.2,true]}`)
}
