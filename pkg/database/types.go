package database

import (
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/trueskill"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

// Team information
type Team struct {
	Id              primitive.ObjectID   `json:"id" bson:"_id,omitempty"`
	Name            string               `json:"name" bson:"name"`
	Number          int                  `json:"number" bson:"number"`
	City            string               `json:"city" bson:"city"`
	Country         string               `json:"country" bson:"country"`
	Registered      *bool                `json:"-" bson:"registered,omitempty"`
	Verified        *bool                `json:"-" bson:"verified,omitempty"`
	TbaUrl          string               `json:"tba_url" bson:"tba_url"`
	EventIds        []string             `json:"-" bson:"events,omitempty"`
	Pit             map[string]PitScout  `json:"-" bson:"pit"`
	GlobalTrueSkill TrueSkill            `json:"trueskill" bson:"trueskill"`
	TeamTrueSkill   map[string]TrueSkill `json:"-" bson:"team_trueskill"`
}

// User information
type User struct {
	Id            primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name          string             `json:"name" bson:"name"`
	Email         string             `json:"email" bson:"email"`
	Password      string             `json:"-" bson:"password"`
	Role          rbac.Key           `json:"role" bson:"role"`
	TeamId        string             `json:"-" bson:"team_id"`
	Enabled       bool               `json:"enabled" bson:"enabled"`
	VerifiedEmail bool               `json:"email_verified" bson:"email_verified"`
	TOTP          TwoFactor          `json:"-" bson:"totp"`
	WebAuthn      TwoFactor          `json:"-" bson:"webauthn"`
	RecoveryCodes []string           `json:"-" bson:"recovery_codes"`
}

// Two factor authentication description
type TwoFactor struct {
	Secret  string `bson:"secret"`
	Enabled bool   `bson:"enabled"`
}

// User token information
type Token struct {
	Id         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	SigningKey []byte             `json:"-" bson:"signing_key"`
	UserId     string             `json:"-" bson:"user_id"`
	Type       int                `json:"-" bson:"type"`
	OS         string             `json:"os" bson:"os"`
	Browser    string             `json:"browser" bson:"browser"`
	IssuedAt   time.Time          `json:"issued_at" bson:"issued_at"`
}

// Pending verifications
type Verification struct {
	Id     primitive.ObjectID `bson:"_id,omitempty"`
	Team   string             `bson:"team"`
	Type   int64              `bson:"type"`
	Source string             `bson:"source"`
	Key    string             `bson:"key"`
}

// Event information
type Event struct {
	Id            primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name          string             `json:"name" bson:"name"`
	Key           string             `json:"key" bson:"key"`
	Start         time.Time          `json:"start" bson:"start"`
	End           time.Time          `json:"end" bson:"end"`
	Year          int                `json:"year" bson:"year"`
	City          string             `json:"city" bson:"city"`
	Country       string             `json:"country" bson:"country"`
	StateProvince string             `json:"state_province" bson:"state_province"`
	TeamIds       []string           `json:"-" bson:"teams"`
	MatchIds      []string           `json:"-" bson:"matches"`
	Rankings      []EventRanking     `json:"-" bson:"rankings,omitempty"`
	Alliances     []Alliance         `json:"-" bson:"alliances,omitempty"`
}

// Match information
type Match struct {
	Id             primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	EventId        string             `json:"event" bson:"event"`
	Key            string             `json:"key" bson:"key"`
	ScheduledAt    time.Time          `json:"scheduled_at" bson:"scheduled_at"`
	HappenedAt     time.Time          `json:"happened_at" bson:"happened_at"`
	MatchType      int                `json:"match_type" bson:"match_type"`
	MatchSetNumber int                `json:"match_set_number" bson:"match_set_number"`
	MatchNumber    int                `json:"match_number" bson:"match_number"`
	Winner         int                `json:"winner" bson:"winner"`
	RedAlliance    TeamAlliance       `json:"red_alliance" bson:"red_alliance"`
	BlueAlliance   TeamAlliance       `json:"blue_alliance" bson:"blue_alliance"`
	RedScore       int                `json:"red_score" bson:"red_score"`
	BlueScore      int                `json:"blue_score" bson:"blue_score"`
}

// Team ranking in event information
type EventRanking struct {
	EventId            string  `json:"event" bson:"event"`
	TeamId             string  `json:"team" bson:"team"`
	Rank               int     `json:"rank" bson:"rank"`
	MatchesPlayed      int     `json:"matches_played" bson:"matches_played"`
	Disqualifications  int     `json:"disqualifications" bson:"disqualifications"`
	TotalRankingPoints float64 `json:"total_ranking_points" bson:"total_ranking_points"`
	RankingScore       float64 `json:"ranking_score" bson:"ranking_score"`
}

// Team alliance information
type TeamAlliance struct {
	Team1          string           `json:"team_1" bson:"team_1"`
	Team2          string           `json:"team_2" bson:"team_2"`
	Team3          string           `json:"team_3" bson:"team_3"`
	ScoreBreakdown ScoringBreakdown `json:"score_breakdown" bson:"score_breakdown"`
}

// Alliance information
type Alliance struct {
	Name    string   `json:"name" bson:"name"`
	TeamIds []string `json:"teams" bson:"teams"`
	Status  string   `json:"status" bson:"status"`
	Wins    int      `json:"wins" bson:"wins"`
	Ties    int      `json:"ties" bson:"ties"`
	Losses  int      `json:"losses" bson:"losses"`
}

// Average data from scouting matches for a team over an event
type EventMatchAverage struct {
	Team               int      `json:"team" bson:"team"`
	Event              string   `json:"event" bson:"event"`
	PercentMalfunction float64  `json:"percent_malfunction" bson:"percent_malfunction"`
	PushELO            float64  `json:"push_elo" bson:"push_elo"`
	DataPoints         int      `json:"data_points" bson:"data_points"`
	Matches            []string `json:"matches" bson:"matches"`
	ScoutedBy          string   `json:"-" bson:"scouted_by"`

	// Average shot data
	AverageTotalShots float64 `json:"average_total_shots" bson:"average_total_shots"`
	AverageLowShots   float64 `json:"average_low_shots" bson:"average_low_shots"`
	AverageOuterShots float64 `json:"average_outer_shots" bson:"average_outer_shots"`
	AverageInnerShots float64 `json:"average_inner_shots" bson:"average_inner_shots"`

	// Average accuracy data
	AverageCumulativeAccuracy      float64 `json:"average_overall_accuracy" bson:"average_overall_accuracy"`
	AverageAutoAccuracy            float64 `json:"average_auto_accuracy" bson:"average_auto_accuracy"`
	AverageTeleopAccuracy          float64 `json:"average_teleop_accuracy" bson:"average_teleop_accuracy"`
	AverageLowAccuracyAuto         float64 `json:"average_low_accuracy_auto" bson:"average_low_accuracy_auto"`
	AverageLowAccuracyTeleop       float64 `json:"average_low_accuracy_teleop" bson:"average_low_accuracy_teleop"`
	AverageLowAccuracyCumulative   float64 `json:"average_low_accuracy_cumulative" bson:"average_low_accuracy_cumulative"`
	AverageOuterAccuracyAuto       float64 `json:"average_outer_accuracy_auto" bson:"average_outer_accuracy_auto"`
	AverageOuterAccuracyTeleop     float64 `json:"average_outer_accuracy_teleop" bson:"average_outer_accuracy_teleop"`
	AverageOuterAccuracyCumulative float64 `json:"average_outer_accuracy_cumulative" bson:"average_outer_accuracy_cumulative"`
	AverageInnerAccuracyAuto       float64 `json:"average_inner_accuracy_auto" bson:"average_inner_accuracy_auto"`
	AverageInnerAccuracyTeleop     float64 `json:"average_inner_accuracy_teleop" bson:"average_inner_accuracy_teleop"`
	AverageInnerAccuracyCumulative float64 `json:"average_inner_accuracy_cumulative" bson:"average_inner_accuracy_cumulative"`

	// Average cycle time data
	AverageLowCycleTimes   float64 `json:"average_low_cycle_times" bson:"average_low_cycle_times"`
	AverageOuterCycleTimes float64 `json:"average_outer_cycle_times" bson:"average_outer_cycle_times"`
	AverageInnerCycleTimes float64 `json:"average_inner_cycle_times" bson:"average_inner_cycle_times"`

	// Average collection data
	AverageCollectionAccuracy float64 `json:"average_collection_accuracy" bson:"average_collection_accuracy"`
	AverageCollected          float64 `json:"average_collected" bson:"average_collected"`

	// Average climbing data
	AverageClimbTime     float64 `json:"average_climb_time" bson:"average_climb_time"`
	AverageClimbAccuracy float64 `json:"average_climb_accuracy" bson:"average_climb_accuracy"`

	// Average control panel data
	AverageControlPanelStage2Accuracy float64 `json:"average_control_panel_stage_2_accuracy" bson:"average_control_panel_stage_2_accuracy"`
	AverageControlPanelStage3Accuracy float64 `json:"average_control_panel_stage_3_accuracy" bson:"average_control_panel_stage_3_accuracy"`

	// Average block data
	AverageBlockAccuracy float64 `json:"average_block_accuracy" bson:"average_block_accuracy"`
	AverageBlocks        float64 `json:"average_blocks" bson:"average_blocks"`
}

// Scouted data for a match for a team
// This changes year-to-year
type TeamMatchData struct {
	MatchId   string `json:"match" bson:"match"`
	TeamId    string `json:"team" bson:"team"`
	ScoutedBy string `json:"-" bson:"scouted_by"`

	// Shot data
	TotalShots         int            `json:"total_shots" bson:"total_shots"`
	LowShots           int            `json:"low_shots" bson:"low_shots"`
	OuterShots         int            `json:"outer_shots" bson:"outer_shots"`
	InnerShots         int            `json:"inner_shots" bson:"inner_shots"`
	TotalShotLocations map[string]int `json:"shot_locations" bson:"shot_locations"`
	LowShotLocations   map[string]int `json:"low_shot_locations" bson:"low_shot_locations"`
	OuterShotLocations map[string]int `json:"outer_shot_locations" bson:"outer_shot_locations"`
	InnerShotLocations map[string]int `json:"inner_shot_locations" bson:"inner_shot_locations"`

	// Shot accuracy data
	CumulativeAccuracy      float64 `json:"overall_accuracy" bson:"overall_accuracy"`
	AutoAccuracy            float64 `json:"auto_accuracy" bson:"auto_accuracy"`
	TeleopAccuracy          float64 `json:"teleop_accuracy" bson:"teleop_accuracy"`
	LowAccuracyAuto         float64 `json:"low_accuracy_auto" bson:"low_accuracy_auto"`
	LowAccuracyTeleop       float64 `json:"low_accuracy_teleop" bson:"low_accuracy_teleop"`
	LowAccuracyCumulative   float64 `json:"low_accuracy_cumulative" bson:"low_accuracy_cumulative"`
	OuterAccuracyAuto       float64 `json:"outer_accuracy_auto" bson:"outer_accuracy_auto"`
	OuterAccuracyTeleop     float64 `json:"outer_accuracy_teleop" bson:"outer_accuracy_teleop"`
	OuterAccuracyCumulative float64 `json:"outer_accuracy_cumulative" bson:"outer_accuracy_cumulative"`
	InnerAccuracyAuto       float64 `json:"inner_accuracy_auto" bson:"inner_accuracy_auto"`
	InnerAccuracyTeleop     float64 `json:"inner_accuracy_teleop" bson:"inner_accuracy_teleop"`
	InnerAccuracyCumulative float64 `json:"inner_accuracy_cumulative" bson:"inner_accuracy_cumulative"`

	// Cycle time data
	LowCycleTimes          []float64 `json:"low_cycle_times" bson:"low_cycle_times"`
	LowCycleTimesAverage   float64   `json:"low_cycle_times_average" bson:"low_cycle_times_average"`
	OuterCycleTimes        []float64 `json:"outer_cycle_times" bson:"outer_cycle_times"`
	OuterCycleTimesAverage float64   `json:"outer_cycle_times_average" bson:"outer_cycle_times_average"`
	InnerCycleTimes        []float64 `json:"inner_cycle_times" bson:"inner_cycle_times"`
	InnerCycleTimesAverage float64   `json:"inner_cycle_times_average" bson:"inner_cycle_times_average"`

	// Control panel data
	ControlPanelStage2Accuracy   float64 `json:"control_panel_stage_2_accuracy" bson:"control_panel_stage_2_accuracy"`
	ControlPanelStage2Duration   float64 `json:"control_panel_stage_2_duration" bson:"control_panel_stage_2_duration"`
	ControlPanelStage2Successful bool    `json:"control_panel_stage_2_successful" bson:"control_panel_stage_2_successful"`
	ControlPanelStage3Accuracy   float64 `json:"control_panel_stage_3_accuracy" bson:"control_panel_stage_3_accuracy"`
	ControlPanelStage3Duration   float64 `json:"control_panel_stage_3_duration" bson:"control_panel_stage_3_duration"`
	ControlPanelStage3Successful bool    `json:"control_panel_stage_3_successful" bson:"control_panel_stage_3_successful"`

	// Blocking data
	TotalBlocks   int     `json:"total_blocks" bson:"totalBlocks"`
	BlockAccuracy float64 `json:"block_accuracy" bson:"block_accuracy"`

	// Malfunction data
	TotalMalfunctions    int                  `json:"total_malfunctions" bson:"total_malfunctions"`
	TotalMalfunctionTime float64              `json:"malfunction_time" bson:"malfunction_time"`
	Malfunctions         map[string][]float64 `json:"malfunctions"`

	// Climb data
	ClimbTime       float64 `json:"climb_time" bson:"climb_time"`
	ClimbAccuracy   float64 `json:"climb_accuracy" bson:"climb_accuracy"`
	ClimbSuccessful bool    `json:"climb_successful" bson:"climb_successful"`

	// Power cell collection data
	TotalCollected      int            `json:"total_collected" bson:"total_collected"`
	CollectionAccuracy  float64        `json:"collection_accuracy" bson:"collection_accuracy"`
	CollectionLocations map[string]int `json:"collection_locations" bson:"collection_locations"`

	// Push data
	TotalPushMatches int     `json:"total_push_matches" bson:"total_push_matches"`
	TotalPushWins    int     `json:"total_push_wins" bson:"total_push_wins"`
	TotalPushTime    float64 `json:"total_push_time" bson:"total_push_time"`
}

// Data about all teams in a given match
type ScoutedMatch struct {
	MatchId   string `bson:"match"`
	ScoutedBy string `bson:"scouted_by"`
	Red1      string `bson:"red_1"`
	Red2      string `bson:"red_2"`
	Red3      string `bson:"red_3"`
	Blue1     string `bson:"blue_1"`
	Blue2     string `bson:"blue_2"`
	Blue3     string `bson:"blue_3"`
	Winner    int    `bson:"winner"`
	Computed  bool   `bson:"completed"`
}

// Store TrueSkill data
type TrueSkill struct {
	Mu    float64 `json:"mu" bson:"mu"`
	Sigma float64 `json:"sigma" bson:"sigma"`
}

func (t TrueSkill) ToGrpc() *trueskill.Team {
	return &trueskill.Team{
		Sigma: t.Sigma,
		Mu:    t.Mu,
	}
}
