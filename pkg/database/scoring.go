package database

// ------------------------
//  2020 Infinite Recharge
// ------------------------

// Initiation line exit status
const (
	InitiationLineUnknown = iota
	InitiationLineNone
	InitiationLineExited
)

// Endgame/shield generate status
const (
	EndgameUnknown = iota
	EndgameNone
	EndgamePark
	EndgameHang
)

// Endgame level status
const (
	EndgameLevelUnknown = iota
	EndgameLevelNotLevel
	EndgameLevelIsLevel
)

// Scoring data breakdown
type ScoringBreakdown struct {
	// Status of all three robots
	InitLineRobot []int `json:"init_line_robot" bson:"init_line_robot"`
	EndgameRobot  []int `json:"endgame_robot" bson:"endgame_robot"`

	// Autonomous cell counts
	AutoCellsBottom int `json:"auto_cells_bottom" bson:"auto_cells_bottom"`
	AutoCellsOuter  int `json:"auto_cells_outer" bson:"auto_cells_outer"`
	AutoCellsInner  int `json:"auto_cells_inner" bson:"auto_cells_inner"`

	// Teleop cell counts
	TeleopCellsBottom int `json:"teleop_cells_bottom" bson:"teleop_cells_bottom"`
	TeleopCellsOuter  int `json:"teleop_cells_outer" bson:"teleop_cells_outer"`
	TeleopCellsInner  int `json:"teleop_cells_inner" bson:"teleop_cells_inner"`

	// Stage statuses
	Stage1Activated bool `json:"stage_1" bson:"stage_1"`
	Stage2Activated bool `json:"stage_2" bson:"stage_2"`
	Stage3Activated bool `json:"stage_3" bson:"stage_3"`

	// Whether the rung is level
	EndgameRungLevel bool `json:"endgame_rung_level" bson:"endgame_rung_level"`

	// Points from auto by section
	AutoInitLinePoints int `json:"auto_init_line_points" bson:"auto_init_line_points"`
	AutoCellPoints     int `json:"auto_cell_points" bson:"auto_cell_points"`

	// Points from teleop by section
	TeleopCellPoints   int `json:"teleop_cell_points" bson:"teleop_cell_points"`
	ControlPanelPoints int `json:"control_panel_points" bson:"control_panel_points"`

	// Points breakdown
	AutoPoints    int `json:"auto_points" bson:"auto_points"`
	TeleopPoints  int `json:"teleop_points" bson:"teleop_points"`
	EndgamePoints int `json:"endgame_points" bson:"endgame_points"`
	AdjustPoints  int `json:"adjust_points" bson:"adjust_points"`
	FoulPoints    int `json:"foul_points" bson:"foul_points"`
	TotalPoints   int `json:"total_points" bson:"total_points"`

	// Ranking points breakdown
	ShieldOperationalRankingPoint bool `json:"shield_operational_ranking_point" bson:"shield_operational_ranking_point"`
	ShieldEnergizedRankingPoint   bool `json:"shield_energized_ranking_point" bson:"shield_energized_ranking_point"`
	RankingPoints                 int  `json:"ranking_points" bson:"ranking_points"`

	// Number of fouls by type
	FoulCount     int `json:"foul_count" bson:"foul_count"`
	TechFoulCount int `json:"tech_foul_count" bson:"tech_foul_count"`
}
