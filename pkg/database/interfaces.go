package database

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var ErrSingleResult = errors.New("result is not a single result")

// MongoDB database interface
type Database interface {
	Collection(name string) Collection
	Drop() error
}

// MongoDB collection interface
type Collection interface {
	InsertOne(document interface{}) (insertedId interface{}, err error)
	InsertMany(documents []interface{}) (insertedIds []interface{}, err error)
	FindOne(filter interface{}) (result FindResult)
	FindMany(filter interface{}) (result FindResult)
	Update(filter interface{}, document interface{}, upsert, replace bool) (err error)
	DeleteOne(filter interface{}) (err error)
	DeleteMany(filter interface{}) (err error)
}

// Result type for both find one and find many
type FindResult interface {
	Decode(v interface{}) error
	All(results interface{}) error
	Next() bool
	Err() error
}

// Database implementation for MongoDB
type MongoDatabase struct {
	database *mongo.Database
}

// Get the a collection with the specified name
func (md *MongoDatabase) Collection(name string) Collection {
	return &MongoCollection{collection: md.database.Collection(name)}
}

// Drop the database
func (md *MongoDatabase) Drop() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	return md.database.Drop(ctx)
}

// Collection implementation for MongoDB
type MongoCollection struct {
	collection *mongo.Collection
}

// Insert a document to the database
func (mc *MongoCollection) InsertOne(document interface{}) (insertedId interface{}, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	result, err := mc.collection.InsertOne(ctx, document)
	if err != nil {
		return nil, err
	}
	return result.InsertedID, nil
}

// Insert a set of documents to the database
func (mc *MongoCollection) InsertMany(documents []interface{}) (insertedIds []interface{}, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	result, err := mc.collection.InsertMany(ctx, documents)
	if err != nil {
		return nil, err
	}
	return result.InsertedIDs, nil
}

// Find a single document by a given filter in the database
func (mc *MongoCollection) FindOne(filter interface{}) (result FindResult) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	document := mc.collection.FindOne(ctx, filter)
	return &MongoFindResult{singleResult: document, err: document.Err()}
}

// Find a set of documents by a given filter in the database
func (mc *MongoCollection) FindMany(filter interface{}) (result FindResult) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	cursor, err := mc.collection.Find(ctx, filter)
	return &MongoFindResult{cursor: cursor, err: err}
}

// Update documents matching a given filter with the provided filters.
// An upsert can also be run to create the document if it does not exist.
// The document can be replaced rather than updated if so desired.
func (mc *MongoCollection) Update(filter interface{}, document interface{}, upsert, replace bool) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	if replace {
		_, err = mc.collection.ReplaceOne(ctx, filter, document, &options.ReplaceOptions{Upsert: &upsert})
	} else {
		_, err = mc.collection.UpdateOne(ctx, filter, document, &options.UpdateOptions{Upsert: &upsert})
	}
	return
}

// Delete a document matching the given filter from the database.
func (mc *MongoCollection) DeleteOne(filter interface{}) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	_, err = mc.collection.DeleteOne(ctx, filter)
	return
}

// Delete a set of documents matching the given filter from the database
func (mc *MongoCollection) DeleteMany(filter interface{}) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	_, err = mc.collection.DeleteMany(ctx, filter)
	return
}

// Find result implementation for MongoDB
type MongoFindResult struct {
	singleResult *mongo.SingleResult
	cursor       *mongo.Cursor
	err          error
}

// Decode a found result into a struct
func (fr *MongoFindResult) Decode(v interface{}) error {
	if fr.err != nil {
		return fr.err
	} else if fr.singleResult != nil {
		return fr.singleResult.Decode(v)
	}
	return fr.cursor.Decode(v)
}

// Decode all the results into an array of structs
func (fr *MongoFindResult) All(results interface{}) error {
	if fr.err != nil {
		return fr.err
	} else if fr.cursor == nil {
		return ErrSingleResult
	}
	return fr.cursor.All(context.TODO(), results)
}

// Returns true if there is another document in the result
func (fr *MongoFindResult) Next() bool {
	if fr.cursor == nil {
		return false
	}
	return fr.cursor.Next(context.TODO())
}

// Returns the error from the query
func (fr *MongoFindResult) Err() error {
	return fr.err
}
