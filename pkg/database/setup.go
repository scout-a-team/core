package database

import (
	"context"
	"gitlab.com/scout-a-team/core/pkg/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.uber.org/zap"
	"time"
)

// Create a connection to a MongoDB database
func Connect(cfg config.Config) Database {
	// Create the logger
	logger := zap.L().Named("database")

	// Setup connection
	clientOpts := options.Client().ApplyURI(cfg.Database.ConnectionString)
	client, err := mongo.NewClient(clientOpts)
	if err != nil {
		logger.Fatal("failed to create mongodb client", zap.Error(err))
	}
	logger.Debug("created database client with specified configuration")

	// Initiate connection
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		logger.Fatal("failed to connect to mongodb", zap.Error(err))
	}
	logger.Debug("initialized connection to database")

	// Ensure connection is working
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		logger.Fatal("failed to ping mongodb server", zap.Error(err))
	}
	logger.Debug("ensured database server was found")

	return &MongoDatabase{database: client.Database(cfg.Database.Database)}
}
