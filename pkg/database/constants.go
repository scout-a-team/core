package database

const (
	TokenAuthentication = iota
	TokenEmailVerification
	TokenPasswordReset
	Token2faLogin
)

const (
	WinAllianceUnknown = iota
	WinAllianceRed
	WinAllianceBlue
)

const (
	MatchQualification = iota
	MatchQuarterFinal
	MatchSemiFinal
	MatchFinal
)
