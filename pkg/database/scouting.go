package database

// Pit scout data
type PitScout struct {
	Weight               float64 `json:"weight" bson:"weight"`
	Width                float64 `json:"width" bson:"width"`
	Length               float64 `json:"length" bson:"length"`
	DriveTrainMotorCount int64   `json:"drive_train_motor_count" bson:"dt_motor_count"`

	// Options: Mini CIMs, CIMs, Neos, Other (written out)
	DriveTrainMotorType string `json:"drive_train_motor_type" bson:"dt_motor_type"`

	// Options: Tank, Swerve, Mecanum, Tank Treads, Other (written out)
	DriveTrainType string `json:"drive_train_type" bson:"dt_type"`

	// Options: Traction, Colson, Pneumatics, Omni, Other (written out)
	WheelType string `json:"wheel_type" bson:"wheel_type"`

	// Options: 4, 6, 8, Other (written out) in inches
	WheelDiameter int64 `json:"wheel_diameter" bson:"wheel_diameter"`

	// Options: Java, C++, LabView, Other (written out)
	ProgrammingLanguage string       `json:"programming_language" bson:"programming_language"`
	HasCamera           bool         `json:"camera" bson:"camera"`
	Notes               string       `json:"notes" bson:"notes"`
	GameSpecific        GameSpecific `json:"game_specific" bson:"game_specific"`
	Completed           bool         `json:"-" bson:"completed"`
}

// Pit scout data that changes by year goes into the GameSpecific field
type GameSpecific struct {
	ClimbingMethod string `json:"climbing_method" bson:"climbing_method"`
	MoveOnBar      bool   `json:"move_on_bar" bson:"move_on_bar"`
	CarryRobot     bool   `json:"carry_robot" bson:"carry_robot"`
	LowGoal        bool   `json:"low_goal" bson:"low_goal"`
	OuterGoal      bool   `json:"outer_goal" bson:"outer_goal"`
	InnerGoal      bool   `json:"inner_goal"`
}
