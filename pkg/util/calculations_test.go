package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAddToAverage(t *testing.T) {
	nums := []float64{67, 77, 49, 26, 41, 47, 54, 10, 33, 53}

	// Calculate initial average
	sum := 0.0
	for _, v := range nums {
		sum += v
	}
	initialAverage := sum / float64(len(nums))

	// Calculate actual average
	sum += 32
	average := sum / float64(len(nums)+1)

	// Calculate with initial average
	gotAverage := AddToAverage(initialAverage, 32, len(nums)+1)

	// Reduce precision for comparison
	average = float64(int(average*1000)) / 1000
	gotAverage = float64(int(gotAverage*1000)) / 1000

	assert.Equal(t, gotAverage, average)
}
