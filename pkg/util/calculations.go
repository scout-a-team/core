package util

// Add a number to an average
func AddToAverage(old, value float64, size int) float64 {
	return old + ((value - old) / float64(size))
}
