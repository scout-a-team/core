package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBoolToInt(t *testing.T) {
	assert.Equal(t, BoolToInt(true), int64(1))
	assert.Equal(t, BoolToInt(false), int64(0))
}
