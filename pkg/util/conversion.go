package util

// Convert a boolean to an integer
func BoolToInt(b bool) int64 {
	if b {
		return 1
	}
	return 0
}
