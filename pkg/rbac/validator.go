package rbac

// Ensure user has correct permissions
func Enforce(key Key, resource, method string) bool {
	// Get role from map
	role, ok := Roles[key]
	if !ok {
		return false
	}

	// Select permission object
	var permission Permission
	switch resource {
	case ResourceTeams:
		permission = role.Teams
	case ResourceUsers:
		permission = role.Users
	case ResourcePitScout:
		permission = role.PitScout
	case ResourceMatchScout:
		permission = role.MatchScout
	case ResourceStatistics:
		permission = role.Statistics
	default:
		return false
	}

	// Return valid method
	switch method {
	case MethodCreate:
		return permission.Create
	case MethodDescribe:
		return permission.Describe
	case MethodUpdate:
		return permission.Update
	case MethodDelete:
		return permission.Delete
	case MethodList:
		return permission.List
	case MethodAssign:
		return permission.Assign
	default:
		return false
	}
}
