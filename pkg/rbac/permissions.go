package rbac

// Resource key definitions
const (
	ResourceTeams      = "teams"
	ResourceUsers      = "users"
	ResourceMatchScout = "match_scout"
	ResourcePitScout   = "pit_scout"
	ResourceStatistics = "statistics"
)

// Method key definitions
const (
	MethodCreate   = "create"
	MethodDescribe = "describe"
	MethodUpdate   = "update"
	MethodDelete   = "delete"
	MethodList     = "list"
	MethodAssign   = "assign"
)

// Permission definition
type Permission struct {
	Create   bool
	Describe bool
	Update   bool
	Delete   bool
	List     bool
	Assign   bool
}

// Role definition
type Role struct {
	Teams      Permission
	Users      Permission
	MatchScout Permission
	PitScout   Permission
	Statistics Permission
}

type Key string

var (
	RoleOwner         Key = "owner"
	RoleAdministrator Key = "administrator"
	RoleMentor        Key = "mentor"
	RoleMember        Key = "member"
	RoleNone          Key = ""
)
