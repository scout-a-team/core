package rbac

// Role definitions
var Roles = map[Key]Role{
	RoleOwner: {
		Teams: Permission{
			Describe: true,
			Update:   true,
			Delete:   true,
			List:     true,
		},
		Users: Permission{
			Create:   true,
			Describe: true,
			Update:   true,
			Delete:   true,
			List:     true,
			Assign:   true,
		},
		MatchScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		PitScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		Statistics: Permission{
			Describe: true,
			List:     true,
		},
	},
	RoleAdministrator: {
		Teams: Permission{
			Describe: true,
			Update:   true,
			List:     true,
		},
		Users: Permission{
			Create:   true,
			Describe: true,
			Update:   true,
			Delete:   true,
			List:     true,
			Assign:   true,
		},
		MatchScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		PitScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		Statistics: Permission{
			Describe: true,
			List:     true,
		},
	},
	RoleMentor: {
		Teams: Permission{
			Describe: true,
			List:     true,
		},
		Users: Permission{
			Describe: true,
			List:     true,
		},
		MatchScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		PitScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		Statistics: Permission{
			Describe: true,
			List:     true,
		},
	},
	RoleMember: {
		Teams: Permission{
			Describe: true,
			List:     true,
		},
		MatchScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		PitScout: Permission{
			Create:   true,
			Describe: true,
			List:     true,
		},
		Statistics: Permission{
			Describe: true,
			List:     true,
		},
	},
}
