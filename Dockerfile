# syntax = docker/dockerfile:1.1-experimental
FROM golang:1.14-alpine AS build

RUN apk update && apk add --no-cache git

# Go modules configuration
RUN go env -w GO111MODULE=on GOPRIVATE=gitlab.com/scout-a-team/*
RUN --mount=type=secret,id=gitlab,target=/gitlab,required=true \
    git config --global url."https://$(cat /gitlab)@gitlab.com".insteadOf "https://gitlab.com"

# Retrieve required dependencies
WORKDIR $GOPATH/src/gitlab.com/scout-a-team/core
RUN git clone https://gitlab.com/scout-a-team/trueskill.git/ $GOPATH/src/gitlab.com/scout-a-team/trueskill

# Build server
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-w -s -extldflags '-static'" -o /go/bin/core

FROM alpine:3

# Setup server
COPY --from=build /go/bin/core /
ENTRYPOINT ["/core"]
