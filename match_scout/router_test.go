package match_scout

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"testing"
)

type route struct {
	route     string
	methods   []string
	subroutes []route
}

func TestRoute(t *testing.T) {
	r := chi.NewRouter()

	// Initialize routes
	Route(r)

	expected := []route{
		{
			route: "/{event}/*",
			subroutes: []route{
				{
					route:   "/",
					methods: []string{"GET", "POST"},
				},
				{
					route:   "/stats",
					methods: []string{"GET"},
				},
			},
		},
	}

	// Check routes are correct
	for i, route := range r.Routes() {
		assert.Equal(t, expected[i].route, route.Pattern)

		if route.SubRoutes == nil {
			for _, method := range expected[i].methods {
				assert.Contains(t, route.Handlers, method)
			}
		} else {
			for j, subroute := range route.SubRoutes.Routes() {
				assert.Equal(t, expected[i].subroutes[j].route, subroute.Pattern)
				for _, method := range expected[i].subroutes[j].methods {
					assert.Contains(t, subroute.Handlers, method)
				}
			}
		}
	}
}
