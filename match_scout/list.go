package match_scout

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func list(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("scouting.match.list").With(zap.String("method", "GET"), zap.String("path", "/scouting/match/{event}"),
		zap.String("event", eventKey), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceMatchScout, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Find all matches for team
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for matches in event")
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	responses.SuccessWithData(w, event.MatchIds)
	logger.Info("got list of matches for event")
}
