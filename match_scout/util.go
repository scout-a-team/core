package match_scout

import "strconv"

func addCycleTime(cycleTimes []float64, index, previous int, orderedTimestamps []float64) []float64 {
	if index == 0 {
		// Append current timestamp
		// Equivalent to (current timestamp) - 0
		cycleTimes = append(cycleTimes, orderedTimestamps[index])
	} else {
		// Append difference of timestamps
		cycleTimes = append(cycleTimes, orderedTimestamps[index]-orderedTimestamps[previous])
	}

	return cycleTimes
}

func cycleTimeAverage(cycleTimes []float64) float64 {
	if cycleTimes == nil {
		return 0
	}

	var sum float64
	for _, ct := range cycleTimes {
		sum += ct
	}
	return sum / float64(len(cycleTimes))
}

func stringToFloatKeys(in map[string]eventData) (map[float64]eventData, error) {
	out := make(map[float64]eventData)

	for k, v := range in {
		parsed, err := strconv.ParseFloat(k, 64)
		if err != nil {
			return nil, err
		}

		out[parsed] = v
	}

	return out, nil
}

func intToStringKeysForIntValues(in map[int]int) map[string]int {
	out := make(map[string]int)

	for k, v := range in {
		out[strconv.Itoa(k)] = v
	}

	return out
}

func intToStringKeysForFloatArrayValues(in map[int][]float64) map[string][]float64 {
	out := make(map[string][]float64)

	for k, v := range in {
		out[strconv.Itoa(k)] = v
	}

	return out
}

func zipMalfunctionsToMap(types []int, durations []float64) map[int][]float64 {
	out := make(map[int][]float64)

	for i, type_ := range types {
		if _, ok := out[type_]; !ok {
			out[type_] = []float64{durations[i]}
		} else {
			out[type_] = append(out[type_], durations[i])
		}
	}

	return out
}

func locationFrequency(locations ...[]int) map[int]int {
	frequencies := make(map[int]int)

	for _, set := range locations {
		for _, location := range set {
			frequencies[location]++
		}
	}

	return frequencies
}

func calculateAccuracy(shotsMade, shotsTaken int) float64 {
	if shotsTaken == 0 && shotsMade == 0 {
		return 0
	}
	return float64(shotsMade) / float64(shotsTaken)
}
