package match_scout

import (
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/request_context"
)

// Add routes to main router
func Route(r chi.Router) {
	r.Route("/{event}", func(r chi.Router) {
		r.Use(request_context.EventKeyCtx)
		r.Get("/", list)
		r.Post("/", create)
		r.Get("/stats", stats)
	})
}
