package match_scout

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAddCycleTime(t *testing.T) {
	var cycles []float64
	index := 0
	timestamps := []float64{0.5}

	assert.Equal(t, []float64{0.5}, addCycleTime(cycles, index, 0, timestamps))
}

func TestAddCycleTime_NonZeroCycle(t *testing.T) {
	var cycles []float64
	index := 1
	timestamps := []float64{0.5, 2.4}

	assert.Equal(t, []float64{1.9}, addCycleTime(cycles, index, 0, timestamps))
}

func TestCycleTimeAverage(t *testing.T) {
	times := []float64{0.6, 1.6, 3.6, 2.4}
	assert.Equal(t, 2.05, float64(int(cycleTimeAverage(times)*100))/100)
}

func TestStringToFloatKeys(t *testing.T) {
	unprocessed := map[string]eventData{"1.2": {}, "2.2": {}, "0.2": {}}
	processed, err := stringToFloatKeys(unprocessed)

	assert.NoError(t, err)
	assert.Equal(t, map[float64]eventData{0.2: {Type: 0, ShotsMade: 0, ShotsTaken: 0, ShotsScoreLocation: 0, ShotsFrom: 0, CollectedSuccess: false, CollectedFrom: 0, CPStart: 0, CPEnd: 0, CPAttempts: 0, CPCompleted: false, BlocksSuccessful: 0, BlocksAttempted: 0, MalfunctionStart: 0, MalfunctionEnd: 0, MalfunctionType: 0, PushStart: 0, PushEnd: 0, PushAgainst: 0, PushWon: false, ClimbStart: 0, ClimbEnd: 0, ClimbAttempts: 0, ClimbSuccessful: false}, 1.2: {Type: 0, ShotsMade: 0, ShotsTaken: 0, ShotsScoreLocation: 0, ShotsFrom: 0, CollectedSuccess: false, CollectedFrom: 0, CPStart: 0, CPEnd: 0, CPAttempts: 0, CPCompleted: false, BlocksSuccessful: 0, BlocksAttempted: 0, MalfunctionStart: 0, MalfunctionEnd: 0, MalfunctionType: 0, PushStart: 0, PushEnd: 0, PushAgainst: 0, PushWon: false, ClimbStart: 0, ClimbEnd: 0, ClimbAttempts: 0, ClimbSuccessful: false}, 2.2: {Type: 0, ShotsMade: 0, ShotsTaken: 0, ShotsScoreLocation: 0, ShotsFrom: 0, CollectedSuccess: false, CollectedFrom: 0, CPStart: 0, CPEnd: 0, CPAttempts: 0, CPCompleted: false, BlocksSuccessful: 0, BlocksAttempted: 0, MalfunctionStart: 0, MalfunctionEnd: 0, MalfunctionType: 0, PushStart: 0, PushEnd: 0, PushAgainst: 0, PushWon: false, ClimbStart: 0, ClimbEnd: 0, ClimbAttempts: 0, ClimbSuccessful: false}}, processed)
}

func TestStringToFloatKeys_InvalidKey(t *testing.T) {
	unprocessed := map[string]eventData{"a": {}}
	processed, err := stringToFloatKeys(unprocessed)

	assert.Error(t, err)
	assert.Nil(t, processed)
}
