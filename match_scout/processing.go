package match_scout

import (
	"errors"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"sort"
	"strconv"
	"strings"
)

// Event types
const (
	Shot = iota
	Collected
	CPStage2
	CPStage3
	Block
	Malfunction
	Push
	Climb
)

// Shot to locations
const (
	ShotLocationLow = iota
	ShotLocationOuter
	ShotLocationInner
)

// Power cell collection locations
const (
	CollectedFromFriendlyLoading = iota
	CollectedFromOpponentLoading
	CollectedFromFriendlyTrench
	CollectedFromOpponentTrench
	CollectedFromFriendlyBoundaries
	CollectedFromOpponentBoundaries
	CollectedFromFriendlySector
	CollectedFromOpponentSector
	CollectedFromOther
)

// Malfunction types
const (
	MalfunctionTypeBreak = iota
	MalfunctionTypeBrownout
	MalfunctionTypeDisabled
	MalfunctionTypeOther
)

// Process data for the game
func processData(match string, scoutedBy string, team int, events map[float64]eventData, db database.Database) error {
	// Get averages for event to update
	var teamData database.EventMatchAverage
	if err := db.Collection("team_match_average").FindOne(bson.M{"team": team, "scouted_by": scoutedBy}).Decode(&teamData); err != mongo.ErrNoDocuments && err != nil {
		return err
	}

	// Generic data stuff
	teamData.Team = team
	teamData.Matches = append(teamData.Matches, match)
	teamData.Event = strings.Split(match, "_")[0]
	teamData.ScoutedBy = scoutedBy

	// Increment data points for averages/percentages
	teamData.DataPoints++

	// Get ordered array of timestamps
	var ordered []float64
	for ts := range events {
		ordered = append(ordered, ts)
	}
	sort.Float64s(ordered)

	var (
		// Shot data
		lowShotsTakenAuto     = 0
		lowShotsMadeAuto      = 0
		lowShotsTakenTeleop   = 0
		lowShotsMadeTeleop    = 0
		outerShotsTakenAuto   = 0
		outerShotsMadeAuto    = 0
		outerShotsTakenTeleop = 0
		outerShotsMadeTeleop  = 0
		innerShotsTakenAuto   = 0
		innerShotsMadeAuto    = 0
		innerShotsTakenTeleop = 0
		innerShotsMadeTeleop  = 0
		lowShotLocations      []int
		outerShotLocations    []int
		innerShotLocations    []int

		// Collected power cell data
		collectionsAttempted = 0
		collectionsSuccesses = 0
		collectionLocations  []int

		// Control panel data
		cpS2Duration  float64 = 0
		cpS2Attempts          = 0
		cpS2Completed         = false
		cpS3Duration  float64 = 0
		cpS3Attempts          = 0
		cpS3Completed         = false

		// Power cell shot blocking data
		blocksAttempted = 0
		blocksSucceeded = 0

		// Malfunctions data
		malfunctionDurations  []float64
		malfunctionTypes      []int
		malfunctionsTotal             = 0
		malfunctionsTotalTime float64 = 0

		// Push data
		pushTotal                 = 0
		pushTotalDuration float64 = 0
		pushDurations     []float64
		pushAgainst       []int
		pushWon           []bool
		pushWins          = 0

		// Climb data
		climbDuration   float64 = 0
		climbAttempts           = 0
		climbSuccessful         = false

		// Cycle times data
		allScoreCycleTimes []float64
		lowCycleTimes      []float64
		outerCycleTimes    []float64
		innerCycleTimes    []float64

		// Last index when an event happened
		lastScoreIndex      = 0
		lastLowScoreIndex   = 0
		lastOuterScoreIndex = 0
		lastInnerScoreIndex = 0
	)

	// Extract data from sequential events
	for i, ts := range ordered {
		event := events[ts]

		switch event.Type {
		case Shot:
			allScoreCycleTimes = addCycleTime(allScoreCycleTimes, i, lastScoreIndex, ordered)
			lastScoreIndex = i
			switch event.ShotsScoreLocation {
			case ShotLocationLow:
				lowShotLocations = append(lowShotLocations, event.ShotsFrom)
				lowCycleTimes = addCycleTime(lowCycleTimes, i, lastLowScoreIndex, ordered)
				lastLowScoreIndex = i
				if ts < 17.5 {
					lowShotsMadeAuto += event.ShotsMade
					lowShotsTakenAuto += event.ShotsTaken
				} else {
					lowShotsMadeTeleop += event.ShotsMade
					lowShotsTakenTeleop += event.ShotsTaken
				}

			case ShotLocationOuter:
				outerShotLocations = append(outerShotLocations, event.ShotsFrom)
				outerCycleTimes = addCycleTime(outerCycleTimes, i, lastOuterScoreIndex, ordered)
				lastOuterScoreIndex = i
				if ts < 17.5 {
					outerShotsMadeAuto += event.ShotsMade
					outerShotsTakenAuto += event.ShotsTaken
				} else {
					outerShotsMadeTeleop += event.ShotsMade
					outerShotsTakenTeleop += event.ShotsTaken
				}

			case ShotLocationInner:
				innerShotLocations = append(innerShotLocations, event.ShotsFrom)
				innerCycleTimes = addCycleTime(innerCycleTimes, i, lastInnerScoreIndex, ordered)
				lastInnerScoreIndex = i
				if ts < 17.5 {
					innerShotsMadeAuto += event.ShotsMade
					innerShotsTakenAuto += event.ShotsTaken
				} else {
					innerShotsMadeTeleop += event.ShotsMade
					innerShotsTakenTeleop += event.ShotsTaken
				}

			default:
				return errors.New("invalid shot location")
			}

		case Collected:
			collectionLocations = append(collectionLocations, event.CollectedFrom)
			collectionsAttempted++
			if event.CollectedSuccess {
				collectionsSuccesses++
			}

		case CPStage2:
			lastScoreIndex = i
			lastLowScoreIndex = i
			lastOuterScoreIndex = i
			lastInnerScoreIndex = i
			cpS2Duration += event.CPEnd - event.CPStart
			cpS2Attempts += event.CPAttempts
			cpS2Completed = event.CPCompleted

		case CPStage3:
			lastScoreIndex = i
			lastLowScoreIndex = i
			lastOuterScoreIndex = i
			lastInnerScoreIndex = i
			cpS3Duration += event.CPEnd - event.CPStart
			cpS3Attempts += event.CPAttempts
			cpS3Completed = event.CPCompleted

		case Block:
			blocksAttempted += event.BlocksAttempted
			blocksSucceeded += event.BlocksSuccessful

		case Malfunction:
			malfunctionsTotal++
			malfunctionsTotalTime += event.MalfunctionEnd - event.MalfunctionStart
			malfunctionDurations = append(malfunctionDurations, event.MalfunctionEnd-event.MalfunctionStart)
			malfunctionTypes = append(malfunctionTypes, event.MalfunctionType)

		case Push:
			pushTotal++
			pushTotalDuration += event.PushEnd - event.PushStart
			pushDurations = append(pushDurations, event.PushEnd-event.PushStart)
			pushAgainst = append(pushAgainst, event.PushAgainst)
			pushWon = append(pushWon, event.PushWon)
			if event.PushWon {
				pushWins++
			}

		case Climb:
			climbDuration += event.ClimbEnd - event.ClimbStart
			climbAttempts += event.ClimbAttempts
			climbSuccessful = event.ClimbSuccessful

		default:
			return errors.New("invalid event type")
		}
	}

	// Create data for a match
	matchData := database.TeamMatchData{
		MatchId:   match,
		TeamId:    "frc" + strconv.Itoa(team),
		ScoutedBy: scoutedBy,

		// Shot location data
		TotalShotLocations: intToStringKeysForIntValues(locationFrequency(lowShotLocations, outerShotLocations, innerShotLocations)),
		LowShotLocations:   intToStringKeysForIntValues(locationFrequency(lowShotLocations)),
		OuterShotLocations: intToStringKeysForIntValues(locationFrequency(outerShotLocations)),
		InnerShotLocations: intToStringKeysForIntValues(locationFrequency(innerShotLocations)),

		// Cycle times data
		LowCycleTimes:          lowCycleTimes,
		LowCycleTimesAverage:   cycleTimeAverage(lowCycleTimes),
		OuterCycleTimes:        outerCycleTimes,
		OuterCycleTimesAverage: cycleTimeAverage(outerCycleTimes),
		InnerCycleTimes:        innerCycleTimes,
		InnerCycleTimesAverage: cycleTimeAverage(innerCycleTimes),

		// Control panel data
		ControlPanelStage2Accuracy:   1 / float64(cpS2Attempts),
		ControlPanelStage2Duration:   cpS2Duration,
		ControlPanelStage2Successful: cpS2Completed,
		ControlPanelStage3Accuracy:   1 / float64(cpS3Attempts),
		ControlPanelStage3Duration:   cpS3Duration,
		ControlPanelStage3Successful: cpS3Completed,

		// Blocking data
		TotalBlocks:   blocksAttempted,
		BlockAccuracy: float64(blocksSucceeded) / float64(blocksAttempted),

		// Malfunctions data
		TotalMalfunctions:    malfunctionsTotal,
		TotalMalfunctionTime: malfunctionsTotalTime,
		Malfunctions:         intToStringKeysForFloatArrayValues(zipMalfunctionsToMap(malfunctionTypes, malfunctionDurations)),

		// Climb data
		ClimbTime:       climbDuration,
		ClimbAccuracy:   1 / float64(climbAttempts),
		ClimbSuccessful: climbSuccessful,

		// Power cell collection data
		TotalCollected:      collectionsSuccesses,
		CollectionAccuracy:  float64(collectionsSuccesses) / float64(collectionsAttempted),
		CollectionLocations: intToStringKeysForIntValues(locationFrequency(collectionLocations)),

		// Push data
		TotalPushMatches: pushTotal,
		TotalPushTime:    pushTotalDuration,
		TotalPushWins:    pushWins,
	}

	// Add shot data
	matchData.LowShots = lowShotsTakenAuto + lowShotsTakenTeleop
	matchData.OuterShots = outerShotsTakenAuto + outerShotsTakenTeleop
	matchData.InnerShots = innerShotsTakenAuto + innerShotsTakenTeleop
	matchData.TotalShots = matchData.LowShots + matchData.OuterShots + matchData.InnerShots
	matchData.CumulativeAccuracy = calculateAccuracy(lowShotsMadeAuto+lowShotsMadeTeleop+outerShotsMadeAuto+outerShotsMadeTeleop+innerShotsMadeAuto+innerShotsMadeTeleop, matchData.TotalShots)
	matchData.AutoAccuracy = calculateAccuracy(lowShotsMadeAuto+outerShotsMadeAuto+innerShotsMadeAuto, lowShotsTakenAuto+outerShotsTakenAuto+innerShotsTakenAuto)
	matchData.TeleopAccuracy = calculateAccuracy(lowShotsMadeTeleop+outerShotsMadeTeleop+innerShotsMadeTeleop, lowShotsTakenTeleop+outerShotsTakenTeleop+innerShotsTakenTeleop)
	matchData.LowAccuracyCumulative = calculateAccuracy(lowShotsMadeAuto+lowShotsMadeTeleop, matchData.LowShots)
	matchData.LowAccuracyAuto = calculateAccuracy(lowShotsMadeAuto, lowShotsTakenAuto)
	matchData.LowAccuracyTeleop = calculateAccuracy(lowShotsMadeTeleop, lowShotsTakenTeleop)
	matchData.OuterAccuracyCumulative = calculateAccuracy(outerShotsMadeAuto+outerShotsMadeTeleop, matchData.OuterShots)
	matchData.OuterAccuracyAuto = calculateAccuracy(outerShotsMadeAuto, outerShotsTakenAuto)
	matchData.OuterAccuracyTeleop = calculateAccuracy(outerShotsMadeTeleop, outerShotsTakenTeleop)
	matchData.InnerAccuracyCumulative = calculateAccuracy(innerShotsMadeAuto+innerShotsMadeTeleop, matchData.InnerShots)
	matchData.InnerAccuracyAuto = calculateAccuracy(innerShotsMadeAuto, innerShotsTakenAuto)
	matchData.InnerAccuracyTeleop = calculateAccuracy(innerShotsMadeTeleop, innerShotsTakenTeleop)

	// Prevent NaNs
	if blocksAttempted == 0 && blocksSucceeded == 0 {
		matchData.BlockAccuracy = 0
	}
	if collectionsSuccesses == 0 && collectionsAttempted == 0 {
		matchData.CollectionAccuracy = 0
	}

	// Prevent +/- infinities
	if cpS2Attempts == 0 {
		matchData.ControlPanelStage2Accuracy = 0
	}
	if cpS3Attempts == 0 {
		matchData.ControlPanelStage3Accuracy = 0
	}
	if climbAttempts == 0 {
		matchData.ClimbAccuracy = 0
	}

	// Add average shot data
	teamData.AverageTotalShots = util.AddToAverage(teamData.AverageTotalShots, float64(matchData.TotalShots), teamData.DataPoints)
	teamData.AverageLowShots = util.AddToAverage(teamData.AverageLowShots, float64(matchData.LowShots), teamData.DataPoints)
	teamData.AverageOuterShots = util.AddToAverage(teamData.AverageOuterShots, float64(matchData.OuterShots), teamData.DataPoints)
	teamData.AverageInnerShots = util.AddToAverage(teamData.AverageInnerShots, float64(matchData.InnerShots), teamData.DataPoints)

	// Add average accuracy data
	teamData.AverageCumulativeAccuracy = util.AddToAverage(teamData.AverageCumulativeAccuracy, matchData.CumulativeAccuracy, teamData.DataPoints)
	teamData.AverageAutoAccuracy = util.AddToAverage(teamData.AverageAutoAccuracy, matchData.AutoAccuracy, teamData.DataPoints)
	teamData.AverageTeleopAccuracy = util.AddToAverage(teamData.AverageTeleopAccuracy, matchData.TeleopAccuracy, teamData.DataPoints)
	teamData.AverageLowAccuracyAuto = util.AddToAverage(teamData.AverageLowAccuracyAuto, matchData.LowAccuracyAuto, teamData.DataPoints)
	teamData.AverageLowAccuracyTeleop = util.AddToAverage(teamData.AverageLowAccuracyTeleop, matchData.LowAccuracyTeleop, teamData.DataPoints)
	teamData.AverageLowAccuracyCumulative = util.AddToAverage(teamData.AverageLowAccuracyCumulative, matchData.LowAccuracyCumulative, teamData.DataPoints)
	teamData.AverageOuterAccuracyAuto = util.AddToAverage(teamData.AverageOuterAccuracyAuto, matchData.OuterAccuracyAuto, teamData.DataPoints)
	teamData.AverageOuterAccuracyTeleop = util.AddToAverage(teamData.AverageOuterAccuracyTeleop, matchData.OuterAccuracyTeleop, teamData.DataPoints)
	teamData.AverageOuterAccuracyCumulative = util.AddToAverage(teamData.AverageOuterAccuracyCumulative, matchData.OuterAccuracyCumulative, teamData.DataPoints)
	teamData.AverageInnerAccuracyAuto = util.AddToAverage(teamData.AverageInnerAccuracyAuto, matchData.InnerAccuracyAuto, teamData.DataPoints)
	teamData.AverageInnerAccuracyTeleop = util.AddToAverage(teamData.AverageInnerAccuracyTeleop, matchData.InnerAccuracyTeleop, teamData.DataPoints)
	teamData.AverageInnerAccuracyCumulative = util.AddToAverage(teamData.AverageInnerAccuracyCumulative, matchData.InnerAccuracyCumulative, teamData.DataPoints)

	// Add average cycle time data
	teamData.AverageLowCycleTimes = util.AddToAverage(teamData.AverageLowCycleTimes, matchData.LowCycleTimesAverage, teamData.DataPoints)
	teamData.AverageOuterCycleTimes = util.AddToAverage(teamData.AverageOuterCycleTimes, matchData.OuterCycleTimesAverage, teamData.DataPoints)
	teamData.AverageInnerCycleTimes = util.AddToAverage(teamData.AverageInnerCycleTimes, matchData.InnerCycleTimesAverage, teamData.DataPoints)

	// Add average collection data
	teamData.AverageCollectionAccuracy = util.AddToAverage(teamData.AverageCollectionAccuracy, matchData.CollectionAccuracy, teamData.DataPoints)
	teamData.AverageCollected = util.AddToAverage(teamData.AverageCollected, float64(matchData.TotalCollected), teamData.DataPoints)

	// Add average climbing data
	teamData.AverageClimbTime = util.AddToAverage(teamData.AverageClimbTime, matchData.ClimbTime, teamData.DataPoints)
	teamData.AverageClimbAccuracy = util.AddToAverage(teamData.AverageClimbAccuracy, matchData.ClimbAccuracy, teamData.DataPoints)

	// Add average control panel data
	teamData.AverageControlPanelStage2Accuracy = util.AddToAverage(teamData.AverageControlPanelStage2Accuracy, matchData.ControlPanelStage2Accuracy, teamData.DataPoints)
	teamData.AverageControlPanelStage3Accuracy = util.AddToAverage(teamData.AverageControlPanelStage3Accuracy, matchData.ControlPanelStage3Accuracy, teamData.DataPoints)

	// Add average block data
	teamData.AverageBlockAccuracy = util.AddToAverage(teamData.AverageBlockAccuracy, matchData.BlockAccuracy, teamData.DataPoints)
	teamData.AverageBlocks = util.AddToAverage(teamData.AverageBlocks, float64(matchData.TotalBlocks), teamData.DataPoints)

	// Add new match data
	if _, err := db.Collection("team_match_data").InsertOne(matchData); err != nil {
		return err
	}

	// Update in database
	if err := db.Collection("team_match_average").Update(bson.M{"team": team}, &teamData, true, true); err != nil {
		return err
	}

	return nil
}
