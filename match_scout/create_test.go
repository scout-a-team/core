package match_scout

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          rbac.RoleOwner,
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestCreate(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	matchScoutResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchAverageCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	matchAverageCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("*database.EventMatchAverage"), true, true).Return(nil)
	matchScoutCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchScoutResult)
	matchScoutCollection.On("InsertOne", mock.AnythingOfType("database.TeamMatchData")).Return(primitive.NewObjectID(), nil)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
		event.Year = 2019
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.BlueAlliance.Team1 = "frc1"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})
	matchAverageResult.On("Decode", mock.AnythingOfType("*database.EventMatchAverage")).Return(nil)
	matchScoutResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestCreate_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"break": true,"disabled": true,"crossed_line": true,"events": {"7.2": 2,"14.8": 2,"20.9": 0,"28.1": 0,"44.7": 3,"52.2": 1,"62.1": 3,"69.9": 3,"72.1": 1,"91.5": 1},"data": {"climb_state": 1}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestCreate_FindEventDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"break": true,"disabled": true,"crossed_line": true,"events": {"7.2": 2,"14.8": 2,"20.9": 0,"28.1": 0,"44.7": 3,"52.2": 1,"62.1": 3,"69.9": 3,"72.1": 1,"91.5": 1},"data": {"climb_state": 1}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestCreate_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
		event.Year = 2019
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"break": true,"disabled": true,"crossed_line": true,"events": {"7.2": 2,"14.8": 2,"20.9": 0,"28.1": 0,"44.7": 3,"52.2": 1,"62.1": 3,"69.9": 3,"72.1": 1,"91.5": 1},"data": {"climb_state": 1}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestCreate_InvalidJSONBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestCreate_EmptyBody(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "fields 'team', 'match', and 'events' are required"}`, response.Body.String())
}

func TestCreate_NonExistentMatch(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified match does not exist"}`, response.Body.String())
}

func TestCreate_FindMatchDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestCreate_NotMatchingEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.Key = "some-key"
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.EventId = "another-key"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified match not part of specified event"}`, response.Body.String())
}

func TestCreate_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestCreate_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestCreate_TeamNotInEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team not in event"}`, response.Body.String())
}

func TestCreate_TeamNotInMatch(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team not in match"}`, response.Body.String())
}

func TestCreate_InvalidEventsList(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	teamMatchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	teamMatchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_data").Return(teamMatchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamMatchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamMatchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.BlueAlliance.Team1 = "frc1"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})
	teamMatchResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"a":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'events' must have float64 keys formatted as strings"}`, response.Body.String())
}

func TestCreate_FindEventMatchAverageError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	teamMatchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	teamMatchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	db.On("Collection", "team_match_data").Return(teamMatchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchAverageCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	teamMatchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamMatchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
		event.Year = 2019
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.BlueAlliance.Team1 = "frc1"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})
	matchAverageResult.On("Decode", mock.AnythingOfType("*database.EventMatchAverage")).Return(errors.New("some database error"))
	teamMatchResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to process data for match"}`, response.Body.String())
}

func TestCreate_InvalidMatchEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	teamMatchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	teamMatchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	db.On("Collection", "team_match_data").Return(teamMatchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchAverageCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	teamMatchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamMatchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
		event.Year = 2019
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.BlueAlliance.Team1 = "frc1"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})
	matchAverageResult.On("Decode", mock.AnythingOfType("*database.EventMatchAverage")).Return(nil)
	teamMatchResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":15, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to process data for match"}`, response.Body.String())
}

func TestCreate_UpdateError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	matchScoutCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	teamMatchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	db.On("Collection", "team_match_data").Return(matchScoutCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchAverageCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("*database.EventMatchAverage"), true, true).Return(errors.New("some database error"))
	matchAverageCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	matchScoutCollection.On("InsertOne", mock.AnythingOfType("database.TeamMatchData")).Return(primitive.NewObjectID(), nil)
	matchScoutCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamMatchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		event := args.Get(0).(*database.Event)
		event.TeamIds = []string{"frc1"}
		event.Year = 2019
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.BlueAlliance.Team1 = "frc1"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Number = 1
	})
	matchAverageResult.On("Decode", mock.AnythingOfType("*database.EventMatchAverage")).Return(nil)
	teamMatchResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}", bytes.NewBufferString(`{"match": "some-match","team": 1,"events": {"1.2":{"type":1, "collected_success":true, "collected_from":1}, "2.2":{"type":1, "collected_success":true, "collected_from":1}}}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to process data for match"}`, response.Body.String())
}
