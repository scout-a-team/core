package match_scout

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestStats(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchAverageCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchAverageResult.On("Err").Return(nil)
	matchAverageResult.On("All", mock.AnythingOfType("*[]database.EventMatchAverage")).Return(nil).Run(func(args mock.Arguments) {
		matchData := reflect.ValueOf(args.Get(0))
		sliceVal := matchData.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.EventMatchAverage{
			Team:       1,
			Event:      "some-event",
			DataPoints: 6,
			Matches:    []string{"some-match-1", "some-match-2"},
		}))
		matchData.Elem().Set(sliceVal.Slice(0, 1))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [{"team":1,"event":"some-event","percent_malfunction":0,"push_elo":0,"data_points":6,"matches":["some-match-1","some-match-2"],"average_total_shots":0,"average_low_shots":0,"average_outer_shots":0,"average_inner_shots":0,"average_overall_accuracy":0,"average_auto_accuracy":0,"average_teleop_accuracy":0,"average_low_accuracy_auto":0,"average_low_accuracy_teleop":0,"average_low_accuracy_cumulative":0,"average_outer_accuracy_auto":0,"average_outer_accuracy_teleop":0,"average_outer_accuracy_cumulative":0,"average_inner_accuracy_auto":0,"average_inner_accuracy_teleop":0,"average_inner_accuracy_cumulative":0,"average_low_cycle_times":0,"average_outer_cycle_times":0,"average_inner_cycle_times":0,"average_collection_accuracy":0,"average_collected":0,"average_climb_time":0,"average_climb_accuracy":0,"average_control_panel_stage_2_accuracy":0,"average_control_panel_stage_3_accuracy":0,"average_block_accuracy":0,"average_blocks":0}]}`, response.Body.String())
}

func TestStats_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestStats_FindEventDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestStats_NoMatchData(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchAverageCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchAverageResult.On("Err").Return(nil)
	matchAverageResult.On("All", mock.AnythingOfType("*[]database.EventMatchAverage")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestStats_FindMatchAveragesDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchAverageCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchAverageResult.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestStats_DecodeError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchAverageCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchAverageResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "team_match_average").Return(matchAverageCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchAverageCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchAverageResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchAverageResult.On("Err").Return(nil)
	matchAverageResult.On("All", mock.AnythingOfType("*[]database.EventMatchAverage")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{event}/stats", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	stats(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
