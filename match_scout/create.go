package match_scout

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

// Event data
type eventData struct {
	Type int `json:"type"`

	// Shot data
	ShotsMade          int `json:"shots_made"`
	ShotsTaken         int `json:"shots_taken"`
	ShotsScoreLocation int `json:"shot_score_location"`
	ShotsFrom          int `json:"shot_from"`

	// power cell collection data
	CollectedSuccess bool `json:"collected_success"`
	CollectedFrom    int  `json:"collected_from"`

	// Control panel stage 2 & 3 data
	CPStart     float64 `json:"cp_start"`
	CPEnd       float64 `json:"cp_end"`
	CPAttempts  int     `json:"cp_attempts"`
	CPCompleted bool    `json:"cp_completed"`

	// Block data
	BlocksSuccessful int `json:"block_successful"`
	BlocksAttempted  int `json:"block_attempted"`

	// Malfunction data
	MalfunctionStart float64 `json:"malfunction_start"`
	MalfunctionEnd   float64 `json:"malfunction_end"`
	MalfunctionType  int     `json:"malfunction_type"`

	// Push data
	PushStart   float64 `json:"push_start"`
	PushEnd     float64 `json:"push_end"`
	PushAgainst int     `json:"push_against"`
	PushWon     bool    `json:"push_won"`

	// Climb data
	ClimbStart      float64 `json:"climb_start"`
	ClimbEnd        float64 `json:"climb_end"`
	ClimbAttempts   int     `json:"climb_attempts"`
	ClimbSuccessful bool    `json:"climb_successful"`
}

func create(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("scouting.match.create").With(zap.String("method", "POST"), zap.String("path", "/scouting/match/{event}"),
		zap.String("event", eventKey), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceMatchScout, rbac.MethodCreate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		Match  string               `json:"match"`
		Team   int                  `json:"team"`
		Events map[string]eventData `json:"events"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if body.Team == 0 || body.Match == "" || len(body.Events) == 0 {
		responses.Error(w, http.StatusBadRequest, "fields 'team', 'match', and 'events' are required")
		return
	}

	// Ensure match exists
	var match database.Match
	if err := db.Collection("matches").FindOne(bson.M{"key": body.Match}).Decode(&match); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified match does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for match in event", zap.Error(err), zap.String("match", body.Match))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure match in event
	if match.EventId != event.Key {
		responses.Error(w, http.StatusForbidden, "specified match not part of specified event")
		return
	}

	// Ensure team exists
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"number": body.Team}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for team", zap.Error(err), zap.Int("team", body.Team))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure team in event
	valid := false
	specifiedTeam := "frc" + strconv.Itoa(team.Number)
	for _, tid := range event.TeamIds {
		if specifiedTeam == tid {
			valid = true
			break
		}
	}
	if !valid {
		responses.Error(w, http.StatusBadRequest, "specified team not in event")
		return
	}

	// Ensure team in match
	if tid := "frc" + strconv.FormatInt(int64(team.Number), 10); tid != match.BlueAlliance.Team1 && tid != match.BlueAlliance.Team2 && tid != match.BlueAlliance.Team3 && tid != match.RedAlliance.Team1 && tid != match.RedAlliance.Team2 && tid != match.RedAlliance.Team3 {
		responses.Error(w, http.StatusBadRequest, "specified team not in match")
		return
	}

	// Ensure match not already scouted
	if err := db.Collection("team_match_data").FindOne(bson.M{"match": body.Match, "team": "frc" + strconv.Itoa(body.Team), "scouted_by": user.TeamId}).Err(); err == nil {
		responses.Error(w, http.StatusBadRequest, "specified team already scouted for match")
		return
	} else if err != mongo.ErrNoDocuments {
		logger.Error("failed to query database for existence of team data for a match", zap.Error(err), zap.String("match", body.Match), zap.Int("team", body.Team))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Convert string keys to float64 keys for match events
	converted, err := stringToFloatKeys(body.Events)
	if err != nil {
		responses.Error(w, http.StatusBadRequest, "field 'events' must have float64 keys formatted as strings")
		return
	}

	// Extract data from events
	if err := processData(body.Match, user.TeamId, body.Team, converted, db); err != nil {
		logger.Error("failed to process data for match", zap.String("match", body.Match), zap.Int("team", body.Team), zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to process data for match")
		return
	}

	responses.Success(w)
	logger.Info("created match scout data for specified match", zap.String("match", body.Match))
}
