package match_scout

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func stats(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("scouting.match.stats").With(zap.String("method", "GET"), zap.String("path", "/scouting/match/{event}/stats"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceMatchScout, rbac.MethodDescribe) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Add event to logger
	logger = logger.With(zap.String("event", eventKey))

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Find all team data for match
	var matchData []database.EventMatchAverage
	cur := db.Collection("team_match_average").FindMany(bson.M{"event": eventKey, "scouted_by": user.TeamId})
	if err := cur.Err(); err != nil {
		logger.Error("failed to query database for event match data", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode database response
	if err := cur.All(&matchData); err != nil {
		logger.Error("failed to decode database response into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Return empty array if nothing
	if matchData == nil {
		responses.SuccessWithData(w, []string{})
		return
	}

	responses.SuccessWithData(w, matchData)
	logger.Info("send scouted data for event", zap.Int("records", len(matchData)))
}
