package teams

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestUpdate(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestUpdate_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestUpdate_InvalidJSON(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestUpdate_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestUpdate_FindOneDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestUpdate_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestUpdate_InvalidCountryCode(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"country": "Some Country"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'country' must be the 2 character country code"}`, response.Body.String())
}

func TestUpdate_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{team}", bytes.NewBufferString(`{"name": "Some Name", "city": "Some City", "country": "SC"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update database"}`, response.Body.String())
}
