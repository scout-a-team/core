package teams

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestListMatches(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "matches").Return(matchCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).Number = 1
	})
	matchResult.On("Err").Return(nil)
	matchResult.On("All", mock.AnythingOfType("*[]database.Match")).Return(nil).Run(func(args mock.Arguments) {
		matches := reflect.ValueOf(args.Get(0))
		sliceVal := matches.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.Match{
			EventId: "some-event",
			Key:     "some-key",
		}))
		matches.Elem().Set(sliceVal.Slice(0, 1))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": ["some-key"]}`, response.Body.String())
}

func TestListMatches_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestListMatches_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestListMatches_NoMatches(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "matches").Return(matchCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)
	matchResult.On("Err").Return(nil)
	matchResult.On("All", mock.AnythingOfType("*[]database.Match")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestListMatches_FindMatchesDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "matches").Return(matchCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)
	matchResult.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestListMatches_DecodeError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "matches").Return(matchCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)
	matchResult.On("Err").Return(nil)
	matchResult.On("All", mock.AnythingOfType("*[]database.Match")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listMatches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
