package teams

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func stats(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	logger := zap.L().Named("teams.stats").With(zap.String("method", "GET"), zap.String("path", "/teams/{team}/stats"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", user.Id.Hex()),
		zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceTeams, rbac.MethodDescribe) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure team exists
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Get averages for team
	var matchData []database.EventMatchAverage
	cur := db.Collection("team_match_average").FindMany(bson.M{"team": team.Number, "scouted_by": user.TeamId})
	if err := cur.Err(); err != nil {
		logger.Error("failed to query database for team data", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode database response
	if err := cur.All(&matchData); err != nil {
		logger.Error("failed to decode database response into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Return empty array if none
	if matchData == nil {
		responses.SuccessWithData(w, []string{})
		return
	}

	responses.SuccessWithData(w, matchData)
	logger.Info("sent scouted data for team in all events", zap.Int("records", len(matchData)))
}
