package teams

import (
	"fmt"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

// Get a list of all teams
func list(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	// Create the logger
	logger := zap.L().Named("teams.list").With(zap.String("method", "GET"), zap.String("path", "/teams"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceTeams, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Validate request on request method and query parameters
	if r.Method != http.MethodGet {
		responses.Error(w, http.StatusMethodNotAllowed, "method not allowed")
		return
	} else if len(r.URL.RawQuery) == 0 {
		responses.Error(w, http.StatusBadRequest, "query parameters 'name' or 'number' are required")
		return
	}

	// Determine query method
	var cur database.FindResult
	var by string
	if r.URL.Query().Get("number") != "" {
		by = "number"

		// Convert query param number to integer
		i, err := strconv.ParseInt(r.URL.Query().Get("number"), 10, 64)
		if err != nil {
			responses.Error(w, http.StatusBadRequest, "query parameter 'number' must be an integer")
			return
		}

		// Run the query
		cur = db.Collection("teams").FindMany(bson.M{"number": i})
	} else if r.URL.Query().Get("name") != "" {
		by = "name"

		// Construct filter
		regex := fmt.Sprintf(".*%s.*", r.URL.Query().Get("name"))

		// Query database
		cur = db.Collection("teams").FindMany(bson.M{"name": bson.M{"$regex": primitive.Regex{Pattern: regex, Options: "i"}}})
	} else {
		responses.Error(w, http.StatusBadRequest, "query parameter 'name' or 'number' must be present")
		return
	}

	// Check for errors in database call
	if err := cur.Err(); err != nil {
		logger.Error("failed to query database for all teams matching specified number", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Parse into array
	var teams []database.Team
	if err := cur.All(&teams); err != nil {
		logger.Error("failed to decode teams", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to parse database response")
		return
	}
	logger.Debug("filtered teams", zap.String("by", by))

	// Return empty array if none
	if teams == nil {
		responses.SuccessWithData(w, []string{})
		return
	}

	// Remove all data except for team name, number, and id
	var resp []map[string]interface{}
	for _, team := range teams {
		resp = append(resp, map[string]interface{}{"number": team.Number, "name": team.Name, "id": team.Id.Hex()})
	}

	responses.SuccessWithData(w, resp)
	logger.Info("got list of teams matching query", zap.String("by", by))
}
