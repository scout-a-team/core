package teams

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestListEvents(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Team).EventIds = []string{"some-event"}
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listEvents(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": ["some-event"]}`, response.Body.String())
}

func TestListEvents_NonExistentTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listEvents(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestListEvents_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "teams").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	listEvents(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
