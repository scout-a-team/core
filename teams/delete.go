package teams

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

var registered = false

// Delete a team and all its associated data
func deleteMethod(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	authUser := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	logger := zap.L().Named("teams.delete").With(zap.String("method", "DELETE"), zap.String("path", "/teams/{team}"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", authUser.Id.Hex()), zap.String("authenticated_team", authUser.TeamId))

	if !rbac.Enforce(authUser.Role, rbac.ResourceTeams, rbac.MethodDelete) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if authUser.TeamId != teamId.Hex() {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Get specified team with users
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for specific team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Retrieve all users with specified team for deletion of tokens
	cur := db.Collection("users").FindMany(bson.M{"team_id": team.Id.Hex()})
	if err := cur.Err(); err != nil {
		logger.Error("failed to find users associated with team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Delete tokens associated with user
	for cur.Next() {
		// Get user
		var user database.User
		if err := cur.Decode(&user); err != nil {
			logger.Error("failed to decode user into structure", zap.Error(err), zap.String("user_id", user.Id.Hex()))
			responses.Error(w, http.StatusInternalServerError, "failed to decode user")
			return
		}

		// Delete user's tokens
		if err := db.Collection("tokens").DeleteMany(bson.M{"user_id": user.Id.Hex()}); err != nil {
			logger.Error("failed to delete user's tokens", zap.Error(err), zap.String("user_id", user.Id.Hex()))
			responses.Error(w, http.StatusInternalServerError, "failed to delete user's tokens")
			return
		}

		// Finally, delete the user
		if err := db.Collection("users").DeleteOne(bson.M{"_id": user.Id}); err != nil {
			logger.Error("failed to delete user", zap.Error(err), zap.String("user_id", user.Id.Hex()))
			responses.Error(w, http.StatusInternalServerError, "failed to delete user")
			return
		}
	}

	// Unregister the team
	team.Registered = &registered
	if err := db.Collection("teams").Update(bson.M{"_id": team.Id}, bson.M{"$set": team}, false, false); err != nil {
		logger.Error("failed to delete team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete team")
		return
	}

	responses.Success(w)
	logger.Info("deleted team and user information")
}
