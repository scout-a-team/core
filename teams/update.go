package teams

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func update(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	// Create the logger
	logger := zap.L().Named("teams.update").With(zap.String("method", "PUT"), zap.String("path", "/teams/{team}"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceTeams, rbac.MethodUpdate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	var body struct {
		Name    string `json:"name"`
		City    string `json:"city"`
		Country string `json:"country"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	}

	// Retrieve specified team
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for specific team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure user is part of specified team
	if user.TeamId != teamId.Hex() {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Modify name if passed
	if body.Name != "" {
		team.Name = body.Name
		logger.Debug("updated team name")
	}

	// Modify location if passed
	if body.City != "" {
		team.City = body.City
		logger.Debug("updated team location")
	}

	// Modify country if passed
	if body.Country != "" {
		// Validate length
		if len(body.Country) != 2 {
			responses.Error(w, http.StatusBadRequest, "field 'country' must be the 2 character country code")
			return
		}

		team.Country = body.Country
		logger.Debug("updated team country")
	}

	// Save changes
	if err := db.Collection("teams").Update(bson.M{"_id": teamId}, bson.D{{Key: "$set", Value: team}}, false, false); err != nil {
		logger.Error("failed to update team entry in database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update database")
		return
	}

	responses.Success(w)
	logger.Info("updated team information")
}
