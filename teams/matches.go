package teams

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

func listMatches(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	logger := zap.L().Named("teams.matches").With(zap.String("method", "GET"), zap.String("path", "/teams/{team}/matches"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceTeams, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure team exists
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for specific team", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Get all matches a team is in
	key := "frc" + strconv.Itoa(team.Number)
	cur := db.Collection("matches").FindMany(bson.M{"$or": []bson.M{
		{"blue_alliance.team_1": key}, {"blue_alliance.team_2": key}, {"blue_alliance.team_3": key},
		{"red_alliance.team_1": key}, {"red_alliance.team_2": key}, {"red_alliance.team_3": key},
	}})
	if err := cur.Err(); err != nil {
		logger.Error("failed to query database for list of matches", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode into array
	var matches []database.Match
	if err := cur.All(&matches); err != nil {
		logger.Error("failed to decode database response into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Return empty array if no data
	if matches == nil {
		responses.SuccessWithData(w, []string{})
		return
	}

	// Extract match keys
	var matchKeys []string
	for _, match := range matches {
		matchKeys = append(matchKeys, match.Key)
	}

	responses.SuccessWithData(w, matchKeys)
	logger.Info("send list of matches a team is in")
}
