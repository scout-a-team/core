package teams

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          rbac.RoleOwner,
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestDelete(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	roleCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "roles").Return(roleCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	roleCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(nil)
	manyUserResult.On("Next").Return(true).Once()
	manyUserResult.On("Next").Return(false)
	manyUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestDelete_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestDelete_UserNotOwnerOfTeam(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)

	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "not-owner"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, database.User{Role: rbac.RoleNone, TeamId: user.TeamId}))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())
}

func TestDelete_NoTeamFound(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestDelete_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestDelete_FindManyUserDatabaseError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(errors.New("some database error"))
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestDelete_CursorUserDecodeError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(nil)
	manyUserResult.On("Next").Return(true).Once()
	manyUserResult.On("Next").Return(false)
	manyUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to decode user"}`, response.Body.String())
}

func TestDelete_DeleteManyTokensError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(nil)
	manyUserResult.On("Next").Return(true).Once()
	manyUserResult.On("Next").Return(false)
	manyUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete user's tokens"}`, response.Body.String())
}

func TestDelete_DeleteUserError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(nil)
	manyUserResult.On("Next").Return(true).Once()
	manyUserResult.On("Next").Return(false)
	manyUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete user"}`, response.Body.String())
}

func TestDelete_DeleteTeamError(t *testing.T) {
	// Setup facilities
	id, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	teamCollection := &mocks.Collection{}
	roleCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	singleUserResult := &mocks.FindResult{}
	manyUserResult := &mocks.FindResult{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "roles").Return(roleCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(singleUserResult)
	userCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(manyUserResult)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	roleCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	singleUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})
	manyUserResult.On("Err").Return(nil)
	manyUserResult.On("Next").Return(true).Once()
	manyUserResult.On("Next").Return(false)
	manyUserResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{team}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete team"}`, response.Body.String())
}
