package users

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestUpdate(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestUpdate_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestUpdate_InvalidContentType(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestUpdate_InvalidJSON(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestUpdate_NotMatchingUserWithValidPermissions(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestUpdate_NotMatchingUserWithInvalidPermissions(t *testing.T) {
	// Modify user information
	user.Role = rbac.RoleMember

	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "user not permitted to access route with given method"}`, response.Body.String())

	// Reset user data
	user.Role = rbac.RoleAdministrator
}

func TestUpdate_InvalidEmail(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"email": "abc"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'email' must be between 5 and 254 characters long"}`, response.Body.String())
}

func TestUpdate_InvalidPassword(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"password": "abc"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' must be 64 characters long"}`, response.Body.String())
}

func TestUpdate_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(errors.New("some database error"))
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)
	go func() { <-mailChan }()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}", bytes.NewBufferString(`{"name": "Some Name", "email": "some@email.com", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=", "role": "some_role", "enabled": false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	update(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update database"}`, response.Body.String())
}
