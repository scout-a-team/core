package users

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestRead(t *testing.T) {
	// Setup facilities
	userId := primitive.NewObjectID()
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Id = userId
		user.TeamId = teamId.Hex()
		user.Name = "Some Name"
		user.Email = "some@email.com"
		user.Role = "some_role"
		user.Enabled = true
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": {"id":"`+userId.Hex()+`","name":"Some Name","email":"some@email.com","role":"some_role","enabled":true,"email_verified":false}}`, response.Body.String())
}

func TestRead_DatabaseError(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestRead_UserNotInSameTeam(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified user is not part of authenticated user's team"}`, response.Body.String())
}
