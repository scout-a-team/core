package users

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func disableTwoFactor(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	// Create the logger
	logger := zap.L().Named("users.disable_2fa").With(zap.String("method", "DELETE"), zap.String("path", "/teams/{team}/users/{user}/2fa"),
		zap.String("team", teamId.Hex()), zap.String("user", userId.Hex()),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceUsers, rbac.MethodUpdate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != user.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Ensure query parameter is present
	if r.URL.Query().Get("method") == "" || r.URL.RawQuery == "" {
		responses.Error(w, http.StatusBadRequest, "query parameter 'method' is required")
		return
	}

	// Ensure specified user and requesting user are the same
	if user.Id.Hex() != userId.Hex() {
		responses.Error(w, http.StatusForbidden, "specified user and authenticated user do not match")
		return
	}

	// Operate on removing different 2fa methods
	switch r.URL.Query().Get("method") {
	case "totp":
		user.TOTP = database.TwoFactor{Enabled: false}

	case "webauthn":
		user.WebAuthn = database.TwoFactor{Enabled: false}

	case "all":
		user.TOTP = database.TwoFactor{Enabled: false}
		user.WebAuthn = database.TwoFactor{Enabled: false}

	default:
		responses.Error(w, http.StatusBadRequest, "query parameter 'method' must be 'totp', 'webauthn', or 'all'")
		return
	}

	// Save changes
	if err := db.Collection("users").Update(bson.M{"_id": userId}, bson.D{{Key: "$set", Value: user}}, false, false); err != nil {
		logger.Error("failed to update user entry in database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update database")
		return
	}

	responses.Success(w)
	logger.Info("removed two factor method(s) for user", zap.String("method", r.URL.Query().Get("method")))
}
