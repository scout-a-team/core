package users

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

// Delete a user from a team
func deleteMethod(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	// Create the logger
	logger := zap.L().Named("users.delete").With(zap.String("method", "DELETE"), zap.String("path", "/teams/{team}/users"),
		zap.String("team", teamId.Hex()), zap.String("user", userId.Hex()), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceUsers, rbac.MethodDelete) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != user.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Retrieve user to be deleted
	var deleteUser database.User
	if err := db.Collection("users").FindOne(bson.M{"_id": userId}).Decode(&deleteUser); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified user does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for user to delete", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database for user")
		return
	}

	// Ensure user is not team owner
	if deleteUser.Role == rbac.RoleOwner {
		responses.Error(w, http.StatusForbidden, "owner must delete team to delete self")
		return
	}

	// Delete tokens associated with user
	if err := db.Collection("tokens").DeleteMany(bson.M{"user_id": deleteUser.Id.Hex()}); err != nil {
		logger.Error("failed to delete user's tokens", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete tokens")
		return
	}

	// Delete user
	if err := db.Collection("users").DeleteOne(bson.M{"_id": deleteUser.Id}); err != nil {
		logger.Error("failed to delete user", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to delete user")
		return
	}

	responses.Success(w)
	logger.Info("deleted user from team")
}
