package users

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func assign(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	authUser := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	// Create logger
	logger := zap.L().Named("users.assign").With(zap.String("method", "PUT"), zap.String("path", "/teams/{team}/users/{user}/assign"),
		zap.String("team", teamId.Hex()), zap.String("user", userId.Hex()), zap.String("authenticated_user", authUser.Id.Hex()),
		zap.String("authenticated_team", authUser.TeamId))

	if !rbac.Enforce(authUser.Role, rbac.ResourceUsers, rbac.MethodAssign) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != authUser.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Validate headers
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		Role rbac.Key `json:"role"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body "+err.Error())
		return
	} else if body.Role != rbac.RoleOwner && body.Role != rbac.RoleAdministrator && body.Role != rbac.RoleMentor && body.Role != rbac.RoleMember {
		responses.Error(w, http.StatusBadRequest, "field 'role' must be one of 'administrator', 'mentor', or 'member'")
		return
	} else if body.Role == rbac.RoleOwner {
		responses.Error(w, http.StatusBadRequest, "cannot create a new owner")
		return
	}

	// Check that user exists
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"_id": userId}).Decode(&user); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified user does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query user", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure not owner
	if user.Role == rbac.RoleOwner {
		responses.Error(w, http.StatusForbidden, "cannot change owner's role")
		return
	}

	// Set role
	user.Role = body.Role

	// Update database
	if err := db.Collection("users").Update(bson.M{"_id": user.Id}, bson.M{"$set": user}, false, false); err != nil {
		logger.Error("failed to update user's role", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update user")
		return
	}

	responses.Success(w)
	logger.Info("assigned new role to user", zap.String("role", string(body.Role)))
}
