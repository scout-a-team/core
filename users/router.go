package users

import (
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/request_context"
)

// Add routes to main router
func Route(r chi.Router) {
	r.Use(request_context.TeamIdCtx)
	r.Get("/", list)
	r.Post("/", create)

	r.Route("/{user}", func(r chi.Router) {
		r.Use(request_context.UserIdCtx)
		r.Get("/", read)
		r.Put("/", update)
		r.Delete("/", deleteMethod)
		r.Put("/2fa", enableTwoFactor)
		r.Delete("/2fa", disableTwoFactor)
		r.Put("/assign", assign)
	})
}
