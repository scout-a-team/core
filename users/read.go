package users

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func read(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	authUser := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	logger := zap.L().Named("users.read").With(zap.String("method", "GET"), zap.String("path", "/teams/{team}/users/{user}"),
		zap.String("team", teamId.Hex()), zap.String("userId", userId.Hex()), zap.String("authenticated_user", authUser.Id.Hex()),
		zap.String("authenticated_team", authUser.TeamId))

	if !rbac.Enforce(authUser.Role, rbac.ResourceUsers, rbac.MethodDescribe) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != authUser.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Get user from database
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"_id": userId}).Decode(&user); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified user does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for user", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure specified user and requester are in the same team
	if authUser.TeamId != user.TeamId {
		responses.Error(w, http.StatusForbidden, "specified user is not part of authenticated user's team")
		return
	}

	responses.SuccessWithData(w, user)
	logger.Info("got user in team")
}
