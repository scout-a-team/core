package users

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

// List all users in a team
func list(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)

	logger := zap.L().Named("users.read").With(zap.String("method", "GET"), zap.String("path", "/teams/{team}/users"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceUsers, rbac.MethodList) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != user.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Find all users in team
	cur := db.Collection("users").FindMany(bson.M{"team_id": teamId.Hex()})
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.SuccessWithData(w, []string{})
		return
	} else if err != nil {
		logger.Error("failed to query database for all users in team")
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode into array
	var users []database.User
	for cur.Next() {
		var user database.User
		if err := cur.Decode(&user); err != nil {
			logger.Error("failed to decode users", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to decode users")
			return
		}
		users = append(users, user)
	}

	responses.SuccessWithData(w, users)
	logger.Info("got list of users in team")
}
