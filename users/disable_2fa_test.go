package users

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestDisableTwoFactor_TOTP(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=totp", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestDisableTwoFactor_WebAuthn(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=webauthn", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestDisableTwoFactor_All(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=all", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestDisableTwoFactor_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=all", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestDisableTwoFactor_NoQueryParameters(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'method' is required"}`, response.Body.String())
}

func TestDisableTwoFactor_NotMatchingUser(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=all", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified user and authenticated user do not match"}`, response.Body.String())
}

func TestDisableTwoFactor_Invalid2FAMethod(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=abc", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "query parameter 'method' must be 'totp', 'webauthn', or 'all'"}`, response.Body.String())
}

func TestDisableTwoFactor_UpdateDatabaseError(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}/2fa?method=all", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	disableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update database"}`, response.Body.String())
}
