package users

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"net/http"
)

func enableTwoFactor(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	// Create the logger
	logger := zap.L().Named("users.enable_2fa").With(zap.String("method", "PUT"), zap.String("path", "/teams/{team}/users/{user}/2fa"),
		zap.String("team", teamId.Hex()), zap.String("user", userId.Hex()),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	if !rbac.Enforce(user.Role, rbac.ResourceUsers, rbac.MethodUpdate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != user.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	var body struct {
		TOTP     bool `json:"totp"`
		WebAuthn bool `json:"webauthn"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if !body.TOTP && !body.WebAuthn {
		responses.Error(w, http.StatusBadRequest, "at least one two factor method must be enabled")
		return
	}

	logger = logger.With(zap.Bool("totp", body.TOTP), zap.Bool("webauthn", body.WebAuthn))

	// Ensure specified user and requesting user are the same
	if user.Id.Hex() != userId.Hex() {
		responses.Error(w, http.StatusForbidden, "specified user and authenticated user do not match")
		return
	}

	// Create dynamic response
	response := make(map[string]interface{})

	// Enable TOTP
	if body.TOTP {
		key, err := totp.Generate(totp.GenerateOpts{
			Issuer:      "scouta.team",
			AccountName: user.Email,
			Period:      30,
			Digits:      otp.DigitsSix,
			Algorithm:   otp.AlgorithmSHA1,
		})
		if err != nil {
			logger.Error("failed to generate totp secret", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to generate code")
			return
		}

		user.TOTP = database.TwoFactor{
			Secret:  key.Secret(),
			Enabled: true,
		}

		response["totp"] = key.String()
	}

	// Enable WebAuthn
	if body.WebAuthn {
		// TODO: add webauthn flow
	}

	// Generate recovery codes
	if user.RecoveryCodes == nil || len(user.RecoveryCodes) == 0 {
		rawCodes := make([]byte, 64)
		if _, err := rand.Read(rawCodes); err != nil {
			logger.Error("failed to generate recovery codes base secret", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to generate recovery codes")
			return
		}

		codes := make([]string, 8)
		for i := 0; i < 8; i++ {
			codes[i] = hex.EncodeToString(rawCodes[i*8 : (i+1)*8])
		}

		user.RecoveryCodes = codes
		response["recovery_codes"] = codes
	}

	// Save changes
	if err := db.Collection("users").Update(bson.M{"_id": userId}, bson.D{{Key: "$set", Value: user}}, false, false); err != nil {
		logger.Error("failed to update user entry in database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update database")
		return
	}

	responses.SuccessWithData(w, response)
	logger.Info("enabled 2fa for user")
}
