package users

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestEnableTwoFactor(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	userId := primitive.NewObjectID()

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Id = userId
		user.Email = "some@email.com"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))

	// Validate body
	var body struct {
		Status string `json:"status"`
		Data   struct {
			Recovery []string `json:"recovery_codes"`
			Totp     string   `json:"totp"`
		} `json:"data"`
		Reason string `json:"reason"`
	}
	assert.NoError(t, json.NewDecoder(response.Body).Decode(&body))
	assert.Equal(t, "success", body.Status)
	assert.Equal(t, 8, len(body.Data.Recovery))
	assert.NotEmpty(t, body.Data.Totp)
}

func TestEnableTwoFactor_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestEnableTwoFactor_InvalidContentType(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestEnableTwoFactor_InvalidJSON(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestEnableTwoFactor_NoneEnabled(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":false, "webauthn":false}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "at least one two factor method must be enabled"}`, response.Body.String())
}

func TestEnableTwoFactor_NotMatchingUser(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified user and authenticated user do not match"}`, response.Body.String())
}

func TestEnableTwoFactor_TotpGenerationError(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, database.User{Id: user.Id, TeamId: user.TeamId, Role: rbac.RoleAdministrator}))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to generate code"}`, response.Body.String())
}

func TestEnableTwoFactor_UpdateDatabaseError(t *testing.T) {
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	userId := primitive.NewObjectID()

	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.D"), false, false).Return(errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Id = userId
		user.Email = "some@email.com"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/{user}/2fa", bytes.NewBufferString(`{"totp":true, "webauthn":true}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	enableTwoFactor(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update database"}`, response.Body.String())
}
