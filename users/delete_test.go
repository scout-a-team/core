package users

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

func TestDelete(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestDelete_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(nil)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestDelete_TeamOwner(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = "owner"
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, database.User{TeamId: user.TeamId, Id: user.Id, Role: rbac.RoleOwner}))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "owner must delete team to delete self"}`, response.Body.String())
}

func TestDelete_TokensDeleteManyError(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete tokens"}`, response.Body.String())
}

func TestDelete_DeleteUserDatabaseError(t *testing.T) {
	// Setup facilities
	teamId, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	userCollection.On("DeleteOne", mock.AnythingOfType("primitive.M")).Return(errors.New("some database error"))
	tokenCollection.On("DeleteMany", mock.AnythingOfType("primitive.M")).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("DELETE", "/{user}", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, teamId))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, user.Id))

	// Run request
	deleteMethod(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to delete user"}`, response.Body.String())
}
