package users

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

// Create a new user in a team
func create(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	authUser := request_context.GetTokenUser(r)
	teamId := request_context.GetTeamId(r)
	messages := request_context.GetMailFacility(r)

	// Create the logger
	logger := zap.L().Named("users.create").With(zap.String("method", "POST"), zap.String("path", "/teams/{team}/users"),
		zap.String("team", teamId.Hex()), zap.String("authenticated_user", authUser.Id.Hex()), zap.String("authenticated_team", authUser.TeamId))

	if !rbac.Enforce(authUser.Role, rbac.ResourceUsers, rbac.MethodCreate) {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != authUser.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse and validate JSON
	var body struct {
		FirstName string   `json:"first_name"`
		LastName  string   `json:"last_name"`
		Email     string   `json:"email"`
		Password  string   `json:"password"`
		Role      rbac.Key `json:"role"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	} else if body.FirstName == "" || body.LastName == "" || body.Email == "" || body.Role == rbac.RoleNone {
		responses.Error(w, http.StatusBadRequest, "fields 'first_name', 'last_name', 'email', 'password', and 'role' are required")
		return
	} else if len(body.Email) < 5 || len(body.Email) > 254 {
		responses.Error(w, http.StatusBadRequest, "field 'email' must be between 5 and 254 characters long")
		return
	} else if len(body.Password) > 0 && len(body.Password) != 64 {
		responses.Error(w, http.StatusBadRequest, "field 'password' must be 64 characters long")
		return
	} else if body.Role != rbac.RoleOwner && body.Role != rbac.RoleMember && body.Role != rbac.RoleMentor && body.Role != rbac.RoleAdministrator {
		responses.Error(w, http.StatusBadRequest, "field 'role' must be one of 'administrator', 'mentor', or 'member'")
		return
	}

	// Check that team has an account
	var team database.Team
	if err := db.Collection("teams").FindOne(bson.M{"_id": teamId}).Decode(&team); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified team does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for team's existence", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Check that email is not in use
	var user database.User
	if err := db.Collection("users").FindOne(bson.M{"email": body.Email}).Decode(&user); err == nil {
		responses.Error(w, http.StatusConflict, "specified email address is already in use")
		return
	} else if err != mongo.ErrNoDocuments {
		logger.Error("failed to query database to check if email is in use", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}
	logger.Debug("validated team exists and email not in use")

	// Hash password if provided
	h, err := hash.DefaultHash(body.Password)
	if err != nil {
		logger.Error("failed to hash password", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to hash password")
		return
	}

	// Create user entry
	user = database.User{
		Name:     body.FirstName + " " + body.LastName,
		Email:    body.Email,
		Password: h,
		Role:     body.Role,
		TeamId:   team.Id.Hex(),
		Enabled:  len(h) != 0,
	}
	insertedUser, err := db.Collection("users").InsertOne(user)
	if err != nil {
		logger.Error("failed to insert user into database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to insert user")
		return
	}

	// Generate token for email validation
	token, _, err := jwt.GenerateEmailVerification(r.UserAgent(), user, db)
	if err != nil {
		logger.Error("failed to generate email validation token", zap.Error(err), zap.String("token", "email-verification"))
		responses.Error(w, http.StatusInternalServerError, "failed to generate email verification token")
		return
	}

	// Send verification email
	messages <- mail.Message{
		Subject:   "Please validate your email",
		Recipient: user.Email,
		Template:  "email-verification",
		Variables: map[string]interface{}{
			"token": token,
			"name":  user.Name,
		},
	}

	// Send on-boarding email
	messages <- mail.Message{
		Subject:   "Welcome to Scout a Team",
		Recipient: user.Email,
		Template:  "user-registration",
		Variables: map[string]interface{}{
			"name": user.Name,
			"team": strconv.Itoa(team.Number),
		},
	}

	responses.Success(w)
	logger.Info("create new user under team", zap.String("user", insertedUser.(primitive.ObjectID).Hex()))
}
