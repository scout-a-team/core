package users

import (
	"encoding/json"
	"gitlab.com/scout-a-team/core/pkg/hash"
	"gitlab.com/scout-a-team/core/pkg/jwt"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

// Modify a user's information
func update(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	messages := request_context.GetMailFacility(r)
	teamId := request_context.GetTeamId(r)
	userId := request_context.GetUserId(r)

	// Create the logger
	logger := zap.L().Named("users.update").With(zap.String("method", "PUT"), zap.String("path", "/teams/{team}/users/{user}"),
		zap.String("team", teamId.Hex()), zap.String("user", userId.Hex()),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Check if allowed to modify all users or modifying self
	if !rbac.Enforce(user.Role, rbac.ResourceUsers, rbac.MethodUpdate) && user.Id.Hex() != userId.Hex() {
		responses.Error(w, http.StatusForbidden, "user not permitted to access route with given method")
		return
	}

	// Ensure user is part of team
	if teamId.Hex() != user.TeamId {
		responses.Error(w, http.StatusForbidden, "authenticated user is not part of specified team")
		return
	}

	// Validate headers, and ensure body exists
	if r.Header.Get("Content-Type") != "application/json" {
		responses.Error(w, http.StatusBadRequest, "header 'Content-Type' must be 'application/json'")
		return
	}

	// Parse JSON body
	var body struct {
		Name     string `json:"name"`
		Email    string `json:"email"`
		Password string `json:"password"`
		Enabled  *bool  `json:"enabled"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		responses.Error(w, http.StatusBadRequest, "failed to parse body: "+err.Error())
		return
	}

	// If modifying different user, update `user` variable
	if user.Id.Hex() != userId.Hex() {
		if err := db.Collection("users").FindOne(bson.M{"_id": userId}).Decode(&user); err == mongo.ErrNoDocuments {
			responses.Error(w, http.StatusNotFound, "specified user does not exist")
			return
		} else if err != nil {
			logger.Error("failed to query database for user", zap.Error(err), zap.String("user", userId.Hex()))
			responses.Error(w, http.StatusInternalServerError, "failed to query database")
			return
		}
	}

	// Modify name if passed
	if body.Name != "" {
		user.Name = body.Name
		logger.Debug("updated user name")
	}

	// Modify email if passed
	if body.Email != "" {
		// Validate length
		if len(body.Email) < 5 || len(body.Email) > 254 {
			responses.Error(w, http.StatusBadRequest, "field 'email' must be between 5 and 254 characters long")
			return
		}

		user.Email = body.Email
		logger.Debug("updated user email")

		// Generate token for email validation
		token, _, err := jwt.GenerateEmailVerification(r.UserAgent(), user, db)
		if err != nil {
			logger.Error("failed to generate email validation token", zap.Error(err), zap.String("token", "email-verification"))
			responses.Error(w, http.StatusInternalServerError, "failed to generate email verification")
			return
		}

		messages <- mail.Message{
			Subject:   "Please verify your email",
			Recipient: user.Email,
			Template:  "email-verification",
			Variables: map[string]interface{}{
				"token": token,
				"name":  user.Name,
			},
		}
	}

	// Modify password if passed
	if body.Password != "" {
		// Validate length
		if len(body.Password) != 64 {
			responses.Error(w, http.StatusBadRequest, "field 'password' must be 64 characters long")
			return
		}

		// Hash password
		h, err := hash.DefaultHash(body.Password)
		if err != nil {
			logger.Error("failed to hash password", zap.Error(err))
			responses.Error(w, http.StatusInternalServerError, "failed to hash password")
			return
		}

		user.Password = h
		logger.Debug("updated user password")
	}

	// Modify enabled status if passed
	if body.Enabled != nil {
		user.Enabled = *body.Enabled
		logger.Debug("updated user enabled status")
	}

	// Save changes
	if err := db.Collection("users").Update(bson.M{"_id": userId}, bson.D{{Key: "$set", Value: user}}, false, false); err != nil {
		logger.Error("failed to update user entry in database", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to update database")
		return
	}

	responses.Success(w)
	logger.Info("updated user information in team")
}
