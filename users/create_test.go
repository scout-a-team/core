package users

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mail"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          rbac.RoleAdministrator,
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestCreate(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	tokenCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	db.On("Collection", "tokens").Return(tokenCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("InsertOne", mock.AnythingOfType("database.User")).Return(primitive.NewObjectID(), nil)
	tokenCollection.On("InsertOne", mock.AnythingOfType("database.Token")).Return(primitive.NewObjectID(), nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil).Run(func(args mock.Arguments) {
		team := args.Get(0).(*database.Team)
		team.Id = tid
	})
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	mailChan := make(chan mail.Message)
	go func() {
		<-mailChan
		<-mailChan
	}()

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "administrator", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestCreate_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestCreate_InvalidContentType(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestCreate_InvalidJSON(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"team":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body: invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestCreate_NoBodyFields(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "fields 'first_name', 'last_name', 'email', 'password', and 'role' are required"}`, response.Body.String())
}

func TestCreate_InvalidEmail(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "abc", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'email' must be between 5 and 254 characters long"}`, response.Body.String())
}

func TestCreate_InvalidPassword(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abc"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'password' must be 64 characters long"}`, response.Body.String())
}

func TestCreate_NonExistentTeam(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(mongo.ErrNoDocuments)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified team does not exist"}`, response.Body.String())
}

func TestCreate_FindTeamDatabaseError(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(errors.New("some database error"))

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestCreate_FindUserExists(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	teamCollection := &mocks.Collection{}
	userCollection := &mocks.Collection{}
	teamResult := &mocks.FindResult{}
	userResult := &mocks.FindResult{}
	db.On("Collection", "teams").Return(teamCollection)
	db.On("Collection", "users").Return(userCollection)
	teamCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(teamResult)
	userCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(userResult)
	userCollection.On("InsertOne", mock.AnythingOfType("database.User")).Return(primitive.NewObjectID(), nil)
	teamResult.On("Decode", mock.AnythingOfType("*database.Team")).Return(nil)
	userResult.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	mailChan := make(chan mail.Message)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"first_name": "Some", "last_name": "User", "email": "some@email.com", "role": "owner", "password": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityMail, mailChan))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	create(response, request)

	// Validate response
	assert.Equal(t, 409, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified email address is already in use"}`, response.Body.String())
}
