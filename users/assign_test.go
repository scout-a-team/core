package users

import (
	"bytes"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestAssign(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(nil)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success"}`, response.Body.String())
}

func TestAssign_NotMatchingTeam(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, primitive.NewObjectID()))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "authenticated user is not part of specified team"}`, response.Body.String())
}

func TestAssign_InvalidContentType(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "header 'Content-Type' must be 'application/json'"}`, response.Body.String())
}

func TestAssign_InvalidJSON(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to parse body invalid character '}' looking for beginning of value"}`, response.Body.String())
}

func TestAssign_InvalidRole(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"none"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 400, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "field 'role' must be one of 'administrator', 'mentor', or 'member'"}`, response.Body.String())
}

func TestAssign_NonExistentUser(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified user does not exist"}`, response.Body.String())
}

func TestAssign_FindUserDatabaseError(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestAssign_ModifyingOwner(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil).Run(func(args mock.Arguments) {
		user := args.Get(0).(*database.User)
		user.Role = rbac.RoleOwner
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 403, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "cannot change owner's role"}`, response.Body.String())
}

func TestAssign_UpdateDatabaseError(t *testing.T) {
	// Setup facilities
	tid, _ := primitive.ObjectIDFromHex(user.TeamId)
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "users").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	collection.On("Update", mock.AnythingOfType("primitive.M"), mock.AnythingOfType("primitive.M"), false, false).Return(errors.New("some database error"))
	result.On("Decode", mock.AnythingOfType("*database.User")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/teams/{team}/users/{user}/assign", bytes.NewBufferString(`{"role":"member"}`))
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.TeamId, tid))
	request = request.WithContext(context.WithValue(request.Context(), request_context.UserId, primitive.NewObjectID()))
	request.Header.Set("Content-Type", "application/json")

	// Run request
	assign(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to update user"}`, response.Body.String())
}
