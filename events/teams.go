package events

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func teams(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("events.teams").With(zap.String("path", "/api/events/{event}/teams"), zap.String("method", "GET"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_user", user.TeamId), zap.String("event", eventKey))

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event existence", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	responses.SuccessWithData(w, event.TeamIds)
	logger.Info("sent teams in event")
}
