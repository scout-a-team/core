package events

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/rbac"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

var user = database.User{
	Id:            primitive.NewObjectID(),
	Name:          "Some User",
	Email:         "some@us.er",
	Password:      "some-hashed-password",
	Role:          rbac.RoleOwner,
	TeamId:        primitive.NewObjectID().Hex(),
	Enabled:       true,
	VerifiedEmail: true,
	TOTP:          database.TwoFactor{},
	WebAuthn:      database.TwoFactor{},
	RecoveryCodes: nil,
}

func TestList(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Err").Return(nil)
	eventResult.On("All", mock.AnythingOfType("*[]database.Event")).Return(nil).Run(func(args mock.Arguments) {
		events := reflect.ValueOf(args.Get(0))
		sliceVal := events.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.Event{
			Name: "Some Event",
			Key:  "some-key",
		}))
		events.Elem().Set(sliceVal.Slice(0, 1))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	list(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [{"id":"000000000000000000000000","name":"Some Event","key":"some-key","start":"0001-01-01T00:00:00Z","end":"0001-01-01T00:00:00Z","year":0,"city":"","country":"","state_province":""}]}`, response.Body.String())
}

func TestList_NoEvents(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	list(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestList_FindEventsDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	list(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestList_DecodeEventsError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Err").Return(nil)
	eventResult.On("All", mock.AnythingOfType("*[]database.Event")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))

	// Run request
	list(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
