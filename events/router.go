package events

import (
	"github.com/go-chi/chi"
	"gitlab.com/scout-a-team/core/pkg/request_context"
)

func Route(r chi.Router) {
	r.Get("/", list)

	r.Route("/{event}", func(r chi.Router) {
		r.Use(request_context.EventKeyCtx)
		r.Get("/", read)
		r.Get("/matches", matches)
		r.Get("/teams", teams)

		r.Route("/matches/{match}", func(r chi.Router) {
			r.Use(request_context.MatchKeyCtx)
			r.Get("/", match)
		})
	})
}
