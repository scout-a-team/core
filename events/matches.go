package events

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func matches(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)

	logger := zap.L().Named("events.matches").With(zap.String("path", "/events/{event}/matches"), zap.String("method", "GET"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId), zap.String("event", eventKey))

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for event", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Find all matches in event
	cur := db.Collection("matches").FindMany(bson.M{"event": event.Key})
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.SuccessWithData(w, []string{})
		return
	} else if err != nil {
		logger.Error("failed to query database for matches in event", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode into array
	var matches []database.Match
	if err := cur.All(&matches); err != nil {
		logger.Error("failed to decode matches into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Strip down raw data
	var response []map[string]interface{}
	for _, match := range matches {
		response = append(response, map[string]interface{}{
			"id":               match.Id,
			"key":              match.Key,
			"scheduled_at":     match.ScheduledAt,
			"happened_at":      match.HappenedAt,
			"match_type":       match.MatchType,
			"match_set_number": match.MatchSetNumber,
			"match_number":     match.MatchNumber,
			"red_alliance": map[string]string{
				"team1": match.RedAlliance.Team1,
				"team2": match.RedAlliance.Team2,
				"team3": match.RedAlliance.Team3,
			},
			"blue_alliance": map[string]string{
				"team1": match.BlueAlliance.Team1,
				"team2": match.BlueAlliance.Team2,
				"team3": match.BlueAlliance.Team3,
			},
			"winner": match.Winner,
		})
	}

	responses.SuccessWithData(w, response)
	logger.Info("got all matches in event", zap.Int("records", len(matches)))
}
