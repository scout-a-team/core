package events

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func list(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)

	logger := zap.L().Named("events.list").With(zap.String("path", "/api/events"), zap.String("method", "GET"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId))

	// Event by team
	query := bson.M{}
	if r.URL.Query().Get("team") != "" {
		query["teams"] = r.URL.Query().Get("team")
	}

	// Find all events for team in database
	cur := db.Collection("events").FindMany(query)
	if err := cur.Err(); err == mongo.ErrNoDocuments {
		responses.SuccessWithData(w, []string{})
		return
	} else if err != nil {
		logger.Error("failed to query database for events", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Decode events
	var events []database.Event
	if err := cur.All(&events); err != nil {
		logger.Error("failed to decode events into array", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	responses.SuccessWithData(w, events)
	logger.Info("got list of events", zap.Int("records", len(events)))
}
