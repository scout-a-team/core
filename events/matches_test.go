package events

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestMatches(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Err").Return(nil)
	matchResult.On("All", mock.AnythingOfType("*[]database.Match")).Return(nil).Run(func(args mock.Arguments) {
		matches := reflect.ValueOf(args.Get(0))
		sliceVal := matches.Elem()
		elemType := sliceVal.Type().Elem()
		sliceVal = reflect.Append(sliceVal, reflect.New(elemType).Elem())
		sliceVal.Index(0).Set(reflect.ValueOf(database.Match{
			Key: "some-key",
		}))
		matches.Elem().Set(sliceVal.Slice(0, 1))
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": [{"blue_alliance":{"team1":"","team2":"","team3":""},"happened_at":"0001-01-01T00:00:00Z","id":"000000000000000000000000","key":"some-key","match_number":0,"match_set_number":0,"match_type":0,"red_alliance":{"team1":"","team2":"","team3":""},"scheduled_at":"0001-01-01T00:00:00Z","winner":0}]}`, response.Body.String())
}

func TestMatches_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestMatches_FindEventDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatches_NoMatchesInEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Err").Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": []}`, response.Body.String())
}

func TestMatches_FindMatchesDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Err").Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatches_MatchesDecodeError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindMany", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)
	matchResult.On("Err").Return(nil)
	matchResult.On("All", mock.AnythingOfType("*[]database.Match")).Return(errors.New("some decode error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	matches(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
