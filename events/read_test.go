package events

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestRead(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "events").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": {"id":"000000000000000000000000","name":"","key":"","start":"0001-01-01T00:00:00Z","end":"0001-01-01T00:00:00Z","year":0,"city":"","country":"","state_province":""}}`, response.Body.String())
}

func TestRead_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "events").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestRead_DatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	collection := &mocks.Collection{}
	result := &mocks.FindResult{}
	db.On("Collection", "events").Return(collection)
	collection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(result)
	result.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))

	// Run request
	read(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
