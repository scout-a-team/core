package events

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/mocks"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http/httptest"
	"testing"
)

func TestMatch(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).MatchIds = []string{"some-key"}
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(nil).Run(func(args mock.Arguments) {
		match := args.Get(0).(*database.Match)
		match.Key = "some-key"
		match.Winner = 1
	})

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.MatchKey, "some-key"))

	// Run request
	match(response, request)

	// Validate response
	assert.Equal(t, 200, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "success", "data": {"id":"000000000000000000000000","event":"","key":"some-key","scheduled_at":"0001-01-01T00:00:00Z","happened_at":"0001-01-01T00:00:00Z","match_type":0,"match_set_number":0,"match_number":0,"winner":1,"red_alliance":{"team_1":"","team_2":"","team_3":"","score_breakdown":{"init_line_robot":null,"endgame_robot":null,"auto_cells_bottom":0,"auto_cells_outer":0,"auto_cells_inner":0,"teleop_cells_bottom":0,"teleop_cells_outer":0,"teleop_cells_inner":0,"stage_1":false,"stage_2":false,"stage_3":false,"endgame_rung_level":false,"auto_init_line_points":0,"auto_cell_points":0,"teleop_cell_points":0,"control_panel_points":0,"auto_points":0,"teleop_points":0,"endgame_points":0,"adjust_points":0,"foul_points":0,"total_points":0,"shield_operational_ranking_point":false,"shield_energized_ranking_point":false,"ranking_points":0,"foul_count":0,"tech_foul_count":0}},"blue_alliance":{"team_1":"","team_2":"","team_3":"","score_breakdown":{"init_line_robot":null,"endgame_robot":null,"auto_cells_bottom":0,"auto_cells_outer":0,"auto_cells_inner":0,"teleop_cells_bottom":0,"teleop_cells_outer":0,"teleop_cells_inner":0,"stage_1":false,"stage_2":false,"stage_3":false,"endgame_rung_level":false,"auto_init_line_points":0,"auto_cell_points":0,"teleop_cell_points":0,"control_panel_points":0,"auto_points":0,"teleop_points":0,"endgame_points":0,"adjust_points":0,"foul_points":0,"total_points":0,"shield_operational_ranking_point":false,"shield_energized_ranking_point":false,"ranking_points":0,"foul_count":0,"tech_foul_count":0}},"red_score":0,"blue_score":0}}`, response.Body.String())
}

func TestMatch_NonExistentEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(mongo.ErrNoDocuments)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.MatchKey, "some-key"))

	// Run request
	match(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified event does not exist"}`, response.Body.String())
}

func TestMatch_FindEventDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.MatchKey, "some-key"))

	// Run request
	match(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}

func TestMatch_MatchNotInEvent(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil)

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.MatchKey, "some-key"))

	// Run request
	match(response, request)

	// Validate response
	assert.Equal(t, 404, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "specified match does not exist in specified event"}`, response.Body.String())
}

func TestMatch_FindMatchDatabaseError(t *testing.T) {
	// Setup facilities
	db := &mocks.Database{}
	eventCollection := &mocks.Collection{}
	matchCollection := &mocks.Collection{}
	eventResult := &mocks.FindResult{}
	matchResult := &mocks.FindResult{}
	db.On("Collection", "events").Return(eventCollection)
	db.On("Collection", "matches").Return(matchCollection)
	eventCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(eventResult)
	matchCollection.On("FindOne", mock.AnythingOfType("primitive.M")).Return(matchResult)
	eventResult.On("Decode", mock.AnythingOfType("*database.Event")).Return(nil).Run(func(args mock.Arguments) {
		args.Get(0).(*database.Event).MatchIds = []string{"some-key"}
	})
	matchResult.On("Decode", mock.AnythingOfType("*database.Match")).Return(errors.New("some database error"))

	// Setup request and response recorder
	response := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	request = request.WithContext(context.WithValue(request.Context(), request_context.FacilityDatabase, db))
	request = request.WithContext(context.WithValue(request.Context(), request_context.JwtUser, user))
	request = request.WithContext(context.WithValue(request.Context(), request_context.EventKey, "some-key"))
	request = request.WithContext(context.WithValue(request.Context(), request_context.MatchKey, "some-key"))

	// Run request
	match(response, request)

	// Validate response
	assert.Equal(t, 500, response.Code)
	assert.Equal(t, "application/json", response.Header().Get("Content-Type"))
	assert.Equal(t, `{"status": "error", "reason": "failed to query database"}`, response.Body.String())
}
