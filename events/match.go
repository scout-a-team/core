package events

import (
	"gitlab.com/scout-a-team/core/pkg/database"
	"gitlab.com/scout-a-team/core/pkg/request_context"
	"gitlab.com/scout-a-team/core/pkg/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"net/http"
)

func match(w http.ResponseWriter, r *http.Request) {
	db := request_context.GetDatabaseFacility(r)
	user := request_context.GetTokenUser(r)
	eventKey := request_context.GetEventKey(r)
	matchKey := request_context.GetMatchKey(r)

	logger := zap.L().Named("events.match").With(zap.String("path", "/events/{event}/matches/{match}"), zap.String("method", "GET"),
		zap.String("authenticated_user", user.Id.Hex()), zap.String("authenticated_team", user.TeamId), zap.String("event", eventKey), zap.String("match", matchKey))

	// Ensure event exists
	var event database.Event
	if err := db.Collection("events").FindOne(bson.M{"key": eventKey}).Decode(&event); err == mongo.ErrNoDocuments {
		responses.Error(w, http.StatusNotFound, "specified event does not exist")
		return
	} else if err != nil {
		logger.Error("failed to query database for specified event", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	// Ensure match exists in event
	exists := false
	for _, key := range event.MatchIds {
		if key == matchKey {
			exists = true
			break
		}
	}
	if !exists {
		responses.Error(w, http.StatusNotFound, "specified match does not exist in specified event")
		return
	}

	// Ensure match exists
	var match database.Match
	if err := db.Collection("matches").FindOne(bson.M{"key": matchKey}).Decode(&match); err != nil {
		logger.Error("failed to query database for specified match", zap.Error(err))
		responses.Error(w, http.StatusInternalServerError, "failed to query database")
		return
	}

	responses.SuccessWithData(w, match)
	logger.Info("got match in event")
}
